#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>
typedef struct Node {
	int data;
	struct Node* next;
}Node;

// 初始化栈
Node* initStack() {
	Node* S = (Node*)malloc(sizeof(Node));
	S->data = 0;
	S->next = NULL;
	return S;
}

// 判断是否为空
int isEmpty(Node* S) {
	if (S->data == 0 || S->next == NULL) {
		return 1;
	}
	else {
		return 0;
	}
}

// get 获取头结点后面第一个节点
int getTop(Node* S) {
	if (isEmpty(S)) {
		return -1;
	}
	else {
		return S->next->data;
	}
}

// 出栈
int pop(Node* S) {
	if (isEmpty(S)) {
		return -1;
	}
	else {
		Node* node = S->next;
		int data = node->data;
		S->next = node->next;
		S->data--;
		free(node);
		return data;
	}
}

// push 入栈 头插法
void push(Node* S, int data) {
	Node* node = (Node*)malloc(sizeof(Node));
	node->data = data;
	node->next = S->next;
	S->next = node;
	S->data++;
}

// 打印
void printStack(Node* S) {
	Node* node = S->next;
	while (node) {
		printf("%d -> ", node->data);
		node = node->next;
	}
	printf("NULL\n");
}

int main() {
	// 初始化栈
	Node* S = initStack();
	push(S, 1);
	push(S, 2);
	push(S, 3);
	push(S, 4);
	push(S, 5);
	printStack(S);
	// 出栈
	int i = pop(S);
	printf("当前元素为：%d\n", i);
	int i1 = pop(S);
	printf("当前元素为：%d\n", i1);
	int i2 = pop(S);
	printf("当前元素为：%d\n", i2);
	printStack(S);
	return 0;
}