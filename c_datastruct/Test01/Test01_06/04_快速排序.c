#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

void printArray(int array[], int length) {
    for (int i = 0; i < length; i++) {
        printf("%d ", array[i]);
    }
    printf("\n");
}

// i头节点，j尾结点
int partition(int array[], int i, int j) {
    // 与x作比较
    int x = array[i];
    while (i < j) {
        // 从后往前走一步，
        while (i < j && array[j] >= x) {
            j--;
        }
        // 填左坑并移动
        if (i < j) {
            array[i] = array[j];
            i++;
        }
        // 从前往后走一步
        while (i < j && array[i] < x) {
            i++;
        }
        // 填右坑并移动
        if (i < j) {
            array[j] = array[i];
            j--;
        }
    }
    array[i] = x;
    return i;
}

// 快速排序
void quickSort(int array[], int i, int j) {
    // 递归出口
    if (i < j) {
        // 区分
        int index = partition(array, i, j);
        printArray(array, 8);
        // 递归处理
        quickSort(array, i, index - 1);
        quickSort(array, index + 1, j);
    }
}

int main(){
    int array[8] = { 27, 38, 13, 49, 76, 97, 65, 49 };
    quickSort(array, 0, 7);
    return 0;
}