#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>

// 打印
void printArray(int array[], int length) {
	for (int i = 0; i < length; i++) {
		printf("%d", array[i]);
	}
	printf("\n");
}


void insertSort(int array[], int length) {
	// 从第二个元素开始比较
	for (int i = 1; i < length; i++) {
		// 内层循环，插入并排序
		for (int j = 0; j < i; j++) {
			if (array[i] < array[j]) {
				int temp = array[i];
				// 插入到array[j]之前
				for (int k = i - 1; k >= j;k--) {
					array[k + 1] = array[k];
				}
				array[j] = temp;
			}
		}
		printArray(array, 6);
	}
}


int main() {
	int array[6] = { 4,3,5,1,9,6};
	printArray(array, 6);
	insertSort(array, 6);
	return 0;
}