#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

void printArray(int array[], int length) {
    for (int i = 0; i < length; i++) {
        printf("%d ", array[i]);
    }
    printf("\n");
}

// 希尔排序
void shellSort(int array[], int length, int step) {
    for (int i = 0; i < length; i++) {
        // 第二个数
        for (int j = i + step; j < length; j += step) {
            // 比到j
            for (int k = i; k < j; k += step) {
                if (array[j] < array[k]) {
                    // 交换
                    int temp = array[j];
                    // 从后往前移动
                    for (int l = j - step; l >= k; l -= step) {
                        array[l + step] = array[l];
                    }
                    array[k] = temp;
                }
            }
        }
    }
}

int main(){
    int array[10] = { 49, 38, 65, 97, 76, 13, 27, 49, 55, 4 };
    int steps[3] = { 5, 3, 1 };
    for (int i = 0; i < 3; i++) {
        shellSort(array, 10, steps[i]);
        printArray(array, 10);
    }
    return 0;
}