#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>
//有问题，会卡死

// 最大顶点
// 图顶点不通，邻接矩阵值为MAX
// 顶点为自身，值为0
#define MAX 32767

// 边 
typedef struct Edge {
	// 起点索引
	int start;
	// 终点索引
	char end;
	// 权值
	int weight;
}Edge;


// 图
typedef struct Graph {
	char* vexs;
	int** arcs;
	int vexNum;
	int arcNum;
}Graph;

// 初始化边
Edge* initEdge(Graph* G) {
	int index=0;
	// 顶点数量
	Edge* edge = (Edge*)malloc(sizeof(Edge) * G->arcNum);
	// 初始化
	for (int i = 0;i < G->vexNum;i++) {
		// 从对角线之后开始
		for (int j = i + 1;j < G->vexNum;j++) {
			if (G->arcs[i][j] != MAX) {
				edge[index].start = i;
				edge[index].end = j;
				edge[index].weight = G->arcs[i][j];
				index++;
			}
		}
	}
	return edge;
}

// 边排序
void sortEdge(Edge* edge, Graph* G) {
	Edge temp;
	for (int i = 0; i < G->arcNum - 1; i++) {
		for (int j = 0; j < G->arcNum - i - 1; j++) {
			if (edge[j].weight > edge[j + 1].weight) {
				temp = edge[j];
				edge[j] = edge[j + 1];
				edge[j + 1] = temp;
			}
		}
	}
}


// 初始化图 vexNum个顶点
Graph* initGraph(int vexNum) {
	Graph* G = (Graph*)malloc(sizeof(Graph));
	// 顶点的值
	G->vexs = (char*)malloc(sizeof(char) * vexNum);
	// 存放一级指针地址
	G->arcs = (int**)malloc(sizeof(int*) * vexNum);
	// 二级指针赋值
	for (int i = 0;i < vexNum;i++) {
		G->arcs[i] = (int*)malloc(sizeof(int) * vexNum);
	}
	// 顶点数量
	G->vexNum = vexNum;
	G->arcNum = 0;
	return G;
}

// 创建图
void createGraph(Graph* G, char* vexs, int* arcs) {
	for (int i = 0;i < G->vexNum;i++) {
		// 每个顶点的值
		G->vexs[i] = vexs[i];
		// 邻接矩阵赋值
		for (int j = 0;j < G->vexNum;j++) {
			G->arcs[i][j] = *(arcs + i * G->vexNum + j);
			// 边数
			if (G->arcs[i][j] != 0 && G->arcs[i][j] != MAX)
				G->arcNum++;
		}
	}
	// 邻接矩阵边数/2
	G->arcNum /= 2;
}

// DFS 深度优先遍历
void DFS(Graph* G, int* visited, int index) {
	printf("%c\t", G->vexs[index]);
	// 设置已访问
	visited[index] = 1;
	for (int i = 0;i < G->vexNum;i++) {
		// 有边且未访问
		if (G->arcs[index][i] > 0 && G->arcs[index][i] != MAX && !visited[i]) {
			// 访问该节点
			DFS(G, visited, i);
		}
	}
}


// 克鲁斯卡尔算法
void kruskal(Graph* G) {
	// 辅助数组，判断连通分量
	int* connected = (int*)malloc(sizeof(int) * G->vexNum);
	// 初始化辅助数组，连通分量为其本身
	for (int i = 0;i < G->vexNum;i++) {
		connected[i] = i;
	}
	// 初始化边数组
	Edge* edge = initEdge(G);
	// 排序
	sortEdge(edge, G);
	for (int i = 0;i < G->arcNum;i++) {
		// 起点连通分量
		int start = connected[edge[i].start];
		// 终点连通分量
		int end = connected[edge[i].end];
		// 不相等
		if (start != end) {
			printf("v%c-->v%c weight=%d\n", G->vexs[edge[i].start], G->vexs[edge[i].end], edge[i].weight);
			// 终点连通分量的顶点都改为起点
			for (int j = 0;j < G->vexNum;j++) {
				if (connected[j] == end) {
					connected[j] = start;
				}
			}
		}
	}
}

int main() {
	Graph* G = initGraph(6);
	int arcs[6][6] = {
		0,  6,  1,  5,MAX,MAX,
		6,  0,  5,MAX,  3,MAX,
		1,  5,  0,  5,  6,  4,
		5,MAX,  5,  0,MAX,  2,
		MAX,3,  6,MAX,  0,  6,
		MAX,MAX,4,  2,  6,  0
	};
	createGraph(G, "123456", (int*)arcs);
	// 是否访问数组
	int* visited = (int*)malloc(sizeof(int) * G->vexNum);
	for (int i = 0;i < G->vexNum;i++) {
		visited[i] = 0;
	}
	// 深度优先遍历
	DFS(G, visited, 0);
	printf("\n");

	// 克鲁斯卡尔算法
	kruskal(G);
	return 0;
}