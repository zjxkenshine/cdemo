#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>
#define MAX 65535

// 图
typedef struct Graph {
	char* vexs;
	int** arcs;
	int vexNum;
	int arcNum;
}Graph;

// 用栈记录入度为0的点
typedef struct Node {
	int data;
	struct Node* next;
}Node;

// 初始化栈
Node* initStack() {
	Node* stack = (Node*)malloc(sizeof(Node));
	stack->data = 0;
	stack->next = NULL;
	return stack;
}

// 空栈
int isEmpty(Node* stack) {
	if (stack->next == NULL) {
		return 1;
	}
	else {
		return 0;
	}
}

// 入栈
void push(Node* stack, int data) {
	Node* node = (Node*)malloc(sizeof(Node));
	node->data = data;
	node->next = stack->next;
	stack->next = node;
	stack->data++;
}

// 出栈
int pop(Node* stack) {
	if (!isEmpty(stack)) {
		Node* node = stack->next;
		stack->next = node->next;
		return node->data;
	}
	else {
		return -1;
	}
}

// 求顶点入度
int* findInDegrees(Graph* G) {
	int* inDegrees = (int*)malloc(sizeof(int) * G->vexNum);
	// 初始化
	for (int i = 0; i < G->vexNum; i++) {
		inDegrees[i] = 0;
	}
	// 终点 入度+1
	for (int i = 0; i < G->vexNum; i++) {
		for (int j = 0; j < G->vexNum; j++) {
			if (G->arcs[i][j] > 0 && G->arcs[i][j] != MAX)
				inDegrees[j] = inDegrees[j] + 1;
		}
	}
	return inDegrees;
}

// 拓扑排序
int* topologicalSort(Graph* G) {
	int index = 0;
	int* top = (int*)malloc(sizeof(int) * G->vexNum);
	// 获取入度
	int* inDegrees = findInDegrees(G);
	// 栈
	Node* stack = initStack();
	// 入度为0入栈
	for (int i = 0; i < G->vexNum; i++) {
		if (inDegrees[i] == 0) {
			push(stack, i);
		}
	}
	while (!isEmpty(stack)) {
		// 出栈
		int vex = pop(stack);
		// 拓扑结果
		top[index++] = vex;
		for (int i = 0; i < G->vexNum; i++) {
			// 以vex为起点的弧
			if (G->arcs[vex][i] > 0 && G->arcs[vex][i] != MAX) {
				// 终点入度-1
				inDegrees[i] = inDegrees[i] - 1;
				if (inDegrees[i] == 0)
					push(stack, i);
			}
		}
	}
	for (int i = 0; i < index; i++) {
		printf("%c ", G->vexs[top[i]]);
	}
	printf("\n");
	return top;
}

// 初始化图 vexNum个顶点
Graph* initGraph(int vexNum) {
	Graph* G = (Graph*)malloc(sizeof(Graph));
	// 顶点的值
	G->vexs = (char*)malloc(sizeof(char) * vexNum);
	// 存放一级指针地址
	G->arcs = (int**)malloc(sizeof(int*) * vexNum);
	// 二级指针赋值
	for (int i = 0; i < vexNum; i++) {
		G->arcs[i] = (int*)malloc(sizeof(int) * vexNum);
	}
	// 顶点数量
	G->vexNum = vexNum;
	G->arcNum = 0;
	return G;
}

// 创建图
void createGraph(Graph* G, const char* vexs, int* arcs) {
	for (int i = 0; i < G->vexNum; i++) {
		// 每个顶点的值
		G->vexs[i] = vexs[i];
		// 邻接矩阵赋值
		for (int j = 0; j < G->vexNum; j++) {
			G->arcs[i][j] = *(arcs + i * G->vexNum + j);
			// 边数
			if (G->arcs[i][j] != 0)
				G->arcNum++;
		}
	}
	// 邻接矩阵边数/2
	G->arcNum /= 2;
}

// DFS 深度优先遍历
void DFS(Graph* G, int* visited, int index) {
	printf("%c\t", G->vexs[index]);
	// 设置已访问
	visited[index] = 1;
	for (int i = 0; i < G->vexNum; i++) {
		// 有边且未访问
		if (G->arcs[index][i] == 1 && !visited[i]) {
			// 访问该节点
			DFS(G, visited, i);
		}
	}
}

// 获取i在top序列中的索引
int getIndex(int* top, Graph* G, int i) {
	int j;
	for (j = 0; j < G->vexNum; j++) {
		if (top[j] == i) {
			break;
		}
	}
	return j;
}

// 关键路径
void criticalPath(Graph* G) {
	// 拓扑排序结果
	int* top = topologicalSort(G);
	// 顶点事件最早时间
	int* early = (int*)malloc(sizeof(int) * G->vexNum);
	// 顶点事件最晚时间
	int* late = (int*)malloc(sizeof(int) * G->vexNum);
	for (int i = 0; i < G->vexNum; i++) {
		early[i] = 0;
		late[i] = 0;
	}
	// 计算最早发生时间
	for (int i = 0; i < G->vexNum; i++) {
		// 最大值
		int max = 0;
		// 遍历所有顶点,找到top[i]的所有前置节点
		for (int j = 0; j < G->vexNum; j++) {
			// 是top[i]的前置节点
			if (G->arcs[j][top[i]] > 0 && G->arcs[j][top[i]] != MAX) {
				// 顶点j在拓扑序列中的索引
				int index = getIndex(top, G, j);
				// j的最早发生时间+j~top[i]弧长 的最大值
				if (early[index] + G->arcs[j][top[i]] > max)
					max = early[index] + G->arcs[j][top[i]];
			}
		}
		early[i] = max;
	}
	for (int i = 0; i < G->vexNum; i++) {
		printf("%d ", early[i]);
	}
	printf("\n");
	// 最后一个节点early与late相等
	late[(G->vexNum) - 1] = early[(G->vexNum) - 1];
	// 计算最晚发生时间 拓扑序列 从后往前计算
	for (int i = (G->vexNum) - 2; i >= 0; i--) {
		int min = MAX;
		// 遍历所有节点
		for (int j = 0; j < G->vexNum; j++) {
			// top[i] j为后置节点
			if (G->arcs[top[i]][j] > 0 && G->arcs[top[i]][j] != MAX) {
				// j的索引
				int index = getIndex(top, G, j);
				// j的最晚发生时间-弧
				if (late[index] - G->arcs[top[i]][j] < min)
					min = late[index] - G->arcs[top[i]][j];
			}
		}
		late[i] = min;
	}
	for (int i = 0; i < G->vexNum; i++) {
		printf("%d ", late[i]);
	}
	printf("\n");
	for (int i = 0; i < G->vexNum; i++) {
		for (int j = 0; j < G->vexNum; j++) {
			if (G->arcs[i][j] > 0 && G->arcs[i][j] != MAX) {
				// 起点
				int start = getIndex(top, G, i);
				// 终点
				int end = getIndex(top, G, j);
				// li-le=0 关键路径
				if ((late[end] - G->arcs[i][j]) - early[start] == 0) {
					printf("start = %d end = %d\n", i, j);
				}
			}
		}
	}
}


int main() {
	Graph* G = initGraph(9);
	int arcs[9][9] = {
		0, 6, 4, 5, MAX, MAX, MAX, MAX, MAX,
		MAX, 0, MAX, MAX, 1, MAX, MAX, MAX, MAX,
		MAX, MAX, 0, MAX, 1, MAX, MAX, MAX, MAX,
		MAX, MAX, MAX, 0, MAX, 2, MAX, MAX, MAX,
		MAX, MAX, MAX, MAX, 0, MAX, 9, 7, MAX,
		MAX, MAX, MAX, MAX, MAX, 0, MAX, 4, MAX,
		MAX, MAX, MAX, MAX, MAX, MAX, 0, MAX, 2,
		MAX, MAX, MAX, MAX, MAX, MAX, MAX, 0, 4,
		MAX, MAX, MAX, MAX, MAX, MAX, MAX, MAX, 0
	};
	createGraph(G, "012345678", (int*)arcs);
	// 是否访问数组
	int* visited = (int*)malloc(sizeof(int) * G->vexNum);
	for (int i = 0; i < G->vexNum; i++) {
		visited[i] = 0;
	}
	// 深度优先遍历
	DFS(G, visited, 0);
	printf("\n");
	// 关键路径
	criticalPath(G);
	return 0;
}