#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>

// 图
typedef struct Graph {
	char* vexs;
	int** arcs;
	int vexNum;
	int arcNum;
}Graph;

// 用栈记录入度为0的点
typedef struct Node {
	int data;
	struct Node* next;
}Node;

// 初始化栈
Node* initStack() {
	Node* stack = (Node*)malloc(sizeof(Node));
	stack->data = 0;
	stack->next = NULL;
	return stack;
}

// 空栈
int isEmpty(Node* stack) {
	if (stack->next == NULL) {
		return 1;
	}
	else {
		return 0;
	}
}

// 入栈
void push(Node* stack, int data) {
	Node* node = (Node*)malloc(sizeof(Node));
	node->data = data;
	node->next = stack->next;
	stack->next = node;
	stack->data++;
}

// 出栈
int pop(Node* stack) {
	if (!isEmpty(stack)) {
		Node* node = stack->next;
		stack->next = node->next;
		return node->data;
	}
	else {
		return -1;
	}
}

// 求每个顶点的入度
int* findInDegrees(Graph* G) {
	int* inDegrees = (int*)malloc(sizeof(int) * G->vexNum);
	// 初始化
	for (int i = 0; i < G->vexNum; i++) {
		inDegrees[i] = 0;
	}
	// 求入度
	for (int i = 0; i < G->vexNum; i++) {
		for (int j = 0; j < G->vexNum; j++) {
			// 有以它为结束点的边，领接矩阵竖着数即可
			if (G->arcs[i][j]) {
				inDegrees[j] = inDegrees[j] + 1;
			}
		}
	}
	return inDegrees;
}

// 拓扑排序
void topologicalSort(Graph* G) {
	int index = 0;
	// 排序的结果序列
	int* top = (int*)malloc(sizeof(int) * G->vexNum);
	// 入度初始化
	int* inDegrees = findInDegrees(G);
	// 初始化栈
	Node* stack = initStack();
	for (int i = 0; i < G->vexNum; i++) {
		// 入度为0 入栈
		if (inDegrees[i] == 0) {
			push(stack, i);
		}
	}

	// 选顶点，并调整其他顶点入度
	while (!isEmpty(stack)) {
		int vex = pop(stack);
		// 放入结果集
		top[index++] = vex;
		// 更改其他结果入度
		for (int i = 0; i < G->vexNum; i++) {
			// 从vex出发的所有节点入度-1
			if (G->arcs[vex][i]) {
				inDegrees[i] = inDegrees[i] - 1;
				// 入度为0 入栈
				if (inDegrees[i] == 0)
					push(stack, i);
			}
		}
	}
	// 打印拓扑序列
	for (int i = 0; i < index; i++) {
		printf("%c ", G->vexs[top[i]]);
	}
	printf("\n");
}


// 初始化图 vexNum个顶点
Graph* initGraph(int vexNum) {
	Graph* G = (Graph*)malloc(sizeof(Graph));
	// 顶点的值
	G->vexs = (char*)malloc(sizeof(char) * vexNum);
	// 存放一级指针地址
	G->arcs = (int**)malloc(sizeof(int*) * vexNum);
	// 二级指针赋值
	for (int i = 0; i < vexNum; i++) {
		G->arcs[i] = (int*)malloc(sizeof(int) * vexNum);
	}
	// 顶点数量
	G->vexNum = vexNum;
	G->arcNum = 0;
	return G;
}

// 创建图
void createGraph(Graph* G, const char* vexs, int* arcs) {
	for (int i = 0; i < G->vexNum; i++) {
		// 每个顶点的值
		G->vexs[i] = vexs[i];
		// 邻接矩阵赋值
		for (int j = 0; j < G->vexNum; j++) {
			G->arcs[i][j] = *(arcs + i * G->vexNum + j);
			// 边数
			if (G->arcs[i][j] != 0)
				G->arcNum++;
		}
	}
	// 邻接矩阵边数/2
	G->arcNum /= 2;
}

// DFS 深度优先遍历
void DFS(Graph* G, int* visited, int index) {
	printf("%c\t", G->vexs[index]);
	// 设置已访问
	visited[index] = 1;
	for (int i = 0; i < G->vexNum; i++) {
		// 有边且未访问
		if (G->arcs[index][i] == 1 && !visited[i]) {
			// 访问该节点
			DFS(G, visited, i);
		}
	}
}




int main() {
	Graph* G = initGraph(6);
	int arcs[6][6] = {
		0,1,1,1,0,0,
		0,0,0,0,0,0,
		0,1,0,0,1,0,
		0,0,0,0,1,0,
		0,0,0,0,0,0,
		0,0,0,1,1,0
	};
	createGraph(G, "123456", (int*)arcs);
	// 是否访问数组
	int* visited = (int*)malloc(sizeof(int) * G->vexNum);
	for (int i = 0; i < G->vexNum; i++) {
		visited[i] = 0;
	}
	// 深度优先遍历
	DFS(G, visited, 0);
	printf("\n");
	// 拓扑排序
	topologicalSort(G);
	return 0;
}