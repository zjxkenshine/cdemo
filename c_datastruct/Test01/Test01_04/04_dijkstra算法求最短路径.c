#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
#include<stdlib.h>

#define MAX 32767


// 图
typedef struct Graph {
	char* vexs;
	int** arcs;
	int vexNum;
	int arcNum;
}Graph;


// 初始化图 vexNum个顶点
Graph* initGraph(int vexNum) {
	Graph* G = (Graph*)malloc(sizeof(Graph));
	// 顶点的值
	G->vexs = (char*)malloc(sizeof(char) * vexNum);
	// 存放一级指针地址
	G->arcs = (int**)malloc(sizeof(int*) * vexNum);
	// 二级指针赋值
	for (int i = 0;i < vexNum;i++) {
		G->arcs[i] = (int*)malloc(sizeof(int) * vexNum);
	}
	// 顶点数量
	G->vexNum = vexNum;
	G->arcNum = 0;
	return G;
}

// 创建图
void createGraph(Graph* G, char* vexs, int* arcs) {
	for (int i = 0;i < G->vexNum;i++) {
		// 每个顶点的值
		G->vexs[i] = vexs[i];
		// 邻接矩阵赋值
		for (int j = 0;j < G->vexNum;j++) {
			G->arcs[i][j] = *(arcs + i * G->vexNum + j);
			// 边数
			if (G->arcs[i][j] != 0 && G->arcs[i][j] != MAX)
				G->arcNum++;
		}
	}
	// 邻接矩阵边数/2
	G->arcNum /= 2;
}

// DFS 深度优先遍历
void DFS(Graph* G, int* visited, int index) {
	printf("%c\t", G->vexs[index]);
	// 设置已访问
	visited[index] = 1;
	for (int i = 0;i < G->vexNum;i++) {
		// 有边且未访问
		if (G->arcs[index][i] > 0 && G->arcs[index][i] != MAX && !visited[i]) {
			// 访问该节点
			DFS(G, visited, i);
		}
	}
}

// 获取最小数组
int getMin(int* d, int* s, Graph* G) {
	int min = MAX;
	int index;
	for (int i = 0; i < G->vexNum; i++) {
		if (!s[i] && d[i] < min) {
			min = d[i];
			index = i;
		}
	}
	return index;
}

// 迪杰斯特拉算法
void dijkstra(Graph* G, int index) {
	// 准备辅助数组
	// 是否已求
	int* s = (int*)malloc(sizeof(int) * G->vexNum);
	// 前驱节点
	int* p = (int*)malloc(sizeof(int) * G->vexNum);
	// 最短路径
	int* d = (int*)malloc(sizeof(int) * G->vexNum);
	// 初始化辅助数组
	for (int i = 0; i < G->vexNum; i++) {
		if (G->arcs[index][i] > 0 && G->arcs[index][i] != MAX) {
			d[i] = G->arcs[index][i];
			p[i] = index;
		}
		else {
			d[i] = MAX;
			p[i] = -1;
		}
		if (i == index) {
			s[i] = 1;
			d[i] = 0;
		}
		else
			s[i] = 0;
	}
	for (int i = 0; i < G->vexNum - 1; i++) {
		// 获取最小路径
		int index = getMin(d, s, G);
		s[index] = 1;
		// 调整距离
		for (int j = 0; j < G->vexNum; j++) {
			// 未找到最短路径，且新路径比当前距离小
			if (!s[j] && d[index] + G->arcs[index][j] < d[j]) {
				// 调整距离
				d[j] = d[index] + G->arcs[index][j];
				// 前驱节点
				p[j] = index;
			}
		}
	}
	for (int i = 0; i < G->vexNum; i++) {
		printf("%d %d %d\n", s[i], p[i], d[i]);
	}
}

int main() {
	Graph* G = initGraph(7);
	int arcs[7][7] = {
        0, 12, MAX, MAX, MAX, 16, 14,
        12, 0, 10, MAX, MAX, 7, MAX,
        MAX, 10, 0, 3, 5, 6, MAX,
        MAX, MAX, 3, 0, 4, MAX, MAX,
        MAX, MAX, 5, 4, 0, 2, 8,
        16, 7, 6, MAX, 2, 0, 9,
        14, MAX, MAX, MAX, 8, 9, 0
    };
	createGraph(G, "1234567", (int*)arcs);
	// 是否访问数组
	int* visited = (int*)malloc(sizeof(int) * G->vexNum);
	for (int i = 0;i < G->vexNum;i++) {
		visited[i] = 0;
	}
	// 深度优先遍历
	DFS(G, visited, 0);
	printf("\n");

	dijkstra(G, 0);
	return 0;
}