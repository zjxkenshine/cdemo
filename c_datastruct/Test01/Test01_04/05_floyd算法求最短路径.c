#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>

// 最大顶点
#define MAX 32767

// 图
typedef struct Graph {
	char* vexs;
	int** arcs;
	int vexNum;
	int arcNum;
}Graph;

// 初始化图 vexNum个顶点
Graph* initGraph(int vexNum) {
	Graph* G = (Graph*)malloc(sizeof(Graph));
	// 顶点的值
	G->vexs = (char*)malloc(sizeof(char) * vexNum);
	// 存放一级指针地址
	G->arcs = (int**)malloc(sizeof(int*) * vexNum);
	// 二级指针赋值
	for (int i = 0;i < vexNum;i++) {
		G->arcs[i] = (int*)malloc(sizeof(int) * vexNum);
	}
	// 顶点数量
	G->vexNum = vexNum;
	G->arcNum = 0;
	return G;
}

// 创建图
void createGraph(Graph* G, char* vexs, int* arcs) {
	for (int i = 0;i < G->vexNum;i++) {
		// 每个顶点的值
		G->vexs[i] = vexs[i];
		// 邻接矩阵赋值
		for (int j = 0;j < G->vexNum;j++) {
			G->arcs[i][j] = *(arcs + i * G->vexNum + j);
			// 边数
			if (G->arcs[i][j] != 0 && G->arcs[i][j] != MAX)
				G->arcNum++;
		}
	}
	// 邻接矩阵边数/2
	G->arcNum /= 2;
}

// DFS 深度优先遍历
void DFS(Graph* G, int* visited, int index) {
	printf("%c\t", G->vexs[index]);
	// 设置已访问
	visited[index] = 1;
	for (int i = 0;i < G->vexNum;i++) {
		// 有边且未访问
		if (G->arcs[index][i] > 0 && G->arcs[index][i] != MAX && !visited[i]) {
			// 访问该节点
			DFS(G, visited, i);
		}
	}
}

void floyd(Graph* G) {
	int d[G->vexNum][G->vexNum];
	int p[G->vexNum][G->vexNum];
	for (int i = 0;i < G->vexNum;i++) {
		for (int j = 0;j < G->vexNum;j++) {
			d[i][j] = G->arcs[i][j];
			if (G->arcNum[i][j] > 0 && G->arcNum[i][j] != MAX) {
				p[i][j] = i;
			}
			else {
				p[i][j] = -1;
			}
		}
	}

	// 更新数据
	for (int i = 0;i < G->vexNum;i++) {
		for (int j = 0;i < G->vexNum;j++) {
			for (int k = 0;k < G->vexNum;k++) {
				if (d[j][i] + d[i][k] < d[j][k]) {
					d[j][k] = d[j][i] + d[i][k];
					p[j][k] = p[i][k];
				}
			}
		}
	}

	for (int i = 0;i < G->vexNum;i++) {
		for (int j = 0;i < G->vexNum;j++) {
			printf("%d ", d[i][j]);
		}
		printf("\n");
	}

	printf("\n");
	for (int i = 0; i < G->vexNum; i++) {
		for (int j = 0; j < G->vexNum; j++) {
			printf("%d ", p[i][j]);
		}
		printf("\n");
	}

}

int main() {
	Graph* G = initGraph(4);
	int* visited = (int*)malloc(sizeof(int) * G->vexNum);
	// 节点未访问
	for (int i = 0; i < G->vexNum; i++) {
		visited[i] = 0;
	}
	// 领接矩阵
	int arcs[4][4] = {
		0, 1, MAX, 3,
		1, 0, 2, 2,
		MAX, 2, 0, 8,
		3, 2, 8, 0
	};
	createGraph(G, "1234", (int*)arcs);
	DFS(G, visited, 0);
	printf("\n");
	floyd(G);
	return 0;
}