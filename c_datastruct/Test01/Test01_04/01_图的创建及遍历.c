#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>

// 队列
typedef struct Node {
	int data;
	struct Node* next;
}Node;

// 初始化
Node* initQueue() {
	Node* Q = (Node*)malloc(sizeof(Node));
	Q->data = 0;
	Q->next = NULL;
	return Q;
}

// 入队
void enQueue(Node* Q, int data) {
	Node* q = Q;
	Node* node = (Node*)malloc(sizeof(Node));
	node->data = data;
	node->data = data;
	for (int i = 0;i < Q->data;i++) {
		q = q->next;
	}
	node->next = q->next;
	q->next = node;
	Q->data++;
}

// 判断队列是否为空
int isEmpty(Node* Q) {
	if (Q->data == 0 || Q->next == NULL) {
		return 1;
	}
	else {
		return 0;
	}
}

// 出队
int deQueue(Node* Q) {
	if (isEmpty(Q)) {
		return -1;
	}
	else {
		Node* node = Q->next;
		int data = node->data;
		Q->next = node->next;
		free(node);
		Q->data--;
		return data;
	}

}

// 遍历队列
void printQueue(Node* Q) {
	Node* node = Q->next;
	while (node) {
		printf("%d -> ", node->data);
		node = node->next;
	}
	printf("NULL\n");
}


// 图
typedef struct Graph {
	char* vexs;
	int** arcs;
	int vexNum;
	int arcNum;
}Graph;

// 初始化图 vexNum个顶点
Graph* initGraph(int vexNum) {
	Graph* G = (Graph*)malloc(sizeof(Graph));
	// 顶点的值
	G->vexs = (char*)malloc(sizeof(char) * vexNum);
	// 存放一级指针地址
	G->arcs = (int**)malloc(sizeof(int*) * vexNum);
	// 二级指针赋值
	for (int i = 0;i < vexNum;i++) {
		G->arcs[i] = (int*)malloc(sizeof(int) * vexNum);
	}
	// 顶点数量
	G->vexNum = vexNum;
	G->arcNum = 0;
	return G;
}

// 创建图
void createGraph(Graph* G,const char* vexs, int* arcs) {
	for (int i = 0;i < G->vexNum;i++) {
		// 每个顶点的值
		G->vexs[i] = vexs[i];
		// 邻接矩阵赋值
		for (int j = 0;j < G->vexNum;j++) {
			G->arcs[i][j] = *(arcs + i * G->vexNum + j);
			// 边数
			if (G->arcs[i][j] != 0)
				G->arcNum++;
		}
	}
	// 邻接矩阵边数/2
	G->arcNum /= 2;
}

// DFS 深度优先遍历
void DFS(Graph* G, int* visited, int index) {
	printf("%c\t", G->vexs[index]);
	// 设置已访问
	visited[index] = 1;
	for (int i = 0;i < G->vexNum;i++) {
		// 有边且未访问
		if (G->arcs[index][i] == 1 && !visited[i]) {
			// 访问该节点
			DFS(G, visited, i);
		}
	}
}

// BFS 广度优先遍历
void BFS(Graph* G, int* visited, int index) {
	// 初始化队列
	Node* Q = initQueue();
	printf("%c\t", G->vexs[index]);
	visited[index] = 1;
	enQueue(Q, index);
	while(!isEmpty(Q)){
		// 出队
		int i = deQueue(Q);
		for (int j = 0;j < G->vexNum;j++) {
			// 子节点且未访问
			if (G->arcs[i][j] == 1 && !visited[j]) {
				printf("%c\t", G->vexs[j]);
				// 访问
				visited[j] = 1;
				// 进队
				enQueue(Q, j);
			}
		}
	}
}

int main1() {
	Graph* G = initGraph(5);
	int arcs[5][5] = {
		0,1,1,1,0,
		1,0,1,1,1,
		1,1,0,0,0,
		1,1,0,0,1,
		0,1,0,1,0
	};
	createGraph(G, "ABCDE", (int*)arcs);
	// 是否访问数组
	int* visited=(int*)malloc(sizeof(int) * G->vexNum);
	for (int i = 0;i < G->vexNum;i++) {
		visited[i] = 0;
	}
	// 深度优先遍历
	DFS(G, visited, 0);
	printf("\n");
	
	for (int i = 0;i < G->vexNum;i++) {
		visited[i] = 0;
	}
	// 广度优先遍历
	BFS(G, visited, 0);
	return 0;
}