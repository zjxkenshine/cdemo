#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>

// 最大顶点
// 图顶点不通，邻接矩阵值为MAX
// 顶点为自身，值为0
#define MAX 32767

// 边 U集合，树的边
typedef struct Edge {
	// 边
	char vex;
	// 权值
	int weight;
}Edge;


// 图
typedef struct Graph {
	char* vexs;
	int** arcs;
	int vexNum;
	int arcNum;
}Graph;

// 初始化边数组U集合
// edge.weight=0时，代表顶点已经在U集合中
Edge* initEdge(Graph* G, int index) {
	// 顶点数量
	Edge* edge = (Edge*)malloc(sizeof(Edge) * G -> vexNum);
	// 初始化
	for (int i = 0;i < G->vexNum;i++) {
		// 顶点名称
		edge[i].vex = G->vexs[index];
		edge[i].weight = G->arcs[index][i];
	}
	return edge;
}

// 初始化图 vexNum个顶点
Graph* initGraph(int vexNum) {
	Graph* G = (Graph*)malloc(sizeof(Graph));
	// 顶点的值
	G->vexs = (char*)malloc(sizeof(char) * vexNum);
	// 存放一级指针地址
	G->arcs = (int**)malloc(sizeof(int*) * vexNum);
	// 二级指针赋值
	for (int i = 0;i < vexNum;i++) {
		G->arcs[i] = (int*)malloc(sizeof(int) * vexNum);
	}
	// 顶点数量
	G->vexNum = vexNum;
	G->arcNum = 0;
	return G;
}

// 获取最小边
int getMinEdge(Edge* edge, Graph* G) {
	int index;
	int min = MAX;
	for (int i = 0;i < G->vexNum;i++) {
		if (edge[i].weight != 0 && min > edge[i].weight) {
			min = edge[i].weight;
			index = i;
		}
	}
	return index;
}

// prim算法
void prim(Graph* G, int index) {
	Edge* edge = initEdge(G, index);
	int min;
	for (int i = 0;i < G->vexNum - 1;i++) {
		min = getMinEdge(edge, G);
		printf("v%c --> v%c weight = %d\n", edge[min].vex, G->vexs[min], edge[min].weight);
		// 加入顶点
		edge[min].weight = 0;
		for (int j = 0;j < G->vexNum;j++) {
			if (G->arcs[min][j] < edge[j].weight) {
				// 更新最小边
				edge[j].weight = G->arcs[min][j];
				// 更新起点
				edge[j].vex = G->vexs[min];
			}
		}
	}
}

// 创建图
void createGraph(Graph* G, char* vexs, int* arcs) {
	for (int i = 0;i < G->vexNum;i++) {
		// 每个顶点的值
		G->vexs[i] = vexs[i];
		// 邻接矩阵赋值
		for (int j = 0;j < G->vexNum;j++) {
			G->arcs[i][j] = *(arcs + i * G->vexNum + j);
			// 边数
			if (G->arcs[i][j] != 0&& G->arcs[i][j] !=MAX)
				G->arcNum++;
		}
	}
	// 邻接矩阵边数/2
	G->arcNum /= 2;
}

// DFS 深度优先遍历
void DFS(Graph* G, int* visited, int index) {
	printf("%c\t", G->vexs[index]);
	// 设置已访问
	visited[index] = 1;
	for (int i = 0;i < G->vexNum;i++) {
		// 有边且未访问
		if (G->arcs[index][i] > 0&& G->arcs[index][i]!=MAX && !visited[i]) {
			// 访问该节点
			DFS(G, visited, i);
		}
	}
}

int main2() {
	Graph* G = initGraph(6);
	int arcs[6][6] = {
		0,  6,  1,  5,MAX,MAX,
		6,  0,  5,MAX,  3,MAX,
		1,  5,  0,  5,  6,  4,
		5,MAX,  5,  0,MAX,  2,
		MAX,3,  6,MAX,  0,  6,
		MAX,MAX,4,  2,  6,  0
	};
	createGraph(G, "123456", (int*)arcs);
	// 是否访问数组
	int* visited = (int*)malloc(sizeof(int) * G->vexNum);
	for (int i = 0;i < G->vexNum;i++) {
		visited[i] = 0;
	}
	// 深度优先遍历
	DFS(G, visited, 0);
	printf("\n");

	// 最小生成树
	prim(G,0);
	return 0;
}