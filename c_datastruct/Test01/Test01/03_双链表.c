#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>

#define TRUE 1
#define FALSE 0

typedef struct Node {
	int data;
	struct Node* pre;
	struct Node* next;
}Node;

// 初始化链表
Node* initList() {
	// 头节点
	Node* L = (Node*)malloc(sizeof(Node));
	// 长度
	L->data = 0;
	L->pre = NULL;
	L->next = NULL;
	return L;
}

// 头插法
void headInsert(Node* L, int data) {
	// 开辟空间
	Node* node = (Node*)malloc(sizeof(Node));
	node->data = data;
	// 链表为空
	if (L->data == 0) {
		node->next = L->next;
		node->pre = L;
		L->next = node;
		L->data++;
	}
	else {
		node->pre = L;
		node->next = L->next;
		L->next->pre = node;
		L->next = node;
		L->data++;
	}
}

// 尾插法
void tailInsert(Node* L,int data) {
	Node* node = L;
	Node* n = (Node*)malloc(sizeof(Node));
	n->data = data;
	// 找到最后一个节点
	while (node->next) {
		node = node->next;
	}
	n->next = node->next;
	node->next = n;
	n->pre = node;
	L->data++;
}

// 删除操作
int delete(Node* L,int data) {
	Node* node = L->next;
	while (node) {
		// 删除操作
		if (node->data == data) {
			node->pre->next = node->next;
			node->next->pre = node->pre;
			free(node);
			L->data--;
			break;
		}
		node = node->next;
	}
	return FALSE;
}


// 打印
void printList(Node* L) {
	Node* node = L->next;
	while(node) {
		printf("%d->", node->data);
		node = node->next;
	}
	printf("NULL\n");
}

int main() {
	Node* L = initList();
	headInsert(L, 1);
	headInsert(L, 2);
	headInsert(L, 3);
	headInsert(L, 4);
	printList(L);
	tailInsert(L, 5);
	tailInsert(L, 6);
	tailInsert(L, 7);
	tailInsert(L, 8);
	printList(L);
	// 删除
	delete(L, 1);
	delete(L, 3);
	printList(L);
	return 0;
}


