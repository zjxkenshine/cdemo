#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
// 删除成功或失败
#define TRUE 1
#define FALSE 0

// 单循环链表实现
typedef struct Node {
	int data;
	struct Node* next;
}Node;

// 初始化
Node* initList() {
	Node* L = (Node*)malloc(sizeof(Node));
	L->data = 0;
	L->next = L;
	return L;
}

// 头插
void headInsert(Node* L, int data) {
	Node* node = (Node*)malloc(sizeof(Node));
	node->data = data;
	node->next = L->next;
	L->next = node;
	L->data++;
}

// 尾插法
void tailInsert(Node* L, int data) {
	Node* n = L;
	Node* node = (Node*)malloc(sizeof(Node));
	node->data = data;
	// 找到最后一个节点
	while (n->next != L) {
		n = n->next;
	}
	node->next = L;
	n->next = node;
	L->data++;
}

// 删除节点
int delete(Node* L,int data) {
	Node* pre = L;
	Node* node = L->next;
	// 找到对应节点
	while (node != L) {
		if (node->data == data) {
			// 删除
			pre->next = node->next;
			free(node);
			return TRUE;
		}
		pre = node;
		node = node->next;
	}
	return FALSE;
;}

// 打印
void printList(Node* L) {
	Node* node = L->next;
	while (node!=L) {
		printf("%d->", node->data);
		node = node->next;
	}
	printf("NULL\n");
}

// 测试
int main() {
	Node* L = initList();
	// 头插
	headInsert(L,1);
	headInsert(L,2);
	headInsert(L,3);
	headInsert(L,4);
	printList(L);

	// 尾插法
	tailInsert(L, 5);
	tailInsert(L, 6);
	tailInsert(L, 7);
	tailInsert(L, 8);
	printList(L);

	// 删除
	delete(L, 4);
	printList(L);
	delete(L, 7);
	printList(L);
	return 0;
}