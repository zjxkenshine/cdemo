#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>

typedef struct Node {
	int data;
	struct Node* pre;
	struct Node* next;
}Node;

// 初始化
Node* initList() {
	Node* L = (Node*)malloc(sizeof(Node));
	L->data = 0;
	L->pre = L;
	L->next = L;
	return L;
}

// 头插法
void headInsert(Node* L,int data) {
	Node* node = (Node*)malloc(sizeof(Node));
	node->data = data;
	// 链表为空
	if (L->data == 0) {
		L->next = node;
		L->pre = node;
		node->next = L;
		node->pre = L;
		L->data++;
	}
	else {
		L->next->pre = node;
		node->next = L->next;
		node->pre = L;
		L->next = node;
		L->data++;
	}
}

// 尾插法
void tailInsert(Node* L, int data) {
	Node* node = L;
	Node* n = (Node*)malloc(sizeof(Node));
	n->data = data;
	// 找到最后一个节点
	while (node->next != L) {
		node = node->next;
	}
	n->next = L;
	n->pre = node;
	L->pre = n;
	node->next = n;
	L->data++;
}

// 节点删除
int delete(Node* L,int data) {
	Node* node = L->next;
	// 找到对应节点
	while (node != L) {
		// 删除对应节点
		if (node->data == data) {
			node->next->pre = node->pre;
			node->pre->next = node->next;
			free(node);
			L->data--;
			return 1;
		}
		node=node->next;
	}
	return 0;
}

// 打印
void printList(Node* L) {
	Node* node = L->next;
	while (node != L) {
		printf("%d -> ", node->data);
		node = node->next;
	}
	printf("NULL\n");
}

int main() {
	Node* node = initList();
	headInsert(node, 1);
	headInsert(node, 2);
	headInsert(node, 3);
	headInsert(node, 4);
	printList(node);
	// 尾插法
	tailInsert(node, 5);
	tailInsert(node, 6);
	tailInsert(node, 7);
	tailInsert(node, 8);
	printList(node);

	delete(node, 3);
	delete(node, 7);
	printList(node);
	return 0;
}
