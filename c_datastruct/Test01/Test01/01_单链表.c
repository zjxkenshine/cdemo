#define _CRT_SECURE_NO_WARNINGS
// 单链表代码实现
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Node {
	int data;
	struct Node* next;
}Node;

// 初始化头结点
Node* initList() {
	Node* list = (Node*)malloc(sizeof(Node));
	// 头结点data记录长度
	list->data = 0;
	list->next = NULL;
	return list;
}

// 头插法
void headInsert(Node* list, int data) {
	Node* node = (Node*)malloc(sizeof(Node));
	node->data = data;
	node->next = list->next;
	list->next = node;
	list->data++;
}

// 尾插法
void tailInsert(Node* list, int data) {
	// 头节点
	Node* head = list;
	Node* node = (Node*)malloc(sizeof(Node));
	node->data = data;
	node->next = NULL;
	list = list->next;
	// 找到最后一个节点
	while (list->next) {
		list = list->next;
	}
	list->next = node;
	head->data++;
}

// 删除节点
void delete(Node* list, int data) {
	Node* pre = list;
	Node* current = list->next;
	// list不为空
	while (current) {
		if (current->data == data) {
			// 删除current节点
			pre->next = current->next;
			// 删除并释放
			free(current);
			break;
		}
		pre = current;
		current = current->next;
	}
	list->data--; 
}

void printList(Node* list) {
	Node* node = list->next;
	while (node) {
		printf("node = %d\n", node->data);
		node = node->next;
	}
}

int main() {
	Node* list = initList();
	headInsert(list, 1);
	headInsert(list, 2);
	headInsert(list, 3);
	headInsert(list, 4);
	headInsert(list, 5);
	// 尾插5个数
	tailInsert(list, 6);
	tailInsert(list, 7);
	tailInsert(list, 8);
	tailInsert(list, 9);
	tailInsert(list, 10);
	// 打印列表
	printList(list);
	printf("\n");
	// 删除
	delete(list, 5);
	delete(list, 10);
	delete(list, 6);
	printList(list);

	return 0;
}