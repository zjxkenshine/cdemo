#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>

typedef struct TreeNode {
	int data;
	struct TreeNode* lchild;
	struct TreeNode* rchild;
}TreeNode;

// 查找
TreeNode* bstSearch(TreeNode* T,int data) {
	if (T) {
		if (T->data == data) {
			return T;
		}
		else if (data < T->data) {
			return bstSearch(T->lchild, data);
		}
		else {
			return bstSearch(T->rchild, data);
		}
	}
	else {
		return NULL;
	}
}
// 创建树
void bstInsert(TreeNode** T, int data) {
	// 为空
	if (*T == NULL) {
		*T = (TreeNode*)malloc(sizeof(TreeNode));
		(*T)->data = data;
		(*T)->lchild = NULL;
		(*T)->rchild = NULL;
	}
	else if (data == (*T)->data) {
		// 什么都不做，二叉排序树不能有重复
		return;
	}
	else if (data < (*T)->data) {
		// 左子树
		bstInsert(&((*T)->lchild), data);
	}
	else {
		// 右子树
		bstInsert(&((*T)->rchild), data);
	}
}

// 前序遍历
void preOrder(TreeNode* T) {
	if (T == NULL) {
		return;
	}
	printf("%d ", T->data);
	preOrder(T->lchild);
	preOrder(T->rchild);

}

int main() {
	TreeNode* T = NULL;
	int nums[6] = { 4,5,19,23,2,8 };
	for (int i = 0;i < 6;i++) {
		bstInsert(&T, nums[i]);
	}
	preOrder(T);
	// 搜索
	TreeNode* node =bstSearch(T,4);
	printf("\n");
	printf("%d ", node->data);
	return 0;
}