#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>

typedef struct TreeNode {
	char data;
	struct TreeNode* lchild;
	struct TreeNode* rchild;
	// 父节点
	struct TreeNode* parent;
	// 是否有左右节点的标志 0为有 1为没有
	int ltag;
	int rtag;
}TreeNode;

// 创建树，二级指针
void createTree(TreeNode** T, const char* data, int* index,TreeNode* parent) {
	char ch;
	ch = data[*index];
	*index += 1;

	// 输入data
	//scanf("%c\n", &ch);
	if (ch == '#') {
		// 空节点
		*T = NULL;
	}
	else {
		// 非空节点
		// 函数参数必须为二级指针，否则这里无法赋值
		*T = (TreeNode*)malloc(sizeof(TreeNode));
		(*T)->data = ch;
		(*T)->ltag = 0;
		(*T)->rtag = 0;
		(*T)->parent = parent;
		// 递归创建左右子树
		createTree(&((*T)->lchild), data, index,*T);
		createTree(&((*T)->rchild), data, index,*T);
	}
}

// 线索化  pre前一个遍历的节点
void postThreadTree(TreeNode* T, TreeNode** pre) {
	if (T) {
		postThreadTree(T->lchild, pre);
		postThreadTree(T->rchild, pre);

		// 没有左孩子，左孩子指向前一个前驱节点
		if (T->lchild == NULL) {
			T->ltag = 1;
			T->lchild = *pre;
		}
		// 前驱节点的右孩子指向该节点
		if (*pre != NULL && (*pre)->rchild == NULL) {
			(*pre)->rtag = 1;
			(*pre)->rchild = T;
		}
		*pre = T;
	}
}

// 后序遍历的第一个节点
// 找到最左边的节点 
// 再判断是否有右子树，有的话再找右子树的第一个节点
TreeNode* getFirst(TreeNode* T) {
	// 树本身的指针，找到最左的节点
	while (T->ltag == 0) {
		T = T->lchild;
	}
	if (T->rtag == 0) {
		return getFirst(T->rchild);
	}
	return T;
}

// 找到下一个节点
// 1.根节点 nex=NULL
// 2.左孩子， 判断父亲右孩子是否为空
//    为空：next=parent;
//    不为空：next=getFirst(parent->rchild)
// 3.右孩子：next=parent;
TreeNode* getNext(TreeNode* T) {
	// 没有右子树
	if (T->rtag == 1) {
		return T->rchild;
	}
	// 有右子树
	else {
		// 根节点
		if ((T->parent) == NULL){
			return NULL;
		}
		// 右孩子
		else if (T->parent->rchild == T) {
			return T->parent;
		}
		else { // 左孩子
			// 父亲有右子树
			if (T->parent->rtag == 0) {
				return getFirst(T->parent->rchild);
			}
			else {
				return T->parent;
			}
		}
	}
}


int main() {
	TreeNode* T;
	TreeNode* pre = NULL;
	int index = 0;
	createTree(&T, "ABD##E##CF##G##", &index,NULL);
	// 线索化
	postThreadTree(T, &pre);
	// 最后一个节点是根节点，不用修改指向
	//pre->rtag = 1;
	//pre->rchild = NULL;

	// 后序遍历
	for (TreeNode* node = getFirst(T);node != NULL;node = getNext(node)) {
		printf("%c ", node->data);
	}
	printf("\n");
	return 0;
}