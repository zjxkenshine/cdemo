#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
#include<stdlib.h>

// 二叉树
typedef struct TreeNode{
	char data;
	struct TreeNode* lchild;
	struct TreeNode* rchild;
	int flag;
}TreeNode;

// 栈
typedef struct StackNode {
	TreeNode* data;
	struct StackNode* next;
}StackNode;

// 创建树，二级指针
void createTree(TreeNode** T, const char* data, int* index) {
	char ch;
	ch = data[*index];
	*index += 1;

	// 输入data
	//scanf("%c\n", &ch);
	if (ch == '#') {
		// 空节点
		*T = NULL;
	}
	else {
		// 非空节点
		// 函数参数必须为二级指针，否则这里无法赋值
		*T = (TreeNode*)malloc(sizeof(TreeNode));
		(*T)->data = ch;
		// 是否被访问
		(*T)->flag = 0;
		// 递归创建左右子树
		createTree(&((*T)->lchild), data, index);
		createTree(&((*T)->rchild), data, index);
	}
}

// 初始化栈
StackNode* initStack() {
	StackNode* s = (StackNode*)malloc(sizeof(StackNode));
	s->data = NULL;
	s->next = NULL;
	return s;
}

// 进栈
void push(TreeNode* data, StackNode* S) {
	StackNode* node = (StackNode*)malloc(sizeof(TreeNode));
	node->data = data;
	node->next = S->next;
	S->next = node;
}

// 栈空
int isEmpty(StackNode* S) {
	if (S->next == NULL) {
		return 1;
	}
	else {
		return 0;
	}
}

// 出栈
StackNode* pop(StackNode* S) {
	if (isEmpty(S)) {
		return NULL;
	}
	else {
		StackNode* node = S->next;
		S->next = node->next;
		return node;
	}
}

// 先序遍历
void preOrder(TreeNode* T) {
	TreeNode* node = T;
	StackNode* S = initStack();
	while (node||!isEmpty(S)) {
		if (node) {
			// 入栈访问
			printf("%c ", node->data);
			push(node, S);
			node = node->lchild;
		}
		else {
			// 出栈
			node = pop(S)->data;
			node = node->rchild;
		}
	}
}

// 中序遍历
void inOrder(TreeNode* T) {
	TreeNode* node = T;
	StackNode* S = initStack();
	while (node || !isEmpty(S)) {
		if (node) {
			push(node, S);
			node = node->lchild;
		}
		else {
			// 出栈
			node = pop(S)->data;
			// 出栈访问
			printf("%c ", node->data);
			node = node->rchild;
		}
	}
}

// 获取栈顶元素
StackNode* getTop(StackNode* S) {
	if (isEmpty(S)) {
		return NULL;
	}
	else {
		StackNode* node = S->next;
		return node;
	}
}

// 后序遍历
void postOrder(TreeNode* T) {
	TreeNode* node = T;
	StackNode* S = initStack();
	while (node || !isEmpty(S)) {
		// 寻找最左节点
		if (node) {
			push(node, S);
			node=node->lchild;
		}else {
			TreeNode* top = getTop(S)->data;
			// 有右节点且未被访问
			if (top->rchild&&top->rchild->flag==0) {
				top = top->rchild;
				// 入栈右节点
				push(top, S);
				// 节点是否有左节点
				node = top->lchild;
			} else {
				top = pop(S)->data;
				printf("%c ", top->data);
				// 已经访问过
				top->flag = 1;
			}
		}
	}
}


int main() {
	TreeNode* T;
	int index = 0;
	createTree(&T, "ABD##E##CF##G##", &index);
	preOrder(T);
	printf("\n");
	inOrder(T);
	printf("\n");
	postOrder(T);
	return 0;
}