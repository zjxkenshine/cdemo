#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
#include<stdlib.h>

typedef struct TreeNode {
	char data;
	struct TreeNode* lchild;
	struct TreeNode* rchild;
}TreeNode;

// 创建树，二级指针
void createTree(TreeNode** T,const char* data,int* index) {
	char ch;
	ch = data[*index];
	*index += 1;

	// 输入data
	//scanf("%c\n", &ch);
	if (ch == '#') {
		// 空节点
		*T = NULL;
	}else {
		// 非空节点
		// 函数参数必须为二级指针，否则这里无法赋值
		*T = (TreeNode*)malloc(sizeof(TreeNode));
		(*T)->data = ch;
		// 递归创建左右子树
		createTree(&((*T)->lchild), data, index);
		createTree(&((*T)->rchild), data, index);
	}
}

// preOrder 先序遍历
void preOrder(TreeNode* T) {
	if (T == NULL) {
		return;
	}else {
		// 根
		printf("%c ", T->data);
		// 左
		preOrder(T->lchild);
		// 右
		preOrder(T->rchild);
	}
}


// inOrder 中序遍历
void inOrder(TreeNode* T) {
	if (T == NULL) {
		return;
	}else {
		// 左
		preOrder(T->lchild);
		// 根
		printf("%c ", T->data);
		// 右
		preOrder(T->rchild);
	}
}

// postOrder 后序遍历
void postOrder(TreeNode* T) {
	if (T == NULL) {
		return;
	}else {
		// 左
		preOrder(T->lchild);
		// 右
		preOrder(T->rchild);
		// 根
		printf("%c ", T->data);
	}
}

// 需要把#也算上
int main() {
	TreeNode* T;
	int index = 0;
	createTree(&T,"AB##C##", &index);
	preOrder(T);
	printf("\n");
	inOrder(T);
	printf("\n");
	postOrder(T);
	return 0;
}

