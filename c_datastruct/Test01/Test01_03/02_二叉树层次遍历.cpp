#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>

// 树节点
typedef struct TreeNode {
	char data;
	struct TreeNode* lchild;
	struct TreeNode* rchild;
};

// 队列
typedef struct QueueNode {
	TreeNode* data;
	struct QueueNode* pre;
	struct QueueNode* next;
};


// 创建树
void createTree(TreeNode** T,const char* data, int* index) {
	char ch;
	ch = data[*index];
	*index += 1;
	if (ch == '#') {
		*T = NULL;
	}
	else {
		*T = (TreeNode*)malloc(sizeof(TreeNode));
		(*T)->data = ch;
		createTree(&((*T)->lchild), data, index);
		createTree(&((*T)->rchild), data, index);
	}
}

// 创建队列
QueueNode* initQueue() {
	QueueNode* Q = (QueueNode*)malloc(sizeof(QueueNode));
	Q->data = NULL;
	Q->next = Q;
	Q->pre = Q;
	return Q;
}

// 添加到队列
void enQueue(TreeNode* data,QueueNode* Q) {
	QueueNode* node = (QueueNode*)malloc(sizeof(QueueNode));
	node->data = data;
	node->pre = Q->pre;
	node->next = Q;
	Q->pre->next = node;
	Q->pre = node;
}

// 判断队列是否为空
int isEmpty(QueueNode* Q) {
	if (Q->next == Q) {
		return 1;
	}
	else {
		return 0;
	}
}

// 出队 Q是头结点
QueueNode* deQueue(QueueNode* Q) {
	if (isEmpty(Q)) {
		return NULL;
	}
	else {
		QueueNode* node = Q->next;
		Q->next->next->pre= Q;
		Q->next = Q->next->next;
		return node;
	}
}

// 层次遍历
void level(QueueNode* Q,TreeNode* T) {
	enQueue(T, Q);
	while (!isEmpty(Q)) {
		// 当前节点出队
		QueueNode* node = deQueue(Q);
		printf("%c ",node->data->data);
		// 左右子树入队
		if (node->data->lchild) {
			enQueue(node->data->lchild,Q);
		}
		if (node->data->rchild) {
			enQueue(node->data->rchild, Q);
		}
	}
}


// 先序遍历
void preOrder(TreeNode* T) {
	if (T == NULL) {
		return;
	}else {
		printf("%c ", T->data);
		preOrder(T->lchild);
		preOrder(T->rchild);
	}
}

// 测试
int main() {
	TreeNode* T;
	int index = 0;
	createTree(&T, "AB##C##", &index);
	// 先序遍历
	preOrder(T);
	printf("\n");
	// 层次遍历
	QueueNode* Q = initQueue();
	level(Q,T);
	return 0;
}