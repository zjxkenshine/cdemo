#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>

// 1.建立平衡二叉树的过程就是建立二叉排序树的过程
// 2.在建立过程中需要进行调整，需要添加一个字段表示 高度
// 3.调整方法

typedef struct TreeNode {
	int data;
	int height;
	struct TreeNode* lchild;
	struct TreeNode* rchild;
}TreeNode;

int getHeight(TreeNode* node) {
	return node ? node->height : 0;
}

int max(int a, int b) {
	return a > b ? a : b;
}

// RR 旋转 node父节点
void rrRotation(TreeNode* node, TreeNode** root) {
	// temp为要调整到根节点的节点
	// node为当前根节点
	TreeNode* temp = node->rchild;
	// 原来左孩子变为父亲右孩子
	node->rchild = temp->lchild;
	// 原来父亲变为左孩子
	temp->lchild = node;
	// 调整高度
	node->height = max(getHeight(node->lchild), getHeight(node->rchild))+1;
	temp->height = max(getHeight(temp->lchild), getHeight(temp->rchild))+1;
	// 改变根节点
	*root = temp;
}

// LL 旋转
void llRotation(TreeNode* node, TreeNode** root) {
	// 当前节点
	TreeNode* temp = node->lchild;
	// 当前节点右孩子变成父节点左孩子
	node->lchild = temp->rchild;
	// 当前节点右孩子变为父节点
	temp->rchild = node;
	// 高度调整
	node->height = max(getHeight(node->lchild), getHeight(node->rchild)) + 1;
	temp->height = max(getHeight(temp->lchild), getHeight(temp->rchild)) + 1;
	// 改变根节点
	*root = temp;
}

// 插入AVL树
void avlInsert(TreeNode** T, int data) {
	if (*T == NULL) {
		*T = (TreeNode*)malloc(sizeof(TreeNode));
		(*T)->data = data;
		(*T)->height = 0;
		(*T)->lchild = NULL;
		(*T)->rchild = NULL;
	}
	// 小于当前节点 插入左树
	else if(data<(*T)->data){
		avlInsert(&(*T)->lchild, data);
		// 在左树进行操作 L
		// 拿到当前节点左右子树高度
		int lHeight = getHeight((*T)->lchild);
		int rHeight = getHeight((*T)->rchild);
		// 高度差 数据比当前左树小，在子树左侧 LL
		if (lHeight - rHeight == 2) {
			if (data < (*T)->lchild->data) {
				// LL调整
				llRotation(*T, T);
			}
			// 否则在节点右侧 LR
			else {
				// LR调整
				// RR+LL
				rrRotation((*T)->lchild, &(*T)->lchild);
				llRotation(*T, T);
			}
		}
	}else if (data > (*T)->data) {
		avlInsert(&(*T)->rchild, data);
		// 在右树进行操作 R
		// 拿到当前节点左右子树高度
		int lHeight = getHeight((*T)->lchild);
		int rHeight = getHeight((*T)->rchild);
		// 高度差 数据比当前左树小，在子树左侧 RL
		if (rHeight - lHeight == 2) {
			if (data < (*T)->rchild->data) {
				// RL调整
				// 先LL
				llRotation((*T)->rchild, &(*T)->rchild);
				// 再RR
				rrRotation(*T, T);
			}
			// 否则在节点右侧 RR
			else {
				// RR调整
				rrRotation(*T, T);
			}
		}
	}
	// 当前树调整高度
	(*T)->height = max(getHeight((*T)->lchild),getHeight((*T)->rchild))+1;
}

// 先序遍历
void preOrder(TreeNode* T) {
	if (T) {
		printf("%d ", T->data);
		preOrder(T->lchild);
		preOrder(T->rchild);
	}
}

int main() {
	TreeNode* T = NULL;
	//int num[5] = { 1,2,3,4,5 };
	int num[5] = {5,4,3,2,1};
	for (int i = 0;i < 5;i++) {
		avlInsert(&T, num[i]);
	}
	preOrder(T);
	printf("NULL");
}