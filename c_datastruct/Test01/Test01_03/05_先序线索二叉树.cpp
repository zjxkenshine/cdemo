#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
#include<stdlib.h>

typedef struct TreeNode {
	char data;
	struct TreeNode* lchild;
	struct TreeNode* rchild;
	// 是否有左右节点的标志 0为有 1为没有
	int ltag;
	int rtag;
}TreeNode;

// 创建树，二级指针
void createTree(TreeNode** T, const char* data, int* index) {
	char ch;
	ch = data[*index];
	*index += 1;

	// 输入data
	//scanf("%c\n", &ch);
	if (ch == '#') {
		// 空节点
		*T = NULL;
	}
	else {
		// 非空节点
		// 函数参数必须为二级指针，否则这里无法赋值
		*T = (TreeNode*)malloc(sizeof(TreeNode));
		(*T)->data = ch;
		(*T)->ltag = 0;
		(*T)->rtag = 0;
		// 递归创建左右子树
		createTree(&((*T)->lchild), data, index);
		createTree(&((*T)->rchild), data, index);
	}
}

// 线索化  pre前一个遍历的节点
void preThreadTree(TreeNode* T, TreeNode** pre) {
	if (T) {
		// 没有左孩子，左孩子指向前一个前驱节点
		if (T->lchild == NULL) {
			T->ltag = 1;
			T->lchild = *pre;
		}
		// 前驱节点的右孩子指向该节点
		if (*pre != NULL && (*pre)->rchild == NULL) {
			(*pre)->rtag = 1;
			(*pre)->rchild = T;
		}
		*pre = T;
		// 有左孩子
		if (T->ltag == 0) {
			preThreadTree(T->lchild, pre);
		}
		preThreadTree(T->rchild, pre);
	}
}

// 找到下一个节点
TreeNode* getNext(TreeNode* T) {
	// 没有右节点
	if (T->rtag == 1) {
		return T->rchild;
	}
	else {
		// 有左节点，返回左子树
		if (T->ltag == 0) {
			return T->lchild;
		}
		else {
			// 左子树已经遍历，访问右节点
			return T->rchild;
		}
	}
}

int main() {
	TreeNode* T;
	TreeNode* pre = NULL;
	int index = 0;
	createTree(&T, "ABD##E##CF##G##", &index);
	// 线索化
	preThreadTree(T, &pre);
	// 最后一个节点
	pre->rtag = 1;
	pre->rchild = NULL;

	// 先序遍历
	for (TreeNode* node = T;node != NULL;node = getNext(node)) {
		printf("%c ", node->data);
	}
	printf("\n");
	return 0;
}