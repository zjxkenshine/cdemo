#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>

typedef struct Node {
	// 阶数
	int level;
	// 关键字数量
	int keyNum;
	// 孩子数量
	int childNum;
	// 关键字数组
	int* keys;
	// 孩子数组
	struct Node** children;
	// 父指针
	struct Node* parent;
}Node;

// 初始化
Node* initNode(int level) {
	Node* node = (Node*)malloc(sizeof(Node));
	node->level = level;
	node->keyNum = 0;
	node->childNum = 0;
	node->keys = (int*)malloc(sizeof(int) * (level + 1));
	node->children = (Node**)malloc(sizeof(Node*) * level);
	node->parent = NULL;
	int i;
	// 初始化关键字数组与孩子数组
	for (i = 0; i < level; i++) {
		node->keys[i] = 0;
		node->children[i] = NULL;
	}
	// 后续节点初始化
	node->keys[i] = 0;
	return node;
}

// 从结点中找到合适的插入位置
int findSuiteIndex(Node* node, int data) {
	int index;
	for (index = 1; index <= node->keyNum; index++) {
		// 比data大的索引位置
		if (data < node->keys[index]) {
			break;
		}
	}
	return index;
}

// 找到合适的叶子节点
Node* findSuiteLeafNode(Node* T, int data) {
	if (T->childNum == 0){
		return T;
	}else {
		int index = findSuiteIndex(T, data);
		// 往左边找
		return findSuiteLeafNode(T->children[index - 1], data);
	}
}

// 插入数据
void addData(Node* node, int data, Node** T) {
	int index = findSuiteIndex(node, data);
	// 找到最后一个元素索引
	for (int i = node->keyNum; i >= index; i--) {
		node->keys[i + 1] = node->keys[i];
	}
    // 关键数据
	node->keys[index] = data;
	node->keyNum++;
	// 判断是否进行分裂
    if (node->keyNum == node->level) {
        // 开始分裂，找到中间位置
        int mid = node->level / 2 + node->level % 2;
        // 初始化左孩子节点
        Node* lchild = initNode(node->level);
        // 初始化有孩子节点
        Node* rchild = initNode(node->level);
        // 将mid左边的值赋值给左孩子
        for (int i = 1; i < mid; i++) {
            addData(lchild, node->keys[i], T);
        }
        // 将mid右边的值赋值给右孩子
        for (int i = mid + 1; i <= node->keyNum; i++) {
            addData(rchild, node->keys[i], T);
        }
        // 将原先节点mid左边的孩子赋值给分裂出来的左孩子
        for (int i = 0; i < mid; i++) {
            // 左子树的孩子
            lchild->children[i] = node->children[i];
            // 修改孩子父指针
            if (node->children[i] != NULL) {
                node->children[i]->parent = lchild;
                lchild->childNum++;
            }
        }
        // 将原先节点mid右边的孩子赋值给分裂出来的右孩子
        int index = 0; // 右孩子插入位置从0开始
        for (int i = mid; i < node->childNum; i++) {
            // 右子树的孩子
            rchild->children[index++] = node->children[i];
            // 修改孩子父指针
            if (node->children[i] != NULL) {
                node->children[i]->parent = rchild;
                rchild->childNum++;
            }
        }


        // 对父亲进行操作
        //判断当前节点是不是根节点 仅在开始构建B树时存在
        if (node->parent == NULL) {
            // 是根节点 创建新节点替代
            Node* newParent = initNode(node->level);
            // 中间值赋值给该节点 
            addData(newParent, node->keys[mid], T);
            newParent->children[0] = lchild;
            newParent->children[1] = rchild;
            newParent->childNum = 2;
            lchild->parent = newParent;
            rchild->parent = newParent;
            // 修改根
            *T = newParent;
        }else {
            // 不是根节点
            // 找到节点索引
            int index = findSuiteIndex(node->parent, node->keys[mid]);
            // 修改左右子树父节点
            lchild->parent = node->parent;
            rchild->parent = node->parent;
            // 左孩子插到父节点
            node->parent->children[index - 1] = lchild;
            // 是否有index值，有的话将index连到右孩子
            // 先右移，再修改
            if (node->parent->children[index] != NULL) {
                for (int i = node->parent->childNum - 1; i >= index; i--) {
                    // 整体右移
                    node->parent->children[i + 1] = node->parent->children[i];
                }
            }
            // 右孩子
            node->parent->children[index] = rchild;
            node->parent->childNum++;
            addData(node->parent, node->keys[mid], T);
        }
    }
}

// 插入操作
void insert(Node** T, int data) {
    // 找到叶子节点
    Node* node = findSuiteLeafNode(*T, data);
    addData(node, data, T);
}

// 打印
void printTree(Node* T) {
    if (T != NULL) {
        // 关键字数组
        for (int i = 1; i <= T->keyNum; i++) {
            printf("%d ", T->keys[i]);
        }
        printf("\n");
        // 孩子数组
        for (int i = 0; i < T->childNum; i++) {
            printTree(T->children[i]);
        }
    }
}

// 查找
Node* find(Node* node, int data) {
    if (node == NULL) {
        return NULL;
    }
    for (int i = 1; i <= node->keyNum; i++) {
        if (data == node->keys[i]) {
            return node;
        }
        else if (data < node->keys[i]) {
            return find(node->children[i - 1], data);
        }
        else {
            if (i != node->keyNum && data < node->keys[i + 1])
                return find(node->children[i], data);
        }
    }
    return find(node->children[node->keyNum], data);
}


int main() {
    Node* T = initNode(5);
    insert(&T, 1);
    insert(&T, 2);
    insert(&T, 6);
    insert(&T, 7);
    insert(&T, 11);
    insert(&T, 4);
    insert(&T, 8);
    insert(&T, 13);
    insert(&T, 10);
    insert(&T, 5);
    insert(&T, 17);
    insert(&T, 9);
    insert(&T, 16);
    insert(&T, 20);
    insert(&T, 3);
    insert(&T, 12);
    insert(&T, 14);
    insert(&T, 18);
    insert(&T, 19);
    insert(&T, 15);
    printTree(T);
    Node* node = find(T, 7);
    if (node) {
        for (int i = 1; i <= node->keyNum; i++) {
            printf("%d ", node->keys[i]);
        }
        printf("\n");
    }
	return 0;
}