#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>

typedef struct List {
	int* data;
	int length;
	int num;
}List;

// 初始化list
List* initList(int length) {
	List* list = (List*)malloc(sizeof(List));
	list->length = length;
	list->data = (int*)malloc(sizeof(int)*length);
	list->num = 0;
	return list;
}

// 添加
void listAdd(List* list,int data) {
	list->data[(list->num)] = data;
	list->num = (list->num) + 1;
}

// 打印
void printList(List* list) {
	for (int i = 0; i < list->num;i++) {
		printf("%d ->", list->data[i]);
	}
	printf("NULL\n");
}

// 顺序查找
int search(List* list, int key) {
	for (int i = 0; i < list->num; i++) {
		if (list->data[i] == key) {
			return i;
		}
	}
	return -1;
}

int main() {
	List* list = initList(5);
	// 添加
	listAdd(list, 1);
	listAdd(list, 2);
	listAdd(list, 3);
	listAdd(list, 4);
	printList(list);
	// 查找
	printf("%d\n", search(list, 1));
	printf("%d\n", search(list, 5));
	return 0;
}