#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>

typedef struct List {
	int* data;
	int length;
	int num;
}List;

// 初始化list
List* initList(int length) {
	List* list = (List*)malloc(sizeof(List));
	list->length = length;
	list->data = (int*)malloc(sizeof(int) * length);
	list->data[0] = 0;
	// 第一个元素为哨兵，后面为数据
	list->num = 1;
	return list;
}

// 添加
void listAdd(List* list, int data) {
	list->data[(list->num)] = data;
	list->num = (list->num) + 1;
}

// 打印
void printList(List* list) {
	for (int i = 0; i < list->num; i++) {
		printf("%d ->", list->data[i]);
	}
	printf("NULL\n");
}

// 顺序查找 带哨兵
int search(List* list, int key) {
	int i = 0;
	// 哨兵变为key
	list->data[0] = key;
	printList(list);
	// 从最后一个元素开始
	for (i = (list->num) - 1; list->data[i] != key; i--);
	// i=0为不存在，否则存在
	return i;	
}

int main() {
	List* list = initList(5);
	// 添加
	listAdd(list, 1);
	listAdd(list, 2);
	listAdd(list, 3);
	listAdd(list, 4);
	printList(list);
	printf("%d\n", search(list, 1));
	return 0;
}