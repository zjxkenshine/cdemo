#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>

typedef struct String {
	char* data;
	int len;
}String;

// 初始化字符串
String* initString() {
	String* s = (String*)malloc(sizeof(String));
	s->data = NULL;
	s->len = 0;
	return s;
}

// 添加字符串
void stringAssign(String* s, char* data) {
	if (s->data) {
		free(s->data);
	}
	int len = 0;
	char* temp = data;
	// 字符串长度
	while (*temp) {
		len++;
		temp++;
	}
	if (len == 0) {
		s->data = NULL;
		s->len = 0;
	}
	else {
		temp = data;
		s->len = len;
		// 保存\0,开辟len+1的空间
		s->data = (char*)malloc(sizeof(char) * (len + 1));
		for (int i = 0;i < len;i++, temp++) {
			s->data[i] = *temp;
;		}
	}
}

// 输出字符串
void printString(String* s) {
	for (int i = 0;i < s->len;i++) {
		printf(i == 0 ? "%c " : "-> %c ", s->data[i]);
	}
	printf("\n");
}


// 获取next值
int* getNext(String* s) {
	int* next = (int*)malloc(sizeof(int) * s->len);
	int i = 0;
	int j = -1;
	next[i] = j;
	while (i < s->len - 1) {
		if (j == -1 || s->data[i] == s->data[j]) {
			i++;
			j++;
			next[i] = j;
		}
		else {
			j = next[j];
		}
	}
	return next;
}

// 打印next数组
void printNext(int* next, int len) {
	for (int i = 0;i < len;i++) {
		printf(i == 0 ? "%d " : "-> %d ", next[i]+1);
	}
	printf("\n");
}

// kmp匹配
void kmpMatch(String* master, String* sub, int* next) {
	int i = 0;
	// 子串索引
	int j = 0;
	while (i < master->len && j < sub->len) {
		// 相等
		if (j==-1||master->data[i]==sub->data[j]) {
			i++;
			j++;
		}
		else {
			j = next[j];
		}
	}
	if (j == sub->len) {
		printf("kmp match success.\n");
	}
	else {
		printf("kmp match fail.\n");
	}
}

int main() {
	String* s = initString();
	String* s1 = initString();
	stringAssign(s, "BABABCD");
	printString(s);
	stringAssign(s1, "ABCD");
	printString(s1);

	int* next = getNext(s1);
	printNext(next, s1->len);

	kmpMatch(s, s1, next);
	return 0;
}