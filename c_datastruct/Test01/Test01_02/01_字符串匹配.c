#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>

typedef struct String {
	char* data;
	int len;
}String;

// 初始化String
String* initString() {
	String* s = (String*)malloc(sizeof(String));
	s->data = NULL;
	s->len = 0;
	return s;
}

// 字符串赋值函数
void stringAssign(String* s,char* data) {
	// 有值
	if (s->data) {
		free(s->data);
	}
	int len = 0;
	char* temp = data;
	// 直到出现\0
	while (*temp) {
		len++;
		temp++;
	}
	if (len == 0) {
		s->data = NULL;
		s->len = 0;
	} else {
		temp = data;
		s->len = len;
		// 长度为len+1 保存'\0'
		s->data = (char*)malloc(sizeof(char)*(len+1));
		for (int i = 0;i < len;i++,temp++) {
			s->data[i] = *temp;
		}
	}
}

// 输出字符串
void printString(String* s) {
	for (int i = 0;i < s->len;i++) {
		printf(i == 0 ? "%c " : "-> %c ", s->data[i]);
	}
	printf("\n");
}

// 暴力匹配
void forceMatch(String* master, String* sub) {
	int i = 0;
	int j = 0;
	while (i < master->len&&j<sub->len) {
		if (master->data[i] == sub->data[j]) {
			i++;
			j++;
		}
		else {
			i = i - j + 1;
			j = 0;
		}
	}
	if (j == sub->len) {
		printf("force match success.\n");
	}
	else {
		printf("force match fail.\n");
	}
}

int main() {
	// 一定要加括号，否则会报异常
	String* s = initString();
	String* s1 = initString();
	stringAssign(s,"HELLO World");
	stringAssign(s1, "HELLO");
	printString(s);
	// 暴力匹配
	forceMatch(s, s1);
	return 0;
}

