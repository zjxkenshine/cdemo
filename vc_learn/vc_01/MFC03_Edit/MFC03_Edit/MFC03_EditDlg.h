﻿
// MFC03_EditDlg.h: 头文件
//

#pragma once


// CMFC03EditDlg 对话框
class CMFC03EditDlg : public CDialog
{
// 构造
public:
	CMFC03EditDlg(CWnd* pParent = nullptr);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MFC03_EDIT_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	CEdit m_Edit;
	afx_msg void OnBnClickedButton2();
	afx_msg void OnEnChangeEdit1();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
	CComboBox m_Combobox;
	afx_msg void OnBnClickedButton5();
	afx_msg void OnBnClickedButton6();
	afx_msg void OnBnClickedButton7();
	afx_msg void OnBnClickedButton8();
	afx_msg void OnCbnSelchangeCombo1();
	CComboBox m_Combobox2;
	CComboBox m_Combobox1;
	CListBox m_ListBox;
	afx_msg void OnBnClickedButton9();
	afx_msg void OnBnClickedButton10();
	afx_msg void OnBnClickedButton11();
	afx_msg void OnBnClickedButton12();
	afx_msg void OnBnClickedButton13();
	CStatic m_PicImage;
	afx_msg void OnBnClickedButton15();
	afx_msg void OnBnClickedButton14();
	CStatic m_PicImage1;
	CStatic m_PicImage2;
	afx_msg void OnBnClickedButton16();
};
