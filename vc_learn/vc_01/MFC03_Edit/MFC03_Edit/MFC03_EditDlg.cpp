﻿
// MFC03_EditDlg.cpp: 实现文件
//

#include "pch.h"
#include "framework.h"
#include "MFC03_Edit.h"
#include "MFC03_EditDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnEnChangeEdit1();
};

CAboutDlg::CAboutDlg() : CDialog(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CMFC03EditDlg 对话框



CMFC03EditDlg::CMFC03EditDlg(CWnd* pParent /*=nullptr*/)
	: CDialog(IDD_MFC03_EDIT_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMFC03EditDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, m_Edit);
	DDX_Control(pDX, IDC_COMBO2, m_Combobox);
	DDX_Control(pDX, IDC_COMBO3, m_Combobox2);
	DDX_Control(pDX, IDC_COMBO1, m_Combobox1);
	DDX_Control(pDX, IDC_LIST1, m_ListBox);
	DDX_Control(pDX, IDC_STATIC2, m_PicImage);
	DDX_Control(pDX, IDC_STATIC3, m_PicImage1);
	DDX_Control(pDX, IDC_STATIC4, m_PicImage2);
}

BEGIN_MESSAGE_MAP(CMFC03EditDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CMFC03EditDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CMFC03EditDlg::OnBnClickedButton2)
	ON_EN_CHANGE(IDC_EDIT1, &CMFC03EditDlg::OnEnChangeEdit1)
	ON_BN_CLICKED(IDC_BUTTON3, &CMFC03EditDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &CMFC03EditDlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON5, &CMFC03EditDlg::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON6, &CMFC03EditDlg::OnBnClickedButton6)
	ON_BN_CLICKED(IDC_BUTTON7, &CMFC03EditDlg::OnBnClickedButton7)
	ON_BN_CLICKED(IDC_BUTTON8, &CMFC03EditDlg::OnBnClickedButton8)
	ON_CBN_SELCHANGE(IDC_COMBO1, &CMFC03EditDlg::OnCbnSelchangeCombo1)
	ON_BN_CLICKED(IDC_BUTTON9, &CMFC03EditDlg::OnBnClickedButton9)
	ON_BN_CLICKED(IDC_BUTTON10, &CMFC03EditDlg::OnBnClickedButton10)
	ON_BN_CLICKED(IDC_BUTTON11, &CMFC03EditDlg::OnBnClickedButton11)
	ON_BN_CLICKED(IDC_BUTTON12, &CMFC03EditDlg::OnBnClickedButton12)
	ON_BN_CLICKED(IDC_BUTTON13, &CMFC03EditDlg::OnBnClickedButton13)
	ON_BN_CLICKED(IDC_BUTTON15, &CMFC03EditDlg::OnBnClickedButton15)
	ON_BN_CLICKED(IDC_BUTTON14, &CMFC03EditDlg::OnBnClickedButton14)
	ON_BN_CLICKED(IDC_BUTTON16, &CMFC03EditDlg::OnBnClickedButton16)
END_MESSAGE_MAP()


// CMFC03EditDlg 消息处理程序

BOOL CMFC03EditDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CMFC03EditDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CMFC03EditDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CMFC03EditDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


// 设置编辑框内容
void CMFC03EditDlg::OnBnClickedButton1(){
	SetDlgItemText(IDC_EDIT1,_T("11111"));
	m_Edit.SetWindowText(_T("2222"));

	// 编辑框窗口句柄
	HWND hEdit = ::GetDlgItem(GetSafeHwnd(), IDC_EDIT1);
	//调用全局方法
	::SetWindowText(hEdit, _T("3333"));
}


// 获取编辑框内容
void CMFC03EditDlg::OnBnClickedButton2(){
	CString str;
	//GetDlgItemText(IDC_EDIT1, str);
	//m_Edit.GetWindowText(str);
	//MessageBox(str);

	TCHAR szMsg[256];
	HWND hEdit = ::GetDlgItem(GetSafeHwnd(), IDC_EDIT1);
	::GetWindowText(hEdit, szMsg,256);
	MessageBox(szMsg);

}


void CMFC03EditDlg::OnEnChangeEdit1()
{
	// TODO:  如果该控件是 RICHEDIT 控件，它将不
	// 发送此通知，除非重写 CDialog::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。

		// 可以判断内容是否是合法的
	CString str;
	GetDlgItemText(IDC_EDIT1, str);
	for (int idx = 0; idx < str.GetLength(); ++idx) {
		// 替换 不显示其他字符
		if (str[idx] != 'A' && str[idx] != 'B' && str[idx] != 'C') {
			str.SetAt(idx, '#');
		}
	}
	str.Replace(_T("#"), _T(""));
	// 设置
	//m_Edit.SetWindowText(str);
}


void CMFC03EditDlg::OnBnClickedButton3(){
	//追加数据
	TCHAR szMsg[] = _T("hello world. \r\n");
	int iLen = m_Edit.GetWindowTextLength();
	// 光标设置到文字结尾
	m_Edit.SetSel(iLen, iLen, TRUE);
	// 替换
	m_Edit.ReplaceSel(szMsg, FALSE);
}


// 代码方式向下拉框添加数据
void CMFC03EditDlg::OnBnClickedButton4(){
	m_Combobox.AddString(_T("11111"));
	m_Combobox.AddString(_T("00000"));
	m_Combobox.AddString(_T("22222"));

	// 添加到指定位置
	m_Combobox.InsertString(0,_T("66666"));
}

// 删除下拉框数据
void CMFC03EditDlg::OnBnClickedButton5(){
	// 删除指定索引的值
	m_Combobox.DeleteString(1);
	// 删除所有值
	m_Combobox.ResetContent();
}


// 选中下拉框数据
void CMFC03EditDlg::OnBnClickedButton6(){
	// 取消选中
	m_Combobox.SetCurSel(-1);
	// 选中索引1
	m_Combobox.SetCurSel(1);
}


// 下拉框获取选中索引
void CMFC03EditDlg::OnBnClickedButton7(){
	int cur_sel = m_Combobox.GetCurSel();
	CString s;
	s.Format(_T("%d"), cur_sel);
	MessageBox(s);
}


// 下拉框获取选中数据
void CMFC03EditDlg::OnBnClickedButton8(){
	CString szText;
	int cur_sel = m_Combobox.GetCurSel();
	// 获取指定索引的值
	m_Combobox.GetLBText(cur_sel, szText);
	MessageBox(szText);
}


// 下拉框改变时
void CMFC03EditDlg::OnCbnSelchangeCombo1(){
	CString szText;
	int cur_sel = m_Combobox1.GetCurSel();
	// 获取指定索引的值
	m_Combobox1.GetLBText(cur_sel, szText);
	m_Combobox2.ResetContent();	
	if (szText == _T("浙江省")) {
		m_Combobox2.AddString(_T("杭州市"));
		m_Combobox2.AddString(_T("温州市"));
	}
	else if (szText == _T("山东省")) {
		m_Combobox2.AddString(_T("济南市"));
		m_Combobox2.AddString(_T("青岛市"));
	}
}


// 列表框添加数据
void CMFC03EditDlg::OnBnClickedButton9(){
	// 添加数据
	m_ListBox.AddString(_T("111"));
	m_ListBox.AddString(_T("222"));
	m_ListBox.AddString(_T("333"));

	// 插入到某个位置
	m_ListBox.InsertString(0,_T("000"));
}


// 获取列表框条数 并删除数据 选中条目
void CMFC03EditDlg::OnBnClickedButton10(){
	int count = m_ListBox.GetCount();
	CString s;
	s.Format(_T("总共%d条记录"), count);
	MessageBox(s);

	m_ListBox.DeleteString(0);
	// 删除所有条目
	//m_ListBox.ResetContent();

	// 选中条目
	m_ListBox.SetCurSel(1);
}

// 获得选中数据 单选
void CMFC03EditDlg::OnBnClickedButton11(){
	// 选中数据
	int idx = m_ListBox.GetCurSel();
	CString s;
	s.Format(_T("选中了第%d条记录"), idx);
	MessageBox(s);
}

// 多选
void CMFC03EditDlg::OnBnClickedButton12(){
	m_ListBox.SetSel(0);
	m_ListBox.SetSel(1);
	// 不选中
	m_ListBox.SetSel(2,false);
}


// 获得多选数据
void CMFC03EditDlg::OnBnClickedButton13(){
	// 选中总数
	int sel_count = m_ListBox.GetSelCount();
	if (sel_count > 0) {
		int* pSel = new int[sel_count];
		// 数量，指针
		m_ListBox.GetSelItems(sel_count, pSel);
		for (int idx = 0; idx < sel_count; ++idx) {
			int sel_idx = pSel[idx];
			// 其他操作
			CString s;
			s.Format(_T("选中了第%d条记录"), sel_idx);
			MessageBox(s);
		}

		// 获取第2条数据
		int text_len = m_ListBox.GetTextLen(2);
		TCHAR* pszText = new TCHAR[text_len+1];
		// 清空
		memset(pszText, 0, text_len);
		// 获取数据
		m_ListBox.GetText(2, pszText);
		MessageBox(pszText);
		delete[] pszText;
		delete[] pSel;
	}

}

// 设置bitmap
void CMFC03EditDlg::OnBnClickedButton15(){
	CBitmap m_bmp;
	// 获取bitmap句柄
	bool load=m_bmp.LoadBitmap(IDB_BITMAP1);
	if (load) {
		// 设置bitmap
		HBITMAP hRet = m_PicImage.SetBitmap((HBITMAP)m_bmp);
	}
}

// 从磁盘设置bitmap
void CMFC03EditDlg::OnBnClickedButton14() {
	HBITMAP hbitmap = (HBITMAP)::LoadImage(NULL, _T("F:\\Cdemo\\vc_learn\\vc_01\\MFC03_Edit\\MFC03_Edit\\res\\test.bmp"), IMAGE_BITMAP, 0,0, LR_DEFAULTCOLOR
	|LR_DEFAULTSIZE|LR_LOADFROMFILE);
	m_PicImage1.SetBitmap(hbitmap);
}


// CImage添加jpg\png格式图片
void CMFC03EditDlg::OnBnClickedButton16(){
	CImage mImg;
	HRESULT hr = mImg.Load(_T("F:\\Cdemo\\vc_learn\\vc_01\\MFC03_Edit\\MFC03_Edit\\res\\test01.jpg"));
	if (hr == S_OK) {
		HBITMAP hb = mImg.Detach();
		m_PicImage2.SetBitmap(hb);
	}
}
