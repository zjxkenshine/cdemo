# 参考地址

【实用VC编程】之玩转控件

- https://www.cctry.com/forum-142-1.html
- https://www.bilibili.com/video/BV16t411e7GY

# 0.目录

- MFC01_Button ：Button按钮
- MFC02_Radio_Check：单选框、复选框
- MFC03_Edit ：
  - Edit 文本框
  - ComboBox下拉框
  - ListBox 列表框
  - GroupBox 分组框
  - StaticText 静态文本框
  - Picture 图像控件
- MFC04Test:
  - Spin 数值调节控件
  - Slider 滑块控件
  - Progress 进度条控件



# 1.Windows编程简介

Windows程序分类：exe的可执行程序，dll的动态链接库程序，lib的静态库程序，sys的驱动类型程序

exe程序分类：

- 控制台程序，无界面，入口点函数是main
- 非控制台类型的程序，入口函数式WinMain

相关概念：

- Windows API：Windows操作系统应用程序接口，有非正式的简称法为WinAPI

基本win32类型资源文件：非常麻烦，没法自动拖拽

解决win32工程麻烦的方法：MFC、QT

- MFC：微软公共类库，支持良好，vs使用向导进行创建，但是比较麻烦，不能跨平台，美化程度不高
- QT：跨平台的C++应用程序开发框架，应用广泛，体积稍大，执行效率没有MFC高

## 相关控件

Button        按钮控件
CheckBox    复选框控件
Edit            编辑框控件
ComboBox    下拉控件
ListBox        列表控件
GroupBox    分组控件
Radio Button    单选框控件
Static Text    静态文本框控件
Picture Control    图像控件
Slider Control    滑块控件
Spin Control    数值调节钮控件
Progress Control    进度条控件
List Control    高级列表控件
Tree Control    树形控件
Tab Control    标签控件

# 2.VS创建MFC应用

- C++，windows，桌面筛选一下就会出来

- cannot open include file ‘afxres.h‘：https://blog.csdn.net/llx20001212/article/details/128074925

- 工作中遇到的各种VC错误处理方法：https://blog.csdn.net/anhuizhj/article/details/126605875

- 创建完成后一些类：
  - 主线程类：CXXXApp
  - 对的画框类：CXXXDlg

- 初始化函数：`BOOL CXXXDlg::OnInitDialog()`

- 资源视图：`IDD_XXX_DIALOG` 为主对话框资源

- 窗口句柄：HWND 用于标识窗口变量，实际上是个指针

# 3.Button 按钮

## 3.1 添加

- 双击模版进行添加；
- 右键事件方式进行添加；

## 3.2 删除

要删除3个地方：

- h头文件中的函数声明；
- cpp文件中的函数定义；
- cpp文件中的ON_BN_CLICKED宏绑定代码。

## 3.3 常用选项

Disabled、ID、Visable

## 3.4 按钮启用与禁用

代码控制：

```
// 全局函数
HWND hBtn = ::GetDlgItem(m_hWnd, IDC_BUTTON1);
::EnableWindow(hBtn,false);
// MFC函数
CWnd* hBtn = GetDlgItem(IDC_BUTTON1);
hBtn->EnableWindow(true);
```

## 3.5 绑定控件变量（方便获取句柄）

右键—>添加控制变量，添加控件变量，可以在任意地方获取控件进行操作

## 3.6 控件的美化

微软也提供给我们控件的美化接口，那就是自绘，属性中的所有者描述改为true

- https://www.codeproject.com/
- https://blog.csdn.net/m0_72895175/article/details/132738673
- https://blog.csdn.net/dz131lsq/article/details/136840249

三态按钮：鼠标移开，放上去，点击各一种状态

使用方式：

- 从codeproject下载源文件，将.h与.cpp文件复制到工程中
- 关闭预编译头`stdafx`
- 添加`afxwin.h`
- 修改主工程`MFC01_ButtonDlg.h`中的CButton为添加进来的派生类(需要引入头文件)
  - 控件变量类型改为该类型
- 加载相关资源
- 调整自绘属性为true

# 4.单选框与复选框

## 3.1 单选框 radio button

相关代码：

```c++
// 选中方式1
CButton* pNan = (CButton*)GetDlgItem(IDC_RADIO2);		
if (pNan) {
	// 选中
	pNan->SetCheck(BST_CHECKED);
}
// 选中方式2
CheckRadioButton(IDC_RADIO3, IDC_RADIO5, IDC_RADIO3); 
// 获取选中值 (传入起始与结束按钮ID) 什么的都没选为0
int nID= GetCheckedRadioButton(IDC_RADIO3, IDC_RADIO5);
```

分组方式：

1. 要求一组内的单选框按钮控件ID必须由小到大排列，中间不能夹杂其他组的单选框按钮；(ID再Resource.h中定义，可以修改)
2. 若有多组，则组内第一个单选框按钮的Group属性为True，其他为False；
3. 选中组内某个单选框按钮：CheckRadioButton(IDC_RADIO1, IDC_RADIO3, IDC_RADIO2);
4. 获得当前当前选中的单选框按钮控件：int nCheckId = GetCheckedRadioButton(IDC_RADIO1, IDC_RADIO3);

## 3.2 复选框 check box

1. 勾选复选框控件：三种方法任意一种都可以！

   ```C++
   ::SendMessage(::GetDlgItem(m_hWnd, IDC_CHECK1), BM_SETCHECK, BST_CHECKED, 0);
   GetDlgItem(IDC_CHECK1)->SendMessage(BM_SETCHECK, BST_CHECKED, 0);
   m_Check.SetCheck(BST_CHECKED);
   ```

   - 备注：取消勾选的话只需要把 BST_CHECKED 改成 BST_UNCHECKED 就可以了！

2. 获取复选框的勾选状态：三种方法任意一种都可以！

   ```c++
   UINT nCheckState = ::SendMessage(::GetDlgItem(m_hWnd, IDC_CHECK1), BM_GETCHECK, 0, 0);
   nCheckState = GetDlgItem(IDC_CHECK1)->SendMessage(BM_GETCHECK, 0, 0);
   nCheckState = m_Check.GetCheck();
   ```

   - 如果返回值 nCheckState 的值为 BST_CHECKED，那么就是勾选状态。如果值为 BST_UNCHECKED，那么就是非勾选状态。

# 5.Edit 编辑框

## 5.1 设置编辑框内容

```c++
SetDlgItemText(IDC_EDIT1,_T("11111"));	// 方式1
m_Edit.SetWindowText(_T("11111"));	// 方式2 控件变量引用
// 方式3：窗口句柄，全局函数
HWND hEdit = ::GetDlgItem(GetSafeHwnd(), IDC_EDIT1);
::SetWindowText(hEdit, _T("11111"));
```

## 5.2 获取编辑框文本内容

```c++
CString str;
// 方式1 mfc方式
GetDlgItemText(IDC_EDIT1, str);
// 方式2 控件变量
m_Edit.GetWindowText(str);
MessageBox(str);
// 方式3 sdk窗口句柄
TCHAR szMsg[256];
HWND hEdit = ::GetDlgItem(GetSafeHwnd(), IDC_EDIT1);
::GetWindowText(hEdit, szMsg,256);
MessageBox(szMsg);
```

## 5.3 事件

 EN_CHANGE：更改后触发

## 5.4 光标定位到结尾

```c++
m_Edit.SetFocus();
m_Edit.SetSel(-1);
```

## 5.5 长度限制

```c++
UINT nLimitLen = m_Edit.GetLimitText(); // 长度
m_Edit.SetLimitText(0);	// 解除限制
```

## 5.6 向结尾增加数据

```c++
//追加数据
TCHAR szMsg[] = _T("hello world. \r\n");
int iLen = m_Edit.GetWindowTextLength();
// 光标设置到文字结尾
m_Edit.SetSel(iLen, iLen, TRUE);
// 替换
m_Edit.ReplaceSel(szMsg, FALSE)
```

# 6.ComboBox下拉框

## 6.1 向下拉框添加数据

1. 通过data属性，以;分割

2. 代码方式

   ```c++
   m_Combobox.AddString(_T("11111"));
   m_Combobox.InsertString(0,_T("66666"));
   ```

3. 默认会排序，可以将控件sort属性指定为false

## 6.2 删除下拉框数据

```c++
// 删除指定索引的值
m_Combobox.DeleteString(1);
// 删除所有值
m_Combobox.ResetContent();
```

## 6.3 设置下拉框选中某条数据

```c++
m_ComboBox.SetCurSel(1);
// 取消选中
m_Combobox.SetCurSel(-1);
```

## 6.4 获取当前选中

```c++
int idx = m_ComboBox.GetCurSel();
```

## 6.5 得到某一项的值

```c++
TCHAR szText[100] = { 0 };
CString szText;
m_ComboBox.GetLBText(idx, szText);
```

## 6.6 下拉框类型

- Dropdown：可以从下拉列表中选择一项，也可以自己输入；
- Drop List：只能从下拉列表中选择一项，不能修改。

## 6.7 控制下拉高度

- 控件模版中，点击一下 ComboBox 的下拉箭头，此时出现的调整框就是 ComboBox 的下拉框的高度；
- 控件属性中有个 No Integral Height 选项，表示最大高度为设计长度，如果实际内容比设计长度多，就出现滚动条，少就以实际长度显示。
  - 可以将该选项设置为默认的FALSE，即表示最大高度不是设计的高度，而是根据实际内容的高度来决定。

## 6.8 下拉框事件

响应 CBN_SELCHANGE 事件，在其响应函数中判断当前选中的是哪个

# 7.ListBox 列表控件

## 7.1 插入数据

```c++
m_ListBox.AddString(_T("111"));
m_ListBox.InsertString(0,_T("000"));
```

## 7.2 获取列表框条数

```c++
int nCount = m_ListBox.GetCount();
```

## 7.3 从列表框删除数据

```c++
m_ListBox.DeleteString(0);    //删除指定索引的数据
m_ListBox.ResetContent();    //删除全部数据
```

## 7.4 选中条目

```c++
m_ListBox.SetCurSel(1);
m_ListBox.SetCurSel(-1); // 不选中
// 多选 设置selection属性 为multiple
m_ListBox.SetSel(0);
m_ListBox.SetSel(2,false); // 不选中
```

## 7.5 获得列表框当前选中的是哪条数据

```c++
int idx =m_ListBox.GetCurSel(); // 单选数据
// 获取多选数据
int sel_count = m_ListBox.GetSelCount();
int* pSel = new int[sel_count];
m_ListBox.GetSelItems(sel_count, pSel); // 数量，指针
for (int idx = 0; idx < sel_count; ++idx) {
	int sel_idx = pSel[idx];
	// 其他操作
}
delete[] pSel;
```

## 7.6 获取选中的数据

```c++
// 获取第2条数据
int text_len = m_ListBox.GetTextLen(2);
TCHAR* pszText = new TCHAR[text_len+1];
// 初始化
memset(pszText, 0, text_len);
// 获取数据
m_ListBox.GetText(2, pszText);
MessageBox(pszText);
delete[] pszText;
```

## 7.7 Selection属性

- `Single`：单选
- `Multiple`：多选
- `Extended`：支持键盘扩展操作
- `None`：选中item，但是不高亮，只显示该item上交点

# 8.GroupBox 分组控件

将控件分到不同组中

# 9.StaticText 静态文本框

静态文本框

# 10.Picture 图像控件

## 10.1 Picture控件加载静态BMP图片资源

1. 向工程中插入一个BMP类型的图片资源，例如，ID为：IDC_BITMAP1
2. 在Picture控件的Type属性下拉框中选择Bitmap；
3. 在Picture控件的Image属性下拉框中选择BMP资源ID为IDC_BITMAP1即可。

Type属性下拉列表中有8种类型，下面分别介绍下：

- Frame：显示一个无填充的矩形框，边框颜色可以通过Color属性的下拉列表设定
- Etched Horz：显示一条横分割线
- Etched Vert：显示一条竖分割线
- Rectangle：显示一个填充的矩形框，矩形颜色可通过Color属性的下拉列表设定
- Icon：显示一个图标（Icon），图标通过Image下拉列表来设置图标资源ID
- Bitmap：显示一个位图（Bitmap），位图通过Image下拉列表来设置位图资源ID
- Enhanced Metafile：显示一个加强的元数据文件（Metafile）
- Owner Draw：自绘

## 10.2 以代码方式添加BMP资源

```c++
CBitmap m_bmp;
// 获取bitmap句柄
bool load=m_bmp.LoadBitmap(IDB_BITMAP1);
if (load) {
	// 设置bitmap
	HBITMAP hRet = m_PicImage.SetBitmap((HBITMAP)m_bmp);
}
```

## 10.3 Picture控件加载磁盘上面的BMP图片

```c++
HBITMAP hbitmap = (HBITMAP)::LoadImage(NULL, _T("F:\\Cdemo\\vc_learn\\vc_01\\MFC03_Edit\\MFC03_Edit\\res\\test.bmp"), IMAGE_BITMAP, 0,0, LR_DEFAULTCOLOR
|LR_DEFAULTSIZE|LR_LOADFROMFILE);
m_PicImage1.SetBitmap(hbitmap);
```

加载的图片在当前EXE的模块外

- 可以指定图片大小，如上图中的50, 50，若实际大小和此不相符，会自动缩放图片
  - LR_DEFAULTCOLOR — 指定按照原图的颜色加载图片，不可少
  - LR_LOADFROMFILE — 加载外部的Bitmap，一定要指定
  - LR_CREATEDIBSECTION — 一般会指定
  - 如若按照图片本身的大小加载，则设置cx,cy为0,0，并且在最后的标志位加上|LR_DEFAULTSIZE

## 10.4 Picture控件加载 png、jpg 等常用格式图片

- 默认情况下，Picture控件只能显示BMP类型的图像，因为BMP文件格式是微软自家的
- 那么Picture控件如何来显示我们常用的 png、jpg 等图像格式呢？这里要借助微软的ATL中的一个类：CImage
  - （VC6用不了，VC6可以考虑使用开源的 CxImage）

```c++
CImage mImg;
HRESULT hr = mImg.Load(_T("F:\\Cdemo\\vc_learn\\vc_01\\MFC03_Edit\\MFC03_Edit\\res\\test01.jpg"));
if (hr == S_OK) {
	HBITMAP hb = mImg.Detach();
	m_PicImage2.SetBitmap(hb);
}
```

# 11.Spin数值调节控件

## 11.1 简介

样子上是一对上下的箭头按钮，用户可单击它来增加或减少控件的设定值。通常，紧靠着Spin数值调节控件有一个编辑框控件

## 11.2 控件的使用

1. 分别拖拽一个Spin数值调节控件和一个编辑框控件到对话框界面上，挨着摆放；

2. 设置Spin数值调节控件的属性如下

   - Set Buddy Interger 设置合作整数(使控件设置关联控件数值，这个值可以是十进制或十六进制) - True
   - Wrap 换行(数值超过范围时循环) - True
   - Arrow keys 箭头键(当按下向上和向下方向键时，控件可以增加或减小) - True
     - Alignlent 对齐 - Right Alient  //右侧嵌入到编辑框中

3. 给Spin数值调节控件绑定一个 CSpinButtonCtrl 类型的控件类型变量 m_Spin；

4. 在对话框的初始化函数 `BOOL CMFCTestDlg::OnInitDialog()` 中设置 Spin数值调节控件：

   ```C++
   m_Spin.SetRange32(1,10); //范围
   m_Spin.SetBase(10); // 十进制
   // 设置编辑框
   m_Spin.SetBuddy(GetDlgItem(IDC_EDIT1));
   // 默认值
   m_Spin.SetPos(1);
   ```

# 12.Slider 滑块控件

## 12.1 简介

- Slider滑块控件，也是VC中比较常用的一个控件。一般而言它是由一个滑动条，一个滑块和可选的刻度组成。用户可以通过移动滑块在相应的控件中显示对应的值。在滑动控件附近一定有静态文本框控件或编辑框控件，用于显示相应的值

## 12.2 使用

1. 拖拽Slider滑块控件到对话框界面上，调整其大小！
2. 给Slider控件绑定一个CSliderCtrl类型的控件类型变量 m_Slider；
3. Slider控件属性：
   - Tick Marks 打勾标记： 为是否显示刻度线
   - Auto Ticks 自动打勾：是否显示每个增量的刻度线

## 12.3 相关操作

```c++
m_Slider.SetRange(0, 100); //设置移动范围
m_Slider.SetPos(50); //设置当前位置
int pos = m_Slider.GetPos(); //获取当前位置
// 键盘操作,按一下左或右箭头时滑块的偏移量
int ret = m_Slider.SetLineSize(1);
ret = m_Slider.GetLineSize();
// 键盘操作，获取和设置当按下PgUp或PgDown时滑块的移动量
ret = m_Slider.SetPageSize(10);
ret = m_Slider.GetPageSize();
```

## 12.4 显示滑块值

使用编辑框或静态文本框控件： OnNMCustomdrawSlider1事件

```c++
// 获取滑块值
int pos = m_Slider.GetPos();
CString strText;
strText.Format(_T("%d"), pos);
// 设置静态文本框值
SetDlgItemText(IDC_STATIC1, strText);
```

# 13.Progress 进度条控件

## 13.1 进度条的使用

1. 拖拽Progress进度条控件到对话框界面上，调整其大小！
2. 给Progress进度条控件绑定一个CProgressCtrl类型的控件类型变量m_ProgressCtrl；
3. Progress控件属性中的选项

## 13.2 进度条的相关操作

```c++
m_Progress.SetRange32(0, 100); // 滑动范围
m_Progress.SetStep(1); //步进长度
m_Progress.SetPos(50); //当前位置
int pos = m_Progress.GetPos(); //获取当前位置
m_Progress.StepIt();//步进一次
```

## 13.3 定时器控制

- 为对话框类添加 WM_TIMER 定时器消息的响应函数；
  - (主对话框右键->消息)
- 在响应函数中添加：`m_ProgressCtrl.StepIt();` 步进；
- 在对话框的初始化函数中启动定时器：`SetTimer(1, 500, NULL); //500ms 执行一次`

# 14.Tab标签控件

## 14.1 简介

一个对话框界面的空间有限，如果需要放置很多控件的话恐怕摆不下而且也比较乱，这时候就可以使用 TabCtrl 标签页控件了。每个标签页下都可以作为一个单独的页面，对控件进行分组摆放。TabCtrl 控件也是其他很多软件经常用的一个控件。

## 14.2 标签页使用

添加子对话框，选中某个标签，其他子对话框隐藏

踩坑：添加了.h文件与.c文件，识别不到.h文件

- `#include "pch.h"`，要放在最前面

## 14.3 使用步骤

1. 将 TabSheet.h 和 TabSheet.cpp 这两个文件放到 MFCTest 工程目录下，并导入到vs工程中；

2. 拖拽Tab控件到对话框模版上；

3. 为Tab控件绑定一个 CTabCtrl 控件类型变量m_Tab；

4. 对话框类头文件中的 CTabCtrl m_Tab; 声明更改为：CTabSheet m_Tab; 即更改控件变量的类型，如果编译不通过，需要包含 TabSheet.h；

5. 添加一个对话框资源作为第一子页，并绑定派生自 CDialog 的子对话框类：CPage1；

6. 修改 CPage1 的对话框资源属性：Border：None，Style：Child

7. 在主对话框类声明中加入第一子页的实例化对象：CPage1 m_page_1；

8. 在主对话框的初始化函数中加入加载子页的代码：

   ```
   m_Tab.AddPage(_T("第一页"), &m_page_1, IDD_DIALOG1);
   m_Tab.Show();
   ```

9. 添加第二个子页，以此类推！
