#pragma once
#include <afxwin.h>
// CTabSheet
#define MAX_PAGES 30

class CTabNode
{
public:
	CTabNode()
	{
		m_DlgID = 0;
		m_pDialog = NULL;
	};
	~CTabNode(){};

public:
	UINT m_DlgID;
	CDialog* m_pDialog;
	CString m_strTitle;
};

class CTabSheet : public CTabCtrl
{
	DECLARE_DYNAMIC(CTabSheet)

public:
	CTabSheet();
	virtual ~CTabSheet();

	BOOL AddPage(CString strTitle, CDialog* pDialog, UINT nID);
	BOOL AddPage(CString strTitle, CRuntimeClass* pPageClass, UINT nID); //Ҫ���У�DECLARE_DYNCREATE(CPage1)��IMPLEMENT_DYNCREATE(CPage1, CDialog)
	CDialog* GetPage(UINT nIdx);
	void Show(UINT nIdx = 0);

protected:
	DECLARE_MESSAGE_MAP()

private:
	UINT m_nTotalPages;
	UINT m_nCurrentPage;
	CTabNode m_ArrPages[MAX_PAGES];
	void SetRect(UINT nIdx);
	afx_msg void OnTcnSelchange(NMHDR *pNMHDR, LRESULT *pResult);
};


