﻿
// MFC04TestDlg.h: 头文件
//

#pragma once
#include "TabSheet.h"
#include "CPage1.h"
#include "CPage2.h"

// CMFC04TestDlg 对话框
class CMFC04TestDlg : public CDialog
{
// 构造
public:
	CMFC04TestDlg(CWnd* pParent = nullptr);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MFC04TEST_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnEnChangeEdit1();
	CSpinButtonCtrl m_Spin;
	CSliderCtrl m_Slider;
	afx_msg void OnNMCustomdrawSlider1(NMHDR* pNMHDR, LRESULT* pResult);
	CProgressCtrl m_Progress;
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	//CTabCtrl m_Tab;
	CTabSheet m_Tab; // 自定义控件
	// TAB控件子对话框
	CPage1 m_cpage1;
	CPage2 m_cpage2;
};
