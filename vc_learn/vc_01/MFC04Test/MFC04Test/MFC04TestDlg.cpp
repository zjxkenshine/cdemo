﻿
// MFC04TestDlg.cpp: 实现文件
//

#include "pch.h"
#include "framework.h"
#include "MFC04Test.h"
#include "MFC04TestDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CMFC04TestDlg 对话框



CMFC04TestDlg::CMFC04TestDlg(CWnd* pParent /*=nullptr*/)
	: CDialog(IDD_MFC04TEST_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMFC04TestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SPIN1, m_Spin);
	DDX_Control(pDX, IDC_SLIDER1, m_Slider);
	DDX_Control(pDX, IDC_PROGRESS1, m_Progress);
	DDX_Control(pDX, IDC_TAB1, m_Tab);
}

BEGIN_MESSAGE_MAP(CMFC04TestDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_EN_CHANGE(IDC_EDIT1, &CMFC04TestDlg::OnEnChangeEdit1)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER1, &CMFC04TestDlg::OnNMCustomdrawSlider1)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CMFC04TestDlg 消息处理程序

BOOL CMFC04TestDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// 在此添加额外的初始化代码
	// 初始化spin调节框
	m_Spin.SetRange32(1,10); //范围
	m_Spin.SetBase(10); // 十进制
	// 设置编辑框
	m_Spin.SetBuddy(GetDlgItem(IDC_EDIT1));
	// 默认值
	m_Spin.SetPos(1);

	// slider控件初始化
	m_Slider.SetRange(1, 10);	//范围
	m_Slider.SetPos(4); //初始值
	m_Slider.SetLineSize(1); // 键盘移动一格
	m_Slider.SetPageSize(2); // 换页移动两格

	// 进度条处理
	m_Progress.SetRange32(0, 100); // 滑动范围
	m_Progress.SetStep(1); //步进长度
	m_Progress.SetPos(50); //当前位置
	int pos = m_Progress.GetPos(); //获取当前位置
	m_Progress.StepIt();//步进一次

	// 启动定时器
	// ID 间隔
	SetTimer(1, 500, NULL);

	// tab页设置
	//m_Tab.InsertItem(0, __T("第一页"));
	//m_Tab.InsertItem(1, __T("第二页"));
	// 自定义tab类设置
	m_Tab.AddPage(_T("第一页"), &m_cpage1, IDD_DIALOG1);
	m_Tab.AddPage(_T("第二页"), &m_cpage2, IDD_DIALOG2);
	m_Tab.Show();


	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CMFC04TestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CMFC04TestDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CMFC04TestDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CMFC04TestDlg::OnEnChangeEdit1(){

}

// 滑块滑动处理
void CMFC04TestDlg::OnNMCustomdrawSlider1(NMHDR* pNMHDR, LRESULT* pResult){
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// 获取滑块值
	int pos = m_Slider.GetPos();
	CString strText;
	strText.Format(_T("%d"), pos);
	// 设置静态文本框值
	SetDlgItemText(IDC_STATIC1, strText);
	*pResult = 0;
}


// 定时器
void CMFC04TestDlg::OnTimer(UINT_PTR nIDEvent){
	// 步进一格
	m_Progress.StepIt();
	CDialog::OnTimer(nIDEvent);
}
