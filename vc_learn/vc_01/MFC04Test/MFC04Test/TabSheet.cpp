// TabSheet.cpp : implementation file
//

#include "pch.h"
#include "TabSheet.h"

// CTabSheet
IMPLEMENT_DYNAMIC(CTabSheet, CTabCtrl)

CTabSheet::CTabSheet()
{
	m_nTotalPages = 0;
	m_nCurrentPage = 0;
}

CTabSheet::~CTabSheet()
{
}

BEGIN_MESSAGE_MAP(CTabSheet, CTabCtrl)
	ON_NOTIFY_REFLECT(TCN_SELCHANGE, &CTabSheet::OnTcnSelchange)
END_MESSAGE_MAP()

// CTabSheet message handlers
BOOL CTabSheet::AddPage(CString strTitle, CDialog* pDialog, UINT nID)
{
	if (MAX_PAGES <= m_nTotalPages) return FALSE;
	if (strTitle.IsEmpty() || !pDialog || nID <= 0) return FALSE;

	++m_nTotalPages;
	CTabNode& tab_node = m_ArrPages[m_nTotalPages - 1];
	tab_node.m_pDialog = pDialog;
	tab_node.m_DlgID = nID;
	tab_node.m_strTitle = strTitle;

	return TRUE;
}

BOOL CTabSheet::AddPage(CString strTitle, CRuntimeClass* pPageClass, UINT nID)
{
	if (MAX_PAGES <= m_nTotalPages) return FALSE;
	if (!pPageClass || !pPageClass->IsDerivedFrom(RUNTIME_CLASS(CDialog))) return FALSE;

	CDialog* pDlg = (CDialog*)pPageClass->CreateObject();
	if (!pDlg) return FALSE;

	++m_nTotalPages;
	CTabNode& tab_node = m_ArrPages[m_nTotalPages - 1];
	tab_node.m_pDialog = pDlg;
	tab_node.m_DlgID = nID;
	tab_node.m_strTitle = strTitle;

	return TRUE;
}

CDialog* CTabSheet::GetPage(UINT nIdx)
{
	if (nIdx >= m_nTotalPages) return NULL;

	CTabNode& tab_node = m_ArrPages[nIdx];
	return tab_node.m_pDialog;
}

void CTabSheet::Show(UINT nIdx)
{
	ASSERT(nIdx < m_nTotalPages);

	for (UINT idx = 0; idx < m_nTotalPages; ++idx)
	{
		CTabNode& tab_node = m_ArrPages[idx];
		if (!tab_node.m_pDialog) continue;

		tab_node.m_pDialog->Create(tab_node.m_DlgID, this);
		InsertItem(idx, tab_node.m_strTitle);
	}

	SetRect(nIdx);
	SetCurFocus(nIdx);
}

void CTabSheet::SetRect(UINT nIdx)
{
	CRect tabRect, itemRect;
	int nX = 0, nY = 0, nXc = 0, nYc = 0;

	GetClientRect(&tabRect);
	GetItemRect(0, &itemRect);
	nX = itemRect.left;
	nY = itemRect.bottom + 1;
	nXc = tabRect.right - itemRect.left - 2;
	nYc = tabRect.bottom - nY - 2;

	for (UINT idx = 0; idx < m_nTotalPages; ++idx)
	{
		CTabNode& tab_node = m_ArrPages[idx];
		if (!tab_node.m_pDialog) continue;

		UINT nFlags = (idx == nIdx) ? SWP_SHOWWINDOW : SWP_HIDEWINDOW;
		tab_node.m_pDialog->SetWindowPos(&wndTop, nX, nY, nXc, nYc, nFlags);
	}
}

void CTabSheet::OnTcnSelchange(NMHDR *pNMHDR, LRESULT *pResult)
{
	if (m_nCurrentPage != GetCurFocus())
	{
		CTabNode& old_node = m_ArrPages[m_nCurrentPage];
		if (old_node.m_pDialog) old_node.m_pDialog->ShowWindow(SW_HIDE);

		m_nCurrentPage = GetCurFocus();
		CTabNode& new_node = m_ArrPages[m_nCurrentPage];
		if (new_node.m_pDialog) new_node.m_pDialog->ShowWindow(SW_SHOW);
	}

	*pResult = 0;
}
