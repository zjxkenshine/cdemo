﻿
// MFC02_Radio_CheckDlg.cpp: 实现文件
//

#include "pch.h"
#include "framework.h"
#include "MFC02_Radio_Check.h"
#include "MFC02_Radio_CheckDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CMFC02RadioCheckDlg 对话框



CMFC02RadioCheckDlg::CMFC02RadioCheckDlg(CWnd* pParent /*=nullptr*/)
	: CDialog(IDD_MFC02_RADIO_CHECK_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMFC02RadioCheckDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHECK2, Check2);
}

BEGIN_MESSAGE_MAP(CMFC02RadioCheckDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CMFC02RadioCheckDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CMFC02RadioCheckDlg::OnBnClickedButton2)
END_MESSAGE_MAP()


// CMFC02RadioCheckDlg 消息处理程序

BOOL CMFC02RadioCheckDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	// 初始化单选框 默认选中男 (指针方式)
	CButton* pNan = (CButton*)GetDlgItem(IDC_RADIO1);
	if (pNan) {
		// 选中
		pNan->SetCheck(BST_CHECKED);
	}

	// 第二组默认教师 (不通过指针)
	//CButton* pTeacher = (CButton*)GetDlgItem(IDC_RADIO3);
	//if (pTeacher) {
	//	// 选中
	//	pTeacher->SetCheck(BST_CHECKED);
	//}
	// 组起始id 结束id，选中id
	CheckRadioButton(IDC_RADIO3, IDC_RADIO5, IDC_RADIO3);

	// 复选框勾选方式1
	CButton* c1 = (CButton*)GetDlgItem(IDC_CHECK1);
	if (c1) {
		// 选中
		c1->SetCheck(BST_CHECKED);
	}

	// 复选框勾选方式2，控件变量
	Check2.SetCheck(BST_CHECKED);

	


	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CMFC02RadioCheckDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CMFC02RadioCheckDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CMFC02RadioCheckDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CMFC02RadioCheckDlg::OnBnClickedButton1(){
	// 获取选中值 (传入起始与结束按钮ID)
	int nID= GetCheckedRadioButton(IDC_RADIO3, IDC_RADIO5);
	CString temp_value = _T("");   //temp_value用来处理int值
	temp_value.Format(_T("%d"), nID);//固定格式
	MessageBox(temp_value);
}


void CMFC02RadioCheckDlg::OnBnClickedButton2(){
	// 复选框选中状态 获取方式1
	CButton* check1 = (CButton*)GetDlgItem(IDC_CHECK1);
	int c1 = check1->GetCheck();
	if (c1 == BST_CHECKED) {
		MessageBox(_T("足球被选中"));
	}
	else if (c1 == BST_UNCHECKED) {
		MessageBox(_T("足球未被选中"));
	}

	// 获取状态方式2
	int c2 = Check2.GetCheck();
	if (c2 == BST_CHECKED) {
		MessageBox(_T("篮球被选中"));
	}
	else if (c2 == BST_UNCHECKED) {
		MessageBox(_T("篮球未被选中"));
	}

	// 方式3 MFC API方式 不建议，比较麻烦
	// 获取句柄
	HWND h1=::GetDlgItem(GetSafeHwnd(), IDC_CHECK3);
	// 是否选中
	int c3 = (int)::SendMessage(h1, BST_CHECKED, 0, 0);
	if (c2 == BST_CHECKED) {
		MessageBox(_T("乒乓球被选中"));
	}
	else if (c2 == BST_UNCHECKED) {
		MessageBox(_T("乒乓球未被选中"));
	}
}
