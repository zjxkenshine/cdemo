﻿
// MFC01_ButtonDlg.h: 头文件
//

#pragma once
#include "RoundButton.h"

// CMFC01ButtonDlg 对话框
class CMFC01ButtonDlg : public CDialog
{
// 构造
public:
	CMFC01ButtonDlg(CWnd* pParent = nullptr);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MFC01_BUTTON_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	//afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
	CRoundButton Button1;
	CRoundButton Button4;
};
