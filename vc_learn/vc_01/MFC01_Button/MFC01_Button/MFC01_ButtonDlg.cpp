﻿
// MFC01_ButtonDlg.cpp: 实现文件
//

#include "pch.h"
#include "framework.h"
#include "MFC01_Button.h"
#include "MFC01_ButtonDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CMFC01ButtonDlg 对话框



CMFC01ButtonDlg::CMFC01ButtonDlg(CWnd* pParent /*=nullptr*/)
	: CDialog(IDD_MFC01_BUTTON_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMFC01ButtonDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	// 绑定控件变量
	DDX_Control(pDX, IDC_BUTTON1, Button1);
	DDX_Control(pDX, IDC_BUTTON4, Button4);
}

BEGIN_MESSAGE_MAP(CMFC01ButtonDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &CMFC01ButtonDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON1, &CMFC01ButtonDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CMFC01ButtonDlg::OnBnClickedButton2)
	// 绑定按钮与响应函数，删除的时候要删掉
	//ON_BN_CLICKED(IDC_BUTTON3, &CMFC01ButtonDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &CMFC01ButtonDlg::OnBnClickedButton4)
END_MESSAGE_MAP()


// CMFC01ButtonDlg 消息处理程序

BOOL CMFC01ButtonDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CMFC01ButtonDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CMFC01ButtonDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CMFC01ButtonDlg::OnQueryDragIcon()
{ 
	return static_cast<HCURSOR>(m_hIcon);
} 



void CMFC01ButtonDlg::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	CDialog::OnOK();
}


void CMFC01ButtonDlg::OnBnClickedButton1()
{ 
	MessageBox(_T("button1"));
}

// 添加响应函数
void CMFC01ButtonDlg::OnBnClickedButton2()
{
	// 获取BUTTON1句柄 win32 api  
	// ::表示全局函数
	//HWND hBtn = ::GetDlgItem(m_hWnd, IDC_BUTTON1);
	//// 存在
	//if (hBtn) {
	//	// 禁用
	//	::EnableWindow(hBtn,false);
	//}

	// 使用控件变量
	Button1.EnableWindow(false);
	
}

// 删除button3
//void CMFC01ButtonDlg::OnBnClickedButton3()
//{
//	// TODO: 在此添加控件通知处理程序代码
//}


void CMFC01ButtonDlg::OnBnClickedButton4()
{
	// 获取BUTTON1句柄 win32 api  通过id
	//HWND hBtn = ::GetDlgItem(m_hWnd, IDC_BUTTON1);
	//if (hBtn) {
	//	// 启用
	//	::EnableWindow(hBtn, true);
	//}

	// 使用非全局MFC函数
	CWnd* hBtn = GetDlgItem(IDC_BUTTON1);
	if (hBtn) {
		// 启用
		hBtn->EnableWindow(true);
	}

	// 
}
