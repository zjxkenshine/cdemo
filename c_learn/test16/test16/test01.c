#define _CRT_SECURE_NO_WARNINGS

#include<string.h>
#include<errno.h>
#include<stdio.h>

// 打开文件操作
int main() {
	// 打开文件 只读
	FILE* pf = fopen("test.txt", "r");
	if (pf == NULL) {
		printf("%s\n", strerror(errno));
		return 1;
	}
	// 读文件
	// ....
	// 关闭文件
	fclose(pf);
	pf = NULL;
	return 0;
}