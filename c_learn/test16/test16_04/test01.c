#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
#include<errno.h>

// 随机读字符 fseek 根据指针位置与偏移量确定
// rewind 指针回到起始位置
// ftell 指针偏移量
int main() {
	FILE* pf = fopen("test.txt", "r");
	if (pf == NULL) {
		printf("%s\n", strerror(errno));
		return 1;
	}
	// 读文件
	// 定位文件指针 SEEK_SET 文件起始
	fseek(pf, 2, SEEK_SET);
	int ch = fgetc(pf);//c
	printf("%c\n", ch);

	// 下一个 SEEK_CUR 当前位置
	fseek(pf, 2, SEEK_CUR);
	ch = fgetc(pf);	//f
	printf("%c\n", ch);

	// 最后一个字符 SEEK_END 文件末尾
	fseek(pf, -1, SEEK_END);
	ch = fgetc(pf);	//n
	printf("%c\n", ch);
	// ftell 相对于起始位置的偏移量
	printf("%d\n", ftell(pf)); //14

	// rewind 文件指针回到起始位置
	rewind(pf);
	ch = fgetc(pf);	//a
	printf("%c\n", ch);

	// 关闭文件
	fclose(pf);
	pf = NULL;
	return 0;
}