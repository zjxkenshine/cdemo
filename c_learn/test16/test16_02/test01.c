#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>

struct S {
	char arr[10];
	int age;
	float score;
};

int main() {
	struct S s = { "张三",20,55.5f };
	struct S tmp = { 0 };
	char buf[100] = { 0 };
	// sprintf 将格式化的数据转换成字符串
	sprintf(buf, "%s %d %f", s.arr, s.age, s.score);
	printf("%s\n", buf);
	// 从字符串buf获取一个格式化数据，转换成
	sscanf(buf, "%s %d %f", tmp.arr, &(tmp.age), &(tmp.score));
	printf("格式化：%s %d %f", tmp.arr, tmp.age, tmp.score);
	return 0;
}