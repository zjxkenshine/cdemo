#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include<string.h>
#include<errno.h>

// fputs 写一行数据
int main3() {
	// 打开文件
	FILE* pf = fopen("test.txt", "w");
	if (pf == NULL) {
		printf("%s\n", strerror(errno));
		return 1;
	}
	// fputs 写一行数据 自己换行
	fputs("hello bit\n", pf);
	fputs("hello bit\n", pf);
	// 关闭文件
	fclose(pf);
	pf = NULL;
	return 0;
}