#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include<string.h>
#include<errno.h>

//fgetc 读取字符
int main2() {
	// 打开文件
	FILE* pf = fopen("test.txt", "r");
	if (pf == NULL) {
		printf("%s\n", strerror(errno));
		return 1;
	}
	// 读取一个字符
	int ch = fgetc(pf);
	printf("%c\n", ch);

	// 读取所有
	int ch1 = 0;
	while ((ch1 = fgetc(pf)) != EOF) {
		printf("%c ", ch1);
	}

	// 关闭文件
	fclose(pf);
	pf = NULL;
	return 0;
}