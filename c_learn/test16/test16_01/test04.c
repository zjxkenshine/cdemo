#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include<string.h>
#include<errno.h>

//  fgets 读一行数据
int main4() {
	// 打开文件
	FILE* pf = fopen("test.txt", "r");
	if (pf == NULL) {
		printf("%s\n", strerror(errno));
		return 1;
	}
	// fgets 读一行数据
	char arr[20];
	fgets(arr, 20, pf);
	printf("%s\n", arr);
	
	// 关闭文件
	fclose(pf);
	pf = NULL;
	return 0;
}