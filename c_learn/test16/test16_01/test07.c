#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include<string.h>
#include<errno.h>

// fwrite 二进制写
struct S2 {
	char arr[10];
	int age;
	float score;
};

int main7() {
	struct S2 s = { "zhangsan",25,50.5f };

	// 打开文件 二进制写
	FILE* pf = fopen("test.txt", "wb");
	if (pf == NULL) {
		perror("fopen");
		return 1;
	}
	// fwrite 二进制写 一次写一个数据 长度为结构体大小
	fwrite(&s, sizeof(struct S2), 1, pf);

	// 关闭文件
	fclose(pf);
	pf = NULL;
	return 0;
}