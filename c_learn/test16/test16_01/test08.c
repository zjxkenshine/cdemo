#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include<string.h>
#include<errno.h>

// fread 二进制读
struct S3 {
	char arr[10];
	int age;
	float score;
};

int main() {
	struct S3 s = { 0 };

	// 打开文件
	FILE* pf = fopen("test.txt", "rb");
	if (pf == NULL) {
		perror("fopen");
		return 1;
	}
	// fscanf格式化读
	fread(&s, sizeof(struct S3), 1, pf);
	printf("%s %d %f\n", s.arr, s.age, s.score);

	// 关闭文件
	fclose(pf);
	pf = NULL;
	return 0;
}