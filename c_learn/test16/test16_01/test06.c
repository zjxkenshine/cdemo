#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include<string.h>
#include<errno.h>

// fscanf 格式化读
struct S1 {
	char arr[10];
	int age;
	float score;
};

int main6() {
	struct S1 s= {0};

	// 打开文件
	FILE* pf = fopen("test.txt", "r");
	if (pf == NULL) {
		perror("fopen");
		return 1;
	}
	// fscanf格式化读
	fscanf(pf, "%s %d %f", s.arr, &(s.age), &(s.score));
	printf("%s %d %f\n", s.arr, s.age, s.score);

	// 关闭文件
	fclose(pf);
	pf = NULL;
	return 0;
}