#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include<string.h>
#include<errno.h>

struct S {
	char arr[10];
	int age;
	float score;
};

int main5() {
	struct S s = { "zhangsan",25,50.5f };

	// 打开文件
	FILE* pf = fopen("test.txt", "w");
	if (pf == NULL) {
		perror("fopen");
		return 1;
	}
	// fprintf格式化写
	fprintf(pf,"%s %d %f",s.arr,s.age,s.score);
	// 输出到屏幕
	fprintf(stdout, "%s %d %f", s.arr, s.age, s.score);

	// 关闭文件
	fclose(pf);
	pf = NULL;
	return 0;
}