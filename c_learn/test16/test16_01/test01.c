#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include<string.h>
#include<errno.h>

// fputc 写字符
int main1() {
	// 打开文件
	FILE* pf= fopen("test.txt", "w");
	if (pf == NULL) {
		printf("%s\n", strerror(errno));
		return 1;
	}
	// 写文件 写一个字符
	fputc('a', pf);
	int i = 0;
	for (i = 'a';i <= 'z'; i++){
		fputc(i, pf);
	}

	// 关闭文件
	fclose(pf);
	pf = NULL;
	return 0;
}