#define _CRT_SECURE_NO_WARNINGS

// 判断大小端
#include <stdio.h>

int check_sys(){
	int i = 1;
	// 第一个字节为1 小端，第一个字节为0，大端
	return (* (char*)&i);
}
int main(){
	int ret = check_sys();
	if (ret == 1){
		printf("小端\n");
	}
	else{
		printf("大端\n");
	}
	return 0;
}