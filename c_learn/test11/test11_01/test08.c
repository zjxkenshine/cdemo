#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <string.h>

int main() {
	int len = strlen("abcdef");
	printf("%d\n", len);
	// strlen返回值是unsigned int
	if (strlen("abc") - strlen("abcdef") > 0) {
		printf(">");
	}
	else {
		printf("<");
	}

	// 强制转换成int进行计算
	if ((int)strlen("abc") - (int)strlen("abcdef") > 0) {
		printf(">");
	}
	else {
		printf("<");
	}


	if (strlen("abc") > strlen("abcdef")) {
		printf(">");
	}
	else {
		printf("<");
	}

	return 0;
}