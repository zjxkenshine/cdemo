#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <string.h>

int main6() {
	// char类型变量范围 -128~127
	char a[1000];
	int i;
	for (i = 0; i < 1000; i++){
		a[i] = -1 - i;
	}
	// 只会包含-128~128之间的数
	printf("%d", strlen(a));
	// strlen 是求字符串的长度，关注的是字符串'\0' 数字0之前出现多少个字符
	return 0;
}