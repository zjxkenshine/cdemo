#define _CRT_SECURE_NO_WARNINGS

// 调整数组中奇数位与偶数位的位置
// 奇数位于前半部分，偶数位于后半部分
#include<stdio.h>


void move_arr(int arr[], int sz) {
	int left = 0;
	int right = sz - 1;

	while (left < right) {
		// 从左向右找一个偶数，停下来 可能会越界，需要添加 left < right条件
		while (arr[left] % 2 == 1&& left < right) {
			left++;
		}

		// 从右往左找一个奇数
		while (arr[right] % 2 == 0 && left < right) {
			right--;
		}
		// 交换奇数和偶数
		if (left < right) {
			int tmp = arr[left];
			arr[left] = arr[right];
			arr[right] = tmp;
			left++;
			right--;
		}
	}
	


}


int main1() {

	int arr[6] = { 0 };
	// 输入
	int i = 0;
	int sz = sizeof(arr) / sizeof(arr[0]);
	for (i = 0;i < sz;i++) {
		//scanf("%d", arr + i);
		scanf("%d", &arr[i]);
	}
	// 调整奇偶
	move_arr(arr,sz);

	// 输出
	for (i = 0;i < sz;i++) {
		printf("%d ", arr[i]);
	}
	return 0;
}