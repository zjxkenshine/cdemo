#define _CRT_SECURE_NO_WARNINGS

// 有序序列合并
#include <stdio.h>

// 第一行m n 表示两行的数量
// 第2行输入 m个整数
// 第3行输入n个整数
int main() {
	int n = 0;
	int m = 0;
	scanf("%d %d", &n, &m);
	int arr1[n]; //C99支持
	int arr2[m];

	int i = 0;
	// 输入n个整数
	for (i = 0;i < n;i++) {
		scanf("%d", &arr1[i]);
	}
	// 输入m个整数
	for (i = 0;i < m;i++) {
		scanf("%d", &arr2[i]);
	}
	// 合并打印
	int j = 0;
	int k = 0;
	while (j < n && k < m) {
		if (arr1[j] < arr2[k]) {
			printf("%d ",arr1[j]);
			j++;
		}
		else {
			printf("%d ", arr2[k]);
			k++;
		}
	}

	// 剩余字符
	if (j < n) {
		for (;j < n;j++) {
			printf("%d ", arr1[j]);
		}
	}
	else {
		for (;k < m;k++) {
			printf("%d ", arr2[k]);
		}
	}


	return 0;
}