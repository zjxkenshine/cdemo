#define _CRT_SECURE_NO_WARNINGS


// 问题代码
// 无法确定三个func的调用顺序
// 函数的调用先后顺序无法通过操作符的优先级确定
int fun(){
	static int count = 1;
	return ++count;
}
int main(){
	int answer;
	answer = fun() - fun() * fun();
	printf("%d\n", answer);
	return 0;
}