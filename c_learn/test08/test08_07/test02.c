#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

int main2() {
	// -1整型提升
	// 11111111 原始数据
	// 高位补充符号位
	// 提升为11111111111111111111111111111111
	char c1 = -1;

	// 00000001
	// 提升为 00000000000000000000000000000001
	char c2 = 1;

	char d = c1 + c2;
	printf("%d\n", d);
	return 0;
}