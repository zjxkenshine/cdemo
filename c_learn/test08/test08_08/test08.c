#define _CRT_SECURE_NO_WARNINGS

// 输入3个分数，求最高分
#include <stdio.h>

int main8() {
	int i = 0;
	int max = 0;
	int score = 0;
	for (i = 0;i < 3;i++) {
		scanf("%d", &score);
		if (score > max) {
			max = score;
		}
	}
	printf("%d\n", max);
	return 0;
}