#define _CRT_SECURE_NO_WARNINGS

// 一次只能走1步或两步，计算走N个台阶有多少走法
// f(n)=f(n-1)+f(n-2)
#include <stdio.h>

int fib(int n) {
	if (n <= 2) {
		return n;
	}
	else {
		return fib(n - 1) + fib(n - 2);
	}
}

int main1() {
	int n = 0;
	// 输入
	scanf("%d", &n);
	// 计算
	int m = fib(n);
	// 输出
	printf("%d\n", m);
	return 0;
}