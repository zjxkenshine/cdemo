#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <string.h>

// 方式2
int main7() {
	char ch = 0;
	// " %c" 跳过下一个字符之前的所有空白字符
	while (scanf(" %c", &ch) == 1) {
		// isalpha() 判断是不是字符
		if (isalpha(ch)) {
			printf("%c 是字母", ch);
		}
		else {
			printf("%c 不是字母", ch);
		}
	}
	return 0;
}