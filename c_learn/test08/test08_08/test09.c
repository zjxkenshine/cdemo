#define _CRT_SECURE_NO_WARNINGS
// 水仙花数 
// 655=6*55+65*5
// 1461=1*461+14*61+146*1
// 求5位数的水仙花数
#include <stdio.h>

int isLily(int i) {
	// %多少个0
	int j = 10;
	// 整除余数
	int k = 0;
	// 商
	int l = 0;
	int count = 0;
	while(i>j){
		k = i % j;
		l = i / j;
		count = l*k+count;
		j = j * 10;
	}
	if (count == i) {
		return 1;
	}
	else {
		return 0;
	}
}

int main() {
	int i = 0;
	for (i = 1000;i <= 99999;i++) {
		if (isLily(i)) {
			printf("%d\n", i);
		}
	}
	return 0;
}