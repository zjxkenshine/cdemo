#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

// 大小写转换，大写转小写，小写转大写
int main4() {
	char ch = 0;
	// 读取一个处理一个
	// 读取成功时返回数据个数
	// 读取失败返回EOF
	while (scanf("%c", &ch) == 1) {
		if (ch >= 'a' && ch <= 'z') {
			printf("%c\n", ch - 32);
		}
		else if (ch>='A'&&ch<='Z') {
			printf("%c\n", ch + 32);
		}
	}
	return 0;
}