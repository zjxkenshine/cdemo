#define _CRT_SECURE_NO_WARNINGS

// 输入n个成绩，换行输出最高分与最低分的分差
#include <stdio.h>

int main3() {
	int n = 0;
	scanf("%d", &n);
	int arr[50];
	int i = 0;
	for (i = 0;i < n;i++) {
		scanf("%d", &arr[i]);
	}
	// 找出最大值
	int max = arr[0];
	for (i = 1;i < n;i++) {
		if (arr[i] > max) {
			max = arr[i];
		}
	}
	int min = arr[0];
	for (i = 1;i < n;i++) {
		if (arr[i] < min) {
			min = arr[i];
		}
	}
	printf("%d\n",max-min);

	return 0;
}