#define _CRT_SECURE_NO_WARNINGS

// 每行输入一个字符，判断是否为字符
#include <stdio.h>

int main6() {
	char ch = 0;
	while (scanf("%c", &ch) == 1){
		if ((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z')) {
			printf("%c 是字母",ch);
		}
		else {
			printf("%c 不是字母",ch);
		}
		// 处理\n
		getchar();
	}
	return 0;
}