#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <string.h>

int main5() {
	char ch = 0;
	// 另一种读取
	while (scanf("%c", &ch)!=EOF) {
		// 使用库函数
		if (islower(ch)){
			printf("%c\n",toupper(ch));
		}
		else if (isupper(ch)) {
			printf("%c\n", tolower(ch));
		}
	}
	return 0;
}