#define _CRT_SECURE_NO_WARNINGS

// & 取地址
// sizeof 计算所占空间
#include <stdio.h>

int main2() {
	int a = 0;
	// 取出的变量在内存中的起始地址
	printf("%p\n", &a);
	// sizeof变量所占空间大小
	printf("%d\n", sizeof(a));
	// 指针变量大小与计算机系统有关
	printf("%d\n", sizeof(&a));

	// 计算整个数组大小
	int arr[10] = { 0 };
	printf("%d\n", sizeof(arr));
	return 0;
}