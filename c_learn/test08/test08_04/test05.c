#define _CRT_SECURE_NO_WARNINGS

// ++ --操作符
#include <stdio.h>

int main5() {
	int a = 3;
	int a1 = 3;
	// 先++ 后使用
	int b = ++a;

	// 先使用 后++
	int c = a1++;

	printf("a=%d\n", a);
	printf("b=%d\n", b);
	printf("c=%d\n", c);
	return 0;
}