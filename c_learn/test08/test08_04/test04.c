#define _CRT_SECURE_NO_WARNINGS

// 改变某一位的值
#include <stdio.h>

int main4() {
	int a = 13;
	// 将第5位设置为1
	a |= (1 << 4);
	printf("%d\n", a);

	// 第5位设置为0
	a &= (~(1<<4));
	printf("%d\n",a);
	return 0;
}