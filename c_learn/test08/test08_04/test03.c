#define _CRT_SECURE_NO_WARNINGS

// ~按位取反
#include <stdio.h>

int main3() {
	int a = 3;
	// 3的补码按位取反，包括符号位
	int b = ~3;
	printf("%d\n", b);
	return 0;
}
