#define _CRT_SECURE_NO_WARNINGS

// 函数内sizeof数组名为指针大小，长度与计算机平台有关
#include <stdio.h>

void test1(int arr[]){
	printf("%d\n", sizeof(arr));	//8
}
void test2(char ch[]){
	// ch是指针 
	printf("%d\n", sizeof(ch));		//8
}
int main()
{
	int arr[10] = { 0 };
	char ch[10] = { 0 };
	// 数组大小
	printf("%d\n", sizeof(arr));	//40
	printf("%d\n", sizeof(ch));		//10
	test1(arr);
	test2(ch);
	return 0;
}