#define _CRT_SECURE_NO_WARNINGS
// 解引用操作符 *

#include <stdio.h>

int main6() {
	int a = 10;
	int* p = &a;
	// 将P地址数据修改为20
	*p = 20;
	printf("%d\n",a);
	return 0;
}