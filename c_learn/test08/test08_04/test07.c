#define _CRT_SECURE_NO_WARNINGS

// sizeof不是函数， 后面的括号可省略
#include <stdio.h>

int main7() {
	int a = -10;
	int* p = NULL;
	// 0
	printf("%d\n", !2);
	// 1
	printf("%d\n", !0);
	a = -a;
	p = &a;
	printf("%d\n", sizeof(a));
	printf("%d\n", sizeof(int));
	printf("%d\n", sizeof a);
	//这样写错误
	//printf("%d\n", sizeof int);
	return 0;
}