#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
// !取反
int main1() {
	int flag = 3;

	// 0为假，其余为真
	if (flag) {
		printf("真");
	}

	if (!flag) {
		printf("假");
	}
	return 0;
}