#define _CRT_SECURE_NO_WARNINGS

// 连续赋值 不直观 不推荐
#include <stdio.h>

int main1() {
	int a = 10;
	int x = 0;
	int y = 20;
	a = x = y + 1;//连续赋值

	printf("%d\n", a);
	printf("%d\n", x);

	// 推荐这种方式
	x = y + 1;
	a = x;
	return 0;
}