#define _CRT_SECURE_NO_WARNINGS

// 复合赋值操作符
#include <stdio.h>

int main() {
	int x = 10;
	x = x + 10;
	printf("%d\n", x);

	x += 10;//复合赋值 x = x + 10
	printf("%d\n", x);
	return 0;
}
