#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

// 逗号表达式
int main() {
	int a = 1;
	int b = 2;
	// 从左往右计算，但只返回最后一个表达式的值
	int c = (a > b, a = b + 10, a, b = a + 1);//逗号表达式
	printf("%d\n", c);//13

	int d = 1;
	// 只返回d>0
	if (a = b + 1, c = a / 2, d > 0) {
		printf("真");
	}

	return 0;
}