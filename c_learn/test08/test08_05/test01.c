#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <string.h>

// ==比较的是两个字符串首地址的位置
int main1() {
	printf("%d\n", 3 == 5);
	// 错误的比对方式 == 比较首地址
	printf("%d\n", "abc" == "abcde");
	// 正确的比较写法
	printf("%d\n", strcmp("abc","abcde")==0);

	return 0;
}