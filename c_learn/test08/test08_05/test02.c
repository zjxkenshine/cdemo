#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

// 逻辑或，逻辑与 区分按位或按位与
int main2() {
	int a = 3;
	int b = 6;
	int c = 0;

	printf("a|b=%d\n", a | b);
	printf("a||b=%d\n", a || b);
	printf("a|c=%d\n", a | c);
	printf("a||c=%d\n", a || c);


	printf("a&b=%d\n", a & b);
	printf("a&&b=%d\n", a && b);
	printf("a&c=%d\n", a & c);
	printf("a&&c=%d\n", a && c);
	return 0;
}