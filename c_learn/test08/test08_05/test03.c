#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

int main3(){
	int i = 0, a = 0, b = 2, c = 3, d = 4;
	// 短路操作，a++为0，后面的就不计算了
	i = a++ && ++b && d++;
	//i = a++||++b||d++;
	// 输出为1 2 3 4
	printf("a = %d\nb = %d\nc = %d\nd = %d\n", a, b, c, d);
	return 0;
}