#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

// 条件表达式求最大值值
int main6() {
	int a = 0;
	int b = 1;
	int c = a > b ? a : b;
	printf("%d\n", c);
	return 0;
}