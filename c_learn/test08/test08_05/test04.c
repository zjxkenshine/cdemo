#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

int main4() {
	int i = 0, a = 0, b = 2, c = 3, d = 4;
	// 0||3||d++
	// 短路操作 计算到0||3就不继续算了，恒为真
	i = a++||++b||d++;
	// 输出为1 3 3 4
	printf("a = %d\nb = %d\nc = %d\nd = %d\n", a, b, c, d);
	return 0;
}