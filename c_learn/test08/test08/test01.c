#define _CRT_SECURE_NO_WARNINGS

// 算数操作符
#include <stdio.h>

// %取模操作符的两端必须是整数
int main() {
	int a = 7 % 2;
	int b = 7 / 2;
	printf("%d\n", a);
	printf("%d\n", b);

	return 0;
}