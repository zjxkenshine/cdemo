#define _CRT_SECURE_NO_WARNINGS

// [] 是个操作符
#include <stdio.h>

int main1() {
	int arr[10] = { 0 };
	// [] 的操作数为arr和7
	arr[7] = 8;

	// 这样写也没有问题
	7[arr] = 9;
	printf("%d\n", arr[7]);

	// arr+7就是跳过7个元素，指向第8个
	// arr[7] = *(arr+7) = *(7+arr) = 7[arr]
	return 0;
}