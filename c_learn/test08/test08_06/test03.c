#define _CRT_SECURE_NO_WARNINGS
// 结构体
#include <stdio.h>
#include <string.h>

struct Stu {
	char name[10];
	int age;
	double score;
};

// stu不会进行修改
void set_stu(struct Stu ss){
	strcpy(ss.name,"kenshine");
	ss.age = 18;
	ss.score = 100.00;
}
void set_stu2(struct Stu* ps){
	ps->age = 12;//结构成员访问
	strcpy(ps->name,"hong");
	ps->score = 88.00;
}

void print_stu(struct Stu ss) {
	printf("%s %d %lf\n", ss.name, ss.age, ss.score);
}

int main(){
	struct Stu stu = {0};
	struct Stu* ps = &stu;//结构成员访问

	stu.age = 20;
	set_stu(stu);
	print_stu(stu);

	ps->age = 20;
	set_stu2(ps);
	print_stu(stu);

	return 0;
}