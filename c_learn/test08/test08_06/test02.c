#define _CRT_SECURE_NO_WARNINGS

//() 函数调用操作符
#include <stdio.h>

int Add(int x, int y) {
	return x + y;
}

int main2() {
	int a = 10;
	int b = 20;
	// 函数调用
	int c = Add(a, b);

	printf("%d\n", c);
	return 0;
}