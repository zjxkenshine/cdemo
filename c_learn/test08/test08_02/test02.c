#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

// | 按位或
int main2() {
	int a = 3;
	int b = -5;

	int c = a | b;
	// 补码进行计算
	// 按位或，有1为1，否则为0
	printf("c=%d", c);
	return 0;
}