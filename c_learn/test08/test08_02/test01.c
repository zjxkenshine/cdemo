#define _CRT_SECURE_NO_WARNINGS

// 位操作符
#include <stdio.h>

// & 按位与
int main1() {
	int a = 3;
	int b = -5;
	int c = a & b;
	// 补码进行计算
	// 按位与，全1为1 否则为0
	printf("c=%d", c);
	return 0;
}