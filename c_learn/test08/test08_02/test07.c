#define _CRT_SECURE_NO_WARNINGS

// 编写代码，实现整数存储在内存中的1的个数（补码1的个数）
#include <stdio.h>

// 方式3
int main() {
	int num = 10;
	int i = 0;
	int count = 0;//计数
	while (num){
		count++;
		num = num & (num - 1);
	}
	printf("二进制中1的个数 = %d\n", count);
	return 0;
}