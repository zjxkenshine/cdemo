#define _CRT_SECURE_NO_WARNINGS

// 编写代码，实现整数存储在内存中的1的个数（补码1的个数）
#include <stdio.h>


// 方式一
// a&1 为1计数+1 ，然后a右移一位，再次&1，循环统计
int main5() {
	int num = 10;
	int i = 0;
	int count = 0;//计数
	for (i = 0; i < 32; i++)
	{
		if (num & (1 << i))
			count++;
	}
	printf("二进制中1的个数 = %d\n", count);
	return 0;
}
