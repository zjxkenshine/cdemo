#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

// ^ 按位异或
int main3() {
	int a = 3;
	int b = -5;

	int c = a ^ b;
	// 补码进行计算
	// 按位异或，相同为0，否则为1
	printf("c=%d", c);
	return 0;
}