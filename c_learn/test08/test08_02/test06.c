#define _CRT_SECURE_NO_WARNINGS
// 编写代码，实现整数存储在内存中的1的个数（补码1的个数）
#include <stdio.h>

// 方式二 % 取模
int mai6() {
	int num = 10;
	int count = 0;//计数
	while (num){
		if (num % 2 == 1)
			count++;
		num = num / 2;
	}
	printf("二进制中1的个数 = %d\n", count);
	return 0;
}