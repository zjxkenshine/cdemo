#define _CRT_SECURE_NO_WARNINGS

// 不创建临时变量，两数交换
// 按位异或的使用
#include <stdio.h>

// 实际工作过程中会采用临时变量交换，不会使用异或，异或只支持整数
int main4() {
	int a = 3;
	int b = 5;

	printf("交换前：a=%d b=%d\n", a, b);
	// 方式一 和 
	// 会有溢出问题
	//a = a + b;
	//b = a - b;
	//a = a - b;

	//方式2 异或 不会溢出
	a = a ^ b;
	b = a ^ b;
	a = a ^ b;

	printf("交换后：a=%d b=%d\n", a, b);
	return 0;
}