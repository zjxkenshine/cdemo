// 各种类型的大小
#include <stdio.h>

int main() {
	printf("%d\n", sizeof(char));
	printf("%d\n", sizeof(short));
	printf("%d\n", sizeof(int));
	printf("%d\n", sizeof(long));
	printf("%d\n", sizeof(long long));
	printf("%d\n", sizeof(float));
	printf("%d\n", sizeof(double));
	printf("%d\n", sizeof(long double));

	// 基础数据类型使用
	char ch = 'w';
	int weight = 120;
	int salary = 20000;
	return 0;
}