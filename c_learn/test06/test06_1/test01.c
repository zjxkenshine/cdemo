#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

//仅交换地址 值不变
void Swap(int* px, int* py)
{
	int tmp = 0;
	tmp = px;
	px = py;
	py = tmp;
}

int main1() {
	int a = 0;
	int b = 0;
	scanf("%d%d", &a, &b);
	//交换
	printf("交换前：a=%d,b=%d\n", a, b);
	Swap(&a, &b);
	printf("交换后：a=%d,b=%d\n", a, b);
	return 0;
}