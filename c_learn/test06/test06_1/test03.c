#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <math.h>

// 判断是否为素数
int checkSushu(int i) {
	int j = 0;
	// 2-sqrt(i)中可整除即可
	for (j = 2;j <= sqrt(i);j++) {
		if (i % j == 0) {
			return 0;
		}
	}
	return 1;
}

// 打印100~200之间的素数
int main(){
	int i = 0;
	for (i = 101;i <= 200;i+=2) {
		// 判断是否为素数
		if (checkSushu(i)) {
			printf("%d ",i);
		}
	}

	return 0;
}