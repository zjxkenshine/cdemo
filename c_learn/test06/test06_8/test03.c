#define _CRT_SECURE_NO_WARNINGS

// 求两个数最大公约数  暴力求解
#include <stdio.h>

// 求最大公约数
int yue(int a, int b) {
	int min = a < b ? a : b;
	int m = min;
	while (1) {
		if (a % m == 0 && b % m == 0) {
			break;
		}
		m--;
	}
	return m;
}

int main3() {
	int a = 0;
	int b = 0;
	scanf("%d %d", &a, &b);
	int res = yue(a, b);
	printf("%d\n", res);
	return 0;
}