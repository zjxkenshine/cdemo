#define _CRT_SECURE_NO_WARNINGS

// 辗转相除法 求最大公约数
#include <stdio.h>

int yue1(int a, int b) {
	int c = 0;
	while (a % b) {
		int c = a % b;
		a = b;
		b = c;
	}
	return b;
}

int main4() {
	int a = 0;
	int b = 0;
	scanf("%d %d", &a, &b);
	int res = yue1(a, b);
	printf("%d\n", res);
	return 0;
	return 0;
}