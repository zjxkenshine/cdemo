#define _CRT_SECURE_NO_WARNINGS
// 从到小输出3个整数
#include <stdio.h>

void compareAndSwap(int* x, int* y) {
	if (*x < *y) {
		int tmp = *x;
		*x = *y;
		*y = tmp;
	}
}

int main1() {
	int a = 0;
	int b = 0;
	int c = 0;
	scanf("%d %d %d", &a, &b, &c);
	// 调整顺序
	compareAndSwap(&a, &c);
	compareAndSwap(&a, &b);
	compareAndSwap(&b, &c);
	printf("%d %d %d\n", a, b, c);
	return 0;
}
