#define _CRT_SECURE_NO_WARNINGS
// 打印1-100之间的3的倍数

#include <stdio.h>

int checkThree(int i) {
	if (i % 3 == 0) {
		return 1;
	}
	else {
		return 0;
	}
}

int main2() {
	
	int i = 0;
	for (int i = 1;i <= 100;i++) {
		if (checkThree(i)) {
			printf("%d\n", i);
		}
	}
	return 0;
}