#define _CRT_SECURE_NO_WARNINGS

// 判断是否是闰年 1000-2000年
// 1.能被4整除且不能被100整除
// 2.能被400整除

#include <stdio.h>

int checkRun(year) {
	if ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0)) {
		return 1;
	}
	return 0;
}

int main1() {
	int year = 0;
	for (year = 1000;year <= 2000;year++) {
		// 判断是否是闰年
		if (checkRun(year)) {
			printf("%d ", year);
		}
	}
	return 0;
}
