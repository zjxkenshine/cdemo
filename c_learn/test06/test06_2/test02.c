#define _CRT_SECURE_NO_WARNINGS
//  写一个函数，实现一个整形有序数组的二分查找
#include <stdio.h>

// 二分查找函数
// arr[] 实际是一个指针变量 所以不要在函数内部计算size，元素个数
int binary_search(int arr[],int k,int sz) {
	int left = 0;
	int right = sz - 1;
	while (left <=right) {
		int mid = left + (right - left) / 2;
		if (arr[mid] < k) {
			left = mid + 1;
		}
		else if (arr[mid] > k) {
			right = mid - 1;
		}
		else {
			// 找到了返回下标
			return mid;
		}
	}

	return -1;
}

int main2() {

	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
	int k = 7;
	int size = sizeof(arr) / sizeof(arr[0]);
	// 二分查找，找到了返回下标，找不到返回-1
	int ret = binary_search(arr, k, size);
	if (ret == -1) {
		printf("找不到\n");
	}
	else {
		printf("找到了，下标是%d\n", ret);
	}

	return 0;
}