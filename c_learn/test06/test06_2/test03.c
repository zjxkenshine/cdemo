#define _CRT_SECURE_NO_WARNINGS
//  写一个函数，每调用一次这个函数，就会将 num 的值增加1
#include <stdio.h>

// 传址方式
void Add(int* p) {
	(*p)++;
}

int main() {
	int num = 0;
	Add(&num);
	printf("%d\n", num);
	Add(&num);
	printf("%d\n", num);
	Add(&num);
	printf("%d\n", num);
	Add(&num);
	printf("%d\n", num);
}
