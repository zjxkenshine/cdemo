#define _CRT_SECURE_NO_WARNINGS

// 编写函数不允许创建临时变量，求字符串的长度
#include <stdio.h>

//str 首字母地址 一个字符一个字节
//my_strlen(abc);
//1+my_strlen(bc);
//1+1+my_strlen(c);
//1+1+1+my_strlen("");
int my_strlen(const char* str)
{
	if (*str == '\0')
		return 0;
	else

		return 1 + my_strlen(str + 1);
}

int main2() {
	char* p = "abcdef";
	int len = my_strlen(p);
	printf("%d\n", len);
	return 0;
}