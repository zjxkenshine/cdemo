#define _CRT_SECURE_NO_WARNINGS
// 迭代求阶乘
#include <stdio.h>

//求n的阶乘 迭代实现
int factorial1(int n){
	int result = 1;
	while (n > 1){
		result *= n;
		n -= 1;
	}
	return result;
}

int main5() {
	int a = 0;
	scanf("%d", &a);
	int sum = factorial1(a);
	printf("%d", sum);
	return 0;
}
