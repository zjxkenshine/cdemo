#define _CRT_SECURE_NO_WARNINGS

// 斐波那契数列 递归实现
#include <stdio.h>  

// 会调用非常多次的fib，50以上函数就会崩溃了
int count = 0;//全局变量
int fib(int n){
	// 每次调用到n==3时统计一次
	if (n == 3)
		count++;
	if (n <= 2)
		return 1;
	else
		return fib(n - 1) + fib(n - 2);
}

int main4() {
	int n = 0;
	scanf("%d",&n);
	int sum = fib(n);
	printf("%d\n", sum);
	// 总调用次数
	printf("%d\n", count);
	return 0;
}
