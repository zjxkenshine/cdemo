#define _CRT_SECURE_NO_WARNINGS
// 斐波那契数列 非递归实现 迭代实现

//求第n个斐波那契数
int fib1(int n){
	int result;
	int pre_result;
	int next_older_result;
	result = pre_result = 1;
	while (n > 2)
	{
		n -= 1;
		next_older_result = pre_result;
		pre_result = result;
		result = pre_result + next_older_result;
	}
	return result;
}


int main() {
	int n = 0;
	scanf("%d", &n);
	int sum = fib1(n);
	printf("%d\n", sum);
	return 0;
}