#define _CRT_SECURE_NO_WARNINGS

// 接受一个整型值（无符号），按照顺序打印它的每一位
// 输入1234 输出1 2 3 4
// 递归实现
#include <stdio.h>

// print(1234)
// print(123) 4
// print(12) 3 4
// print(1) 2 3 4
void print(unsigned int n){
	if (n > 9){
		print(n / 10);
	}
	printf("%d ", n % 10);
}

int main1() {
	unsigned int num = 0;
	scanf("%u",&num);
	// 接受一个整形值，按顺序打印每一位
	print(num);
	return 0;
}
