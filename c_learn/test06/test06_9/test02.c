#define _CRT_SECURE_NO_WARNINGS

// 输出99乘法表
#include <stdio.h>

int main() {
	
	int i = 0;
	for (i = 1;i <= 9;i++) {
		// 打印一行
		int j = 0;
		for (j = 1;j <= i;j++) {
			// %-2d 位对齐
			printf("%d*%d=%-2d ", i, j, i * j);
		}
		printf("\n");
	}
	return 0;
}
