#define _CRT_SECURE_NO_WARNINGS

// 求10个整数中的最大值
#include <stdio.h>


int main1() {
	int arr[] = { 0,1,2,3,4,5,6,7,8,9,10 };
	// 找出最大值
	int max = arr[0];
	int i = 0;
	for (i = 1;i <=10;i++) {
		if (arr[i] > max) {
			max = arr[i];
		}
	}
	printf("%d\n", max);

	return 0;
}