#define _CRT_SECURE_NO_WARNINGS

// strcpy 函数 复制
#include <stdio.h>
#include <string.h>

int main1() {
	char arr1[20] = { 0 };
	char arr2[] = "hello bit";
	// 从arr2复制到arr1
	strcpy(arr1,arr2);
	printf("%s\n", arr1);
	return 0;
}