#define _CRT_SECURE_NO_WARNINGS

// 自定义函数 交换两个整形变量内容
#include <stdio.h>

// 形式参数 不会影响原来的参数
void Swap1(int x, int y) {
	int z = 0;
	z = x;
	x = y;
	y = z;

}

//正确的版本 通过指针操作地址
void Swap2(int* px, int* py)
{
	int tmp = 0;
	tmp = *px;
	*px = *py;
	*py = tmp;
}


int main() {
	int a = 0;
	int b = 0;
	scanf("%d%d", &a, &b);
	//交换
	printf("交换前：a=%d,b=%d\n", a, b);
	Swap1(a, b);
	printf("交换1后：a=%d,b=%d\n", a, b);
	Swap2(&a, &b);
	printf("交换2后：a=%d,b=%d\n", a, b);
	return 0;
}