#define _CRT_SECURE_NO_WARNINGS

// 求两个整数较大值
#include <stdio.h>

int get_max(int x, int y) {
	return (x > y ? x : y);
}

int main3() {
	int a = 0;
	int b = 0;
	scanf("%d %d",&a, &b);
	// 求较大值 自定义函数
	int m = get_max(a, b);
	printf("%d\n", m);

	return 0;
}