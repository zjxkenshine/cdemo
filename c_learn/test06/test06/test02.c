#define _CRT_SECURE_NO_WARNINGS

// memset(ptr,value,num)
// ptr位置开始向后的num字节设置成value
#include <stdio.h>
#include <string.h>

int main2() {
	char arr[20] = "hello world";
	// arr位置开始5个字符设为x
	memset(arr, 'x', 5);
	printf("%s\n",arr);
	return 0;
}
