#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
// 函数的声明与定义
// 声明 不声明会报错
int Add1(int x, int y);

int main1() {
	int a = 0;
	int b = 0;
	scanf("%d %d", &a, &b);
	int sum = Add1(a, b);
	printf("%d\n", sum);
	return 0;
}

// 定义
int Add1(int x, int y) {
	return x + y;
}