#define _CRT_SECURE_NO_WARNINGS

// 使用头文件声明函数
#include <stdio.h>
// 引入自定义头文件
#include "add.h"
#include "sub.h"

int main() {
	int a = 0;
	int b = 0;
	scanf("%d %d", &a, &b);
	int sum = Add1(a, b);
	printf("sum=%d\n", sum);

	int sub = Sub(a, b);
	printf("sub=%d\n", sub);
	return 0;
}