#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

// 链式访问 把一个函数的返回值作为另外一个函数的参数
int Add(int x) {
	return x + 2;
}

int main2() {
	int x = 10;
	int y = Add(Add(Add(Add(Add(Add(x))))));
	printf("%d\n", y);
	return 0;
}