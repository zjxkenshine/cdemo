#define _CRT_SECURE_NO_WARNINGS

// 打印菱形
#include <stdio.h>

int main1() {
	// 上半部分打印多少行
	int line = 0;
	scanf("%d", &line);

	int i = 0;
	// 上半部分
	for (i = 0;i < line;i++) {
		// 空格
		int j = 0;
		for (j = 0;j < line - 1 - i;j++) {
			printf(" ");
		}
		// *
		for (j = 0;j < 1+2*i;j++) {
			printf("*");
		}
		// 换行
		printf("\n");
	}
	// 下半部分
	for (i = 0;i < line-1;i++) {
		int j = 0;
		for (j = 0;j <=i;j++) {
			printf(" ");
		}
		for (j = 0;j < 2 *(line-1-i)-1;j++) {
			printf("*");
		}
		// 换行
		printf("\n");
	}
	return 0;
}