#define _CRT_SECURE_NO_WARNINGS

// 喝汽水，1瓶汽水1元，两个瓶子换一块钱，给20元，可以换多少汽水
#include <stdio.h>

int main() {
	int money = 0;
	scanf("%d", &money);
	int total = money;
	int empty = money;
	while(empty >= 2) {
		total = empty / 2 + total;
		empty = empty / 2 + empty % 2;
	}
	printf("%d\n", total);
	return 0;
}
