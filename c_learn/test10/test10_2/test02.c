#define _CRT_SECURE_NO_WARNINGS

// F10跟F11区别
int Add(int x, int y) {
	return x + y;
}

int main2() {
	int a = 10;
	int b = 20;
	// F11可以进入Add方法，F10不进入
	int c = Add(a, b);
	printf("%d\n", c);
	return 0;
}