#define _CRT_SECURE_NO_WARNINGS
// 求 1！+2！+3！ ...+ n! ；不考虑溢出
#include <stdio.h>

// 这时候我们如果3，期待输出9，但实际输出的是15
int main5(){
	int i = 0;
	int sum = 0;//保存最终结果
	int n = 0;
	int ret = 1;//保存n的阶乘
	scanf("%d", &n);
	for (i = 1; i <= n; i++){
		int j = 0;
		// ret要重新赋值
		int ret = 1;
		for (j = 1; j <= i; j++){
			ret *= j;
		}
		sum += ret;
	}
	printf("%d\n", sum);
	return 0;
}