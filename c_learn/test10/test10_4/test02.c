#define _CRT_SECURE_NO_WARNINGS

// 指针变量大小
#include <stdio.h>

int main2() {
	int arr[] = { 1,2,3,4,5 };
	short* p = (short*)arr;
	int i = 0;
	// short型指针，一次访问2个字节
	// 这里仅会更新前两个int的值
	for (i <= 0;i < 4;i++) {
		*(p + i) = 0;
	}

	for (i = 0;i < 5;i++) {
		printf("%d", arr[i]);
	}
	return 0;
}

