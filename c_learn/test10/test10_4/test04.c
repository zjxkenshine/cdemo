#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>

// 写一个函数,不使用下标打印数组内容
int main4() {
	int arr[] = { 1,2,3,4,5,6,7,8 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	int* p = arr;

	int i = 0;
	for (i = 0;i < sz;i++) {
		printf("%d ", *(p + i));
	}
	printf("/n");
	return 0;
}