#define _CRT_SECURE_NO_WARNINGS

// 数据在内存中的存储
#include <stdio.h>

int main3() {
	int a = 0x11223344;
	char* p = (char*)&a;
	*p = 0;
	// 内存中存储的是 44 33 22 11 4个字节逆序存储
	// 修改首个字节为00 结果为11223300
	printf("%x\n", a);
	return 0;
}

