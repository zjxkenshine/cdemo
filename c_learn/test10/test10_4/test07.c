#define _CRT_SECURE_NO_WARNINGS

// 求1~100000之间的水仙花数 一个n位数，各个数的n次幂刚好等于它本身
#include <stdio.h>
#include <math.h>

int checki(int i) {
	int n = getNum(i);
	int sum = 0;
	int tmp = i;
	while (i) {
		sum += pow(i % 10, n);
		i /= 10;
	}
	if (sum == tmp) {
		return 1;
	}
	else {
		return 0;
	}
}

// 获取位数
int getNum(int i) {
	int n = 1;
	while (i / 10) {
		n++;
		i /= 10;
	}
	return n;
}

int main() {
	int i = 0;
	for (int i = 0;i <= 100000;i++) {
		// 判断i是否是水仙花数
		if(checki(i)){
			printf("%d\n",i);
		}
	}

	return 0;
}