#define _CRT_SECURE_NO_WARNINGS
// 输入三条边，判断是否构成三角形
#include <stdio.h>

int main1() {
	int a = 0;
	int b = 0;
	int c = 0;
	while (scanf("%d %d %d", &a, &b, &c) == 3) {
		// 判断
		if ((a + b > c)&& (a + c > b) && (b + c > a)) {
			if (a == b && b == c) {
				printf("等边三角形\n");
			}
			else if ((a == b && b != c) || (a == c && c != b) || (b == c && b != a)) {
				printf("等腰三角形\n");
			}
			else {
				printf("普通三角形\n");
			}
		}
		else {
			printf("不是三角形\n");
		}
	}
	return 0;
}