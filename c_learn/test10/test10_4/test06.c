#define _CRT_SECURE_NO_WARNINGS

// 求 a+aa+aaa+aaaa+aaaaa 之和，a是一个数字
#include <stdio.h>

int main6() {
	int a = 0;
	int n = 0;
	scanf("%d %d", &a, &n);
	int i = 0;
	int sum = 0;
	int k = 0;
	for (i = 0;i < n;i++) {
		k = a + k * 10;
		sum += k;
	}
	printf("%d\n", sum);
	return 0;
}