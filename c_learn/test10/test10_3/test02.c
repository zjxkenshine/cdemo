﻿#define _CRT_SECURE_NO_WARNINGS
// const
#include <stdio.h>
#include <assert.h>
//代码1
void test1()
{
	int n = 10;
	int m = 20;
	int* p = &n;
	*p = 20;//ok?
	p = &m; //ok?
}
void test2(){
	//代码2
	int n = 10;
	int m = 20;
	const int* p = &n;
//	*p = 20;
	p = &m;
}
void test3()
{
	int n = 10;
	int m = 20;
	int* const p = &n;
	*p = 20; //ok?
//	p = &m;
}

int main2()
{
	//测试无cosnt的
	test1();
	//测试const放在*的左边 p指向的对象不能通过p改变，但是p变量本身的值可以改变
	test2();
	//测试const放在*的右边  p指向的对象可改，p本身不能修改
	test3();
	return 0;
}