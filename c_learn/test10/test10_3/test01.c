#define _CRT_SECURE_NO_WARNINGS

// strcpy实现
#include <stdio.h>
#include <assert.h>

// const可以在编译期提示错误
char* strcpy(char* dst, const char* src){
	char* cp = dst;
	// 断言，判断指针非空
	assert(dst && src);
	assert(dst != NULL);
	assert(src != NULL);

	while (*cp++ = *src++)
		; 
	return(dst);
}