#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
// 嵌套结构体初始化
struct Peo {
	char name[20];
	char tele[12];
	char sex[5];
	int high;
};

// 结构体中含有结构体
struct Stu {
	struct Peo peo;
	int num;
	float score;
};

int main2() {
	struct Peo p1 = { "kenshine","1223456784","男",188 };
	// 嵌套结构体初始化
	struct Stu stu = { {"qin","12343333333","女",164},001,88.1421414f };
	
	// 结构体打印
	printf("%s %s %s %d\n", p1.name, p1.tele, p1.sex, p1.high);
	printf("%s %s %s %d %d %f", stu.peo.name, stu.peo.tele, stu.peo.sex, stu.peo.high, stu.num, stu.score);
	return 0;
}