#define _CRT_SECURE_NO_WARNINGS

// 结构体对象访问
#include <stdio.h>

struct Peo {
	char name[20];
	char tele[12];
	char sex[5];
	int high;
};

// 结构体访问方式1，指针->变量
void print1(struct Peo* sp) {
	printf("%s %s %s %d\n", sp->name, sp->tele, sp->sex, sp->high);
}

// 结构体访问方式2，结构体变量.成员变量
void print2(struct Peo p1) {
	printf("%s %s %s %d\n", p1.name, p1.tele, p1.sex, p1.high);
}


int main() {
	struct Peo p1 = { "kenshine","1223456784","男",188 };
	struct Peo p2 = { "qin","12343333333","女",164 };
	// 推荐使用这种方式传参
	print1(&p1);
	// 传递对象会有开销
	print2(p2);
	return 0;
}