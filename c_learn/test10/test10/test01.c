#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

// 结构体 声明的同时定义s1 s2
typedef struct Stu {
	char name[20];//名字
	int age;//年龄
	char sex[5];//性别
	char id[20];//学号
}s1,s2;//分号不能丢  s1s2是全局结构体变量


// 一般情况下不用全局变量
int main1() {
	// 结构体初始化
	struct Stu s = { "kenshine", 20,"男","12345"};

	printf("%s\n", s.name);
	printf("%d\n", s.age);
	return 0;
}