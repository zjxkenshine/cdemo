#define _CRT_SECURE_NO_WARNINGS
// 获取二进制奇数位跟偶数位的二进制序列
#include <stdio.h>

int main5() {
	// 获取奇数位的数字
	int i = 0;
	int num = 0;
	scanf("%d", &num);
	for (i = 30;i >= 0;i -= 2) {
		printf("%d ", (num >> i) & i);
	}
	printf("\n");
	// 获取偶数位对应的数字
	for (i = 31;i >= 1;i -= 2) {
		printf("%d ", (num >> i) & i);
	}

	return 0;
}