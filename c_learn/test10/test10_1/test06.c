#define _CRT_SECURE_NO_WARNINGS
// 全局变量，放在静态区
// 全局变量，静态变量不初始化的时候，默认初始化为0
// 局部变量，放在栈区，不初始化默认是随机值
#include <stdio.h>

int i;
int main6() {
	i--;
	printf("%d\n", i);
	// sizeof 计算的结果是无符号整形
	// -1被转换为无符号整型时，会被转换为非常大的值
	if (i > sizeof(i)) {
		printf(">\n");
	}
	else {
		printf("<\n");
	}
	return 0;
}