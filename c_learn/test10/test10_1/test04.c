#define _CRT_SECURE_NO_WARNINGS

// 两个int整数，有多少位不同
#include <stdio.h>

int getDiff(int m, int n) {
	int count = 0;
	int i = 0;
	// 异或，相同为0否则为1
	int ret = m ^ n;
	// 获取ret1的个数，就是有多少位不同
	while (ret) {
		ret = ret & (ret - 1);
		count++;
	}

	return;
}


int main4() {
	int m = 0;
	int n = 0;
	scanf("%d %d", &m, &n);
	int ret = getDiff(m, n);
	printf("%d\n", ret);

	return 0;
}