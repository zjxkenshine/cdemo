#define _CRT_SECURE_NO_WARNINGS

// 写一个函数返回二进制中1的个数
// 按位与方式
#include <stdio.h>

int count_num_of2(unsigned int n) {
	int i = 0;
	int count = 0;
	for (i = 0;i < 32;i++) {
		if (((n >> i) & 1) == 1) {
			count++;
		}
	}
	return count;
}


int main2() {
	int num = 0;
	scanf("%d", &num);
	// n中1的个数
	int n = count_num_of2(num);
	printf("%d\n", n);
	return 0;
}