#define _CRT_SECURE_NO_WARNINGS

// 求二进制中1的个数
#include <stdio.h>

// n&(n-1) 每执行一次，最右边的1就去掉了
int count_num_of3(unsigned int n) {
	int count = 0;
	while (n) {
		// n = n & (n - 1) == 0 也可以判断n是否是2的n次方
		n = n & (n - 1);
		count++;
	}
	return count;
}


int main3() {
	int num = 0;
	scanf("%d", &num);
	// n中1的个数
	int n = count_num_of3(num);
	printf("%d\n", n);
	return 0;
}