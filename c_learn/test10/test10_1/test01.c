#define _CRT_SECURE_NO_WARNINGS

// 写一个函数返回二进制中1的个数
// 模2方式
#include <stdio.h>

int count_num_of_1(unsigned int n) {
	int count = 0;
	while (n>=1) {
		if ((n % 2) == 1) {
			count++;
		}
		n = n/2;
	}
	return count;
}

int main1() {
	int num = 0;
	scanf("%d", &num);
	// n中1的个数
	int n = count_num_of_1(num);
	printf("%d\n", n);
	return 0;
}