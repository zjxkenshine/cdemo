#define _CRT_SECURE_NO_WARNINGS
// ��һ�仰�ĵ��ʵ��ã���㲻����
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>

void reverse(char* left, char* right) {
	assert(left);
	assert(right);
	while (left < right) {
		char tmp = *left;
		*left = *right;
		*right = tmp;
		left++;
		right--;
	}
}

int main() {
	char arr[101] = { 0 };
	//����
	gets(arr);
	// �����ַ���
	int len = strlen(arr);
	reverse(arr, arr + len - 1);
	printf("%s\n", arr);
	// ����ÿ������
	char* start = arr;
	char* end = start;
	while (*start) {
		while (*end != ' '&&*end!='\0') {
			end++;
		}
		reverse(start, end - 1);
		if (*end != '\0') {
			end++;
		}
		start = end;
	}
	// ���
	printf("%s\n", arr);
	return 0;
}