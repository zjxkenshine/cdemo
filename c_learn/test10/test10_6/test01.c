#define _CRT_SECURE_NO_WARNINGS

//求最小公倍数
#include <stdio.h>

int main1() {
	int a = 0;
	int b = 0;
	scanf("%d %d", &a, &b);
	// 计算a和b的最小公倍数
	int i = 1;
	while (a*i%b) {
		i++;
	}
	// 打印
	printf("%d\n", i * a);
	return 0;
}
