#define _CRT_SECURE_NO_WARNINGS

// 自定义实现strlen
#include <stdio.h>
#include <assert.h>

size_t mystrlen(const char* str) {
	size_t count = 0;
	assert(str);
	while (*str != '\0') {
		count++;
		str++;
	}
	return count;
}


int main3() {
	char arr[] = "abcdef";
	size_t n = mystrlen(arr);
	printf("%d\n", n);
	return 0;
}