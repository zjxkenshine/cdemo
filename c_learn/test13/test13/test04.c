#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
#include<string.h>

// strcpy 字符串复制
int main4() {
	char name[20] = { 0 };
	strcpy(name, "kenshine");
	printf("%s\n", name);
	// 到\0就结束，不会复制
	strcpy(name, "kenshi\0ne");
	printf("%s\n", name);

	char name1[3] = { 0 };
	// 错误目标空间太小
	strcpy(name1, "kenshine");
	printf("%s\n", name1);
	return 0;
}
