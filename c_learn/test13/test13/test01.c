#define _CRT_SECURE_NO_WARNINGS

// strlen 返回值为unsign int
#include <stdio.h>
#include <string.h>

int main1() {
	// 不能用-比较大小
	// 错误方式
	if (strlen("abc") - strlen("abcdef") > 0) {
		printf(">");
	}
	else {
		printf("<");
	}
	return 0;
}