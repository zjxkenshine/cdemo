#define _CRT_SECURE_NO_WARNINGS

// strlen 返回值为unsign int
#include <stdio.h>
#include <string.h>

int main2() {
	// 不能用-比较大小
	// 正确比较方式
	if (strlen("abc")> strlen("abcdef")) {
		printf(">");
	}
	else {
		printf("<");
	}
	return 0;
}