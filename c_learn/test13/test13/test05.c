#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
#include<assert.h>

// 返回目标空间代码
char* my_strcpy(char* dest,char* src) {
	assert(dest);
	assert(src);
	char* ret = dest;
	while (*dest++ = *src++) {
		;
	}
	return ret;
}

int main5() {
	char arr1[] = "abcdef";
	char arr2[20] = { 0 };
	// 自定义strcpy函数实现
	my_strcpy(arr2, arr1);
	printf("%s\n", arr2); //abcdef
	return 0;
}