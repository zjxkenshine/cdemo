#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <ctype.h>
// 大小写转化
int main() {
	int i = 0;
	char str[] = "Test String.\n";
	char c;
	while (str[i]) {
		c = str[i];
		// 是大写
		if (isupper(c))
			c = tolower(c);	// 转小写
		putchar(c);
		i++;
	}
	return 0;
}