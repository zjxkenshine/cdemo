#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <string.h>
// strncpy 定长复制
int main3() {
	char arr1[] = "abcdef";
	char arr2[] = "kenshine6666666";
	// 会报错，数组长度不够长
	//strcpy(arr1, arr2);
	//不会报错，指定长度
	strncpy(arr1,arr2,6);

	printf("%s\n", arr1);
	return 0;
}

