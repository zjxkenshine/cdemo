#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <string.h>
// strcmp比较两个字符串是否相等
int main1() {
	char arr1[20] = "zhangsan";
	char arr2[] = "zhangsanfeng";
	// 比较ascii 码值
	// 比较两个字符串是否相等
	if (strcmp(arr1, arr2) == 0) {
		printf("相等");
	}
	else {
		printf("不相等");
	}
	return 0;
}