#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <string.h>

// strncmp 定长比较
int main5() {
	char arr1[] = "abcdef";
	char arr2[] = "abc";

	int ret1 = strcmp(arr1,arr2);
	// 只比较3个字符
	int ret2 = strncmp(arr1, arr2,3);
	if (ret1 == 0) {
		printf("ret1相等\n");
	}
	else {
		printf("ret1不相等\n");
	}

	if (ret2 == 0) {
		printf("ret2相等\n");
	}
	else {
		printf("ret2不相等\n");
	}
	return 0;
}