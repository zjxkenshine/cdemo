#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <assert.h>

int my_strcmp(const char* str1, const char* str2) {
	assert(str1&&str2);
	while (*str1 == *str2) {
		if (*str1 == '\0') {
			//相等
			return 0; 
		}
		str1++;
		str2++;
	}
	if(*str1 > *str2) 
		return 1;
	else 
		return -1;
	
}

// 自定义strcmp函数实现
int main2() {
	char arr1[20] = "zhangsan";
	char arr2[] = "zhangsanfeng";
	char arr3[] = "zhangsan";
	if (my_strcmp(arr1, arr2) == 0) {
		printf("a1 a2 相等");
	}
	else {
		printf("a1 a2  不相等");
	}

	if (my_strcmp(arr1, arr3) == 0) {
		printf("a1 a3 相等");
	}
	else {
		printf("a1 a3  不相等");
	}
	return 0;
}