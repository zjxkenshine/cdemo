#define _CRT_SECURE_NO_WARNINGS
// strncat 定长追加
#include <stdio.h>
#include <string.h>

int main4() {
	// strncat
	char arr1[10] = "hello ";
	char arr2[] = "kenshine";
	// 报错，超长了
	//strcat(arr1, arr2);
	// 只追加3个字符
	strncat(arr1, arr2, 3);

	printf("%s\n",arr1);
	return 0;
}