#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
#include<assert.h>

// 自定义memmove实现
void* my_memmove(void* dest, const void* src, size_t num) {
	assert(dest && src);
	void* ret = dest;
	// 从前往后
	if (dest < src) {
		while (num--) {
			*(char*)dest = *(char*)src;
			dest = (char*)dest + 1;
			src = (char*)src + 1;
		}
	}
	else {	// 从后往前
		while (num--) {
			*((char*)dest + num) = *((char*)src + num);
		}
	}
	return ret;
}

int main4() {
	int arr[] = { 0,1,2,3,4,5,6,7,8,9,10 };
	// memmove 在有重复的时候可以拷贝 从后往前拷贝
	// 20个字节，5个数
	my_memmove(arr + 2, arr, 20);

	int i = 0;
	for (i = 0;i < 10;i++) {
		printf("%d ", arr[i]);
	}
	return 0;
}