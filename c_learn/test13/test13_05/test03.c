#define _CRT_SECURE_NO_WARNINGS
// memmove 可拷贝重复地址
#include<stdio.h>
#include <string.h>

int main3() {
	int arr1[] = { 0,1,2,3,4,5,6,7,8,9,10 };
	// memmove 在有重复的时候可以拷贝 从后往前拷贝
	// 20个字节，5个数
	memmove(arr1 + 2, arr1, 20);
	return 0;
}