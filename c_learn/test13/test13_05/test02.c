#define _CRT_SECURE_NO_WARNINGS
// 自定义实现memcpy
#include<stdio.h>
#include<assert.h>

// 返回目标地址
void* my_memcpy(void* dest, void* src, size_t num) {
	assert(dest && src);
	// 目标地址，用于返回
	void* ret = dest;
	while (num--) {
		*(char*)dest = *(char*)src;
		// 下一个地址
		dest = (char*)dest + 1;
		src = (char*)src + 1;
	}
	return ret;
}
int main2() {
	int arr1[] = { 0,1,2,3,4,5,6,7 };
	int arr2[] = { 0 };
	my_memcpy(arr2, arr1, 28);
	// 不能进行自拷贝
	//my_memcpy(arr1+2, arr1, 20);
	return 0;
}