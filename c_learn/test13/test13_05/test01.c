﻿#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <string.h>
struct {
	char name[40];
	int age;
} person, person_copy;
// memcpy ppt示例
int main1(){
	char myname[] = "Pierre de Fermat";
	memcpy(person.name, myname, strlen(myname) + 1);
	person.age = 46;
	// 可进行结构体复制
	memcpy(&person_copy, &person, sizeof(person));
	printf("person_copy: %s, %d \n", person_copy.name, person_copy.age);
	return 0;
}