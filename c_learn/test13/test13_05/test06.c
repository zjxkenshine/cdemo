#define _CRT_SECURE_NO_WARNINGS

// memset 内存设置
#include<stdio.h>
#include<string.h>

int main() {
	char arr[] = "hello bit";
	//前5个改为a
	memset(arr, "a", 5);
	printf("%s\n", arr);
	return 0;
}