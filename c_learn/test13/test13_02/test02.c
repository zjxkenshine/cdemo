#define _CRT_SECURE_NO_WARNINGS
// 自定义strstr实现
#include<stdio.h>
#include<assert.h>

char* my_strstr(const char* str1,const char* str2) {
	assert(str1 && str2);
	const char* s1 = str1;
	const char* s2 = str2;
	const char* p = str1;
	while (*p) {
		s1 = p;
		s2 = str2;
		// 相等
		while(*s1 != '\0' && *s2 != '\0' && *s1 == *s2) {
			s1++;
			s2++;
		}
		if (*s2 == '\0') {
			return (char*)p;
		}
		p++;
	}
	return NULL;
}

int main2(){
	char email[] = "www.kenshine@qq.com";
	char substr[] = "kenshine";
	// strstr子串查找，能找到返回子串到末尾的值
	char* ret = my_strstr(email, substr);
	if (ret == NULL) {
		printf("子串不存在");
	}
	else {
		printf("%s\n", ret);
	}
	return 0;
}