#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <string.h>

int main1() {
	char email[] = "www.kenshine@qq.com";
	char substr[] = "kenshine";
	// strstr子串查找，能找到返回子串到末尾的值
	char* ret = strstr(email, substr);
	if (ret == NULL) {
		printf("子串不存在");
	}
	else {
		printf("%s\n",ret);
	}
	return 0;
}