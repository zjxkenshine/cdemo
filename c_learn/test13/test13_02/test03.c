#define _CRT_SECURE_NO_WARNINGS
// 这种方法不太好
#include <stdio.h>
#include<string.h>
// strtok 拆分字符串
int main3() {
	// 分隔符
	const char* sep = "@";
	// 空字符串会往下找，不会返回(两个@之间)
	char email[] = "kenshine@@666@hong";
	char cp[50] = { 0 };
	// strtok操作源字符串，需要拷贝
	strcpy(cp, email);

	char* ret = strtok(cp, sep);
	printf("%s\n", ret);
	// 传空指针往下找
	ret = strtok(NULL, sep);
	printf("%s\n", ret);
	ret = strtok(NULL, sep);
	printf("%s\n", ret);
	ret = strtok(NULL, sep);
	printf("%s\n", ret);
	return 0;
}