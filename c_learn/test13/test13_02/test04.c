#define _CRT_SECURE_NO_WARNINGS

// strtok 更好的写法
#include <stdio.h>
#include<string.h>
// strtok 拆分字符串
int main() {
	// 分隔符
	const char* sep = "@";
	// 空字符串会往下找，不会返回(两个@之间)
	char email[] = "kenshine@@666@hong@qing@kkk";
	char cp[50] = { 0 };
	// strtok操作源字符串，需要拷贝
	strcpy(cp, email);

	char* ret = NULL;
	for (ret = strtok(cp, sep);ret != NULL;ret = strtok(NULL, sep)) {
		printf("%s\n", ret);
	}

}