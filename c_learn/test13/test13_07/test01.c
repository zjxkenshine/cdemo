#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
// 判断一个字符串是否由另一个旋转得到

int is_left_move(char arr1[], char arr2[]) {
	int len = strlen(arr1);
	int i = 0;
	for (i = 0;i < len;i++) {
		char tmp = arr1[0];
		// 旋转一次
		int j = 0;
		for (j = 0;j < len - 1;j++) {
			arr1[j] = arr1[j + 1];
		}
		arr1[len - 1] = tmp;
		// 每次旋转比较
		if (strcmp(arr2, arr1) == 0) {
			return 1;
		}
	}
	return 0;
}

int main1() {
	char arr1[] = "abcdef";
	char arr2[] = "cdefab";
	// 判断arr2中的字符串是否可以通过arr1中的字符串旋转得到
	int ret = is_left_move(arr1, arr2);
	if (ret == 1) {
		printf("是");
	}
	else {
		printf("不是");
	}

	return 0;
}