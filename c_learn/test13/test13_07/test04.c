#define _CRT_SECURE_NO_WARNINGS

// 判断n阶方矩阵，是否为上三角矩阵
// 主对角线以下的元素都为0，主对角线为左上到右下连线
#include<stdio.h>

int main4() {
	int n = 0;
	scanf("%d", &n);
	int arr[10][10];
	int i = 0;
	int j = 0;
	int flag = 1;
	for (i = 0;i < n;i++) {
		for (j = 0;j < n;j++) {
			scanf("%d", &arr[i][j]);
		}
	}
	// 判断是否是上三角
	for (i = 0;i < n;i++) {
		for (j = 0;j < i;j++) {
			if (arr[i][j] != 0) {
				flag = 0;
				goto end;
			}
		}
	}
end:
	if (flag == 0) {
		printf("NO\n");
	}
	else {
		printf("YES\n");
	}
	return 0;
}