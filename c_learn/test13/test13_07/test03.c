#define _CRT_SECURE_NO_WARNINGS

// 转置数组
int main3() {
	int n = 0;
	int m = 0;
	scanf("%d %d", &n, &m);
	int arr[10][10] = { 0 };
	int j = 0;
	int i = 0;
	// 输入
	for (i = 0;i < n;i++) {
		for (j = 0;j < m;j++) {
			scanf("%d", &arr[i][j]);
		}
	}
	// 输出
	for (i = 0;i < m;i++) {
		for (j = 0;j < n;j++) {
			printf("%d ", arr[j][i]);
		}
		printf("\n");
	}
	return 0;
}