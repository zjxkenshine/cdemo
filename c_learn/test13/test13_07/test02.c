#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
#include<string.h>

// 判断一个字符串是否由另一个旋转得到
// arr1追加arr1后，包含所有旋转过后的字符串
int is_left_move1(char arr1[], char arr2[]) {
	int len1 = strlen(arr1);
	int len2 = strlen(arr2);
	// 判断长度
	if (len1 != len2) {
		return 0;
	}
	// 自己追加自己
	strncat(arr1, arr1,len1);

	// 在arr1中找arr2
	char* ret = strstr(arr1, arr2);

	if (ret==NULL) {
		return 0;
	}
	else {
		return 1;
	}
}

int main2() {
	char arr1[20] = "abcdef";
	char arr2[] = "cdefab";
	// 判断arr2中的字符串是否可以通过arr1中的字符串旋转得到
	int ret = is_left_move1(arr1, arr2);
	if (ret == 1) {
		printf("是");
	}
	else {
		printf("不是");
	}
	return 0;
}
