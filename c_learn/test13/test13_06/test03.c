#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
// 价格计算，双十一0.7折，双十二0.8折
int main3() {
	double price = 0.0;
	int m = 0;
	int d = 0;
	int flag = 0;
	scanf("%lf %d %d %d", &price, &m, &d, &flag);
	// 计算
	if (m == 11 && d == 11) {
		price = price * 0.7 - flag * 50;
	}
	if (m == 12 && d == 12) {
		price = price * 0.8 - flag * 50;
	}
	// 输出
	if (price < 0.0) {
		printf("%.2f\n",0.0);
	}
	else {
		printf("%.2f\n", price);
	}
	return 0;
}