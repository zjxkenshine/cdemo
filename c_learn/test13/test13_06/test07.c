#define _CRT_SECURE_NO_WARNINGS
// 杨氏矩阵
// 每行从左到右递增，每列从上到下递增
#include <stdio.h>

int find_num(int arr[3][3], int r, int c, int k) {
	int x = 0;
	int y = c - 1;
	while (x <= r - 1 && y > 0) {
		if (k < arr[x][y]) {
			y--;
		}
		else if (k > arr[x][y]) {
			x++;
		}
		else {
			return 1;
		}
	}
	// 找不到
	return 0;
}

int main7() {
	int arr[3][3] = { 1,2,3,4,5,6,7,8,9 };
	int k = 0;
	scanf("%d",&k);
	int i=find_num(arr, 3, 3,k);
	if (i) {
		printf("找到了");
	}
	else {
		printf("找不到");
	}
	return 0;
}
