#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>

int find_num3(int arr[3][3], int* px, int* py, int k) {
	int x = 0;
	int y = *py - 1;
	while (x <= *px - 1 && y >= 0) {
		if (k < arr[x][y]) {
			y--;
		}
		else if (k > arr[x][y]) {
			x++;
		}
		else {
			*px = x;
			*py= y;
			return 1;
		}
	}
	*px = -1;
	*py = -1;
	return 0;
}

int main() {
	int arr[3][3] = { 1,2,3,4,5,6,7,8,9 };
	int k = 0;
	scanf("%d", &k);
	int x = 3; // 行
	int y = 3; // 列
	int i = find_num3(arr, &x, &y, k);
	if (i) {
		printf("找到了%d %d",x,y);
	}
	else {
		printf("找不到");
	}
	return 0;
}