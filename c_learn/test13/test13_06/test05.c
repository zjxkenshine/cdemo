#define _CRT_SECURE_NO_WARNINGS
// 实现一个函数，左旋字符串中的k个字符
#include<stdio.h>
#include<string.h>

// 实现方式1
void left_rotate(char arr[], int k) {
	int i = 0;
	int len = strlen(arr);
	for (i = 0;i < k;i++) {
		// 作旋转一个字节
		char tmp = arr[0];
		int j = 0;
		k %= len;
		for (j = 0;j < k;j++) {
			arr[j] = arr[j + 1];
		}
		arr[len - 1] = tmp;
	}
}

int main5() {
	char arr[] = "abcdef";
	int k = 0;
	scanf("%d", &k);
	// 坐旋转k个字符
	left_rotate(arr,k);
	printf("%s\n", arr);
	return 0;
}