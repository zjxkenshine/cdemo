#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
#include<math.h>

// 一串数字，奇数变为1，偶数变为0
int main1() {
	int input = 0;
	int sum = 0;
	// 输入
	scanf("%d",&input);
	int i = 0;
	while (input) {
		int bit = input % 10;
		if (bit % 2 == 1) {
			sum += 1 * pow(10, i);
			i++;
		}
		input /= 10;
	}

	return 0;
}