#define _CRT_SECURE_NO_WARNINGS


#include<stdio.h>
#include<assert.h>
#include<string.h>

void reverse1(char* left,char* right) {
	// assert
	assert(left && right);
	while (left < right) {
		char cmp = *left;
		*left = *right;
		*right = cmp;
		left--;
		right++;
	}
}

// ����ת��һ��д��
void left_rotate1(char arr[], int k) {
	int len = strlen(arr);
	k %= len;
	// ���
	reverse1(arr, arr + k - 1);
	// �ұ�
	reverse1(arr + k, arr + len - 1);
	// ��������
	reverse1(arr, arr + len - 1);
}

int main6() {
	char arr[] = "abcdef";
	int k = 0;
	scanf("%d", &k);
	// ����תk���ַ�
	left_rotate1(arr, k);
	printf("%s\n", arr);
	return 0;
}