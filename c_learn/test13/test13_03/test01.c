#define _CRT_SECURE_NO_WARNINGS

// strerror全局错误码
#include <stdio.h>
#include <string.h>
#include <errno.h>//必须包含的头文件

int main() {

	FILE* pFile;
	pFile = fopen("unexist.ent", "r");
	if (pFile == NULL){
		// 显示错误信息
		printf("Error opening file unexist.ent: %s\n", strerror(errno));
		return 1;
	}

	return 0;
}