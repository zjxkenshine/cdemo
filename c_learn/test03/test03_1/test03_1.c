#define _CRT_SECURE_NO_WARNINGS
// 处理scanf错误，一定要在第一行

// 变量的使用 两数求和
#include <stdio.h>
int main()
{
	int num1 = 0;
	int num2 = 0;
	int sum = 0;
	printf("输入两个操作数:>");
	// scanf 输入函数，&取址
	// 错误	C4996	'scanf': This function or variable may be unsafe. 参考第一行处理
	// scanf_s不是标准C，慎用
	// 求和
	scanf("%d %d", &num1, &num2);
	sum = num1 + num2;
	printf("sum = %d\n", sum);
	return 0;
}