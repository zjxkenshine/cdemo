#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

// 声明来自外部的符号
extern int a;

// 定义一个方法
void test() {
	printf("test-->%d\n", a);
}

int main() {
	// 引用方法
	test();
	{
		printf("a=%d\n", a);
	}
	printf("a=%d\n", a);
}