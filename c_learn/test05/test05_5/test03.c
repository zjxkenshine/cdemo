#include <stdio.h>
// do while中的break与continue
// 打印1-8，除了5
int main3()
{
	int i = 0;

	do
	{
		i++;
		if (5 == i)
			continue;
		if (9 == i)
			break;
		printf("%d\n", i);
	} while (i < 10);

	return 0;
}