#define _CRT_SECURE_NO_WARNINGS
// while循环
#include <stdio.h>

int main() {

	// while循环打印1-10数字	
	int i = 1;
	while (i <= 10)
	{
		i++;
		// 跳过本次循环
		if (5 == i)
			continue;

		// 跳过整个循环
		if (9 == i)
			break;

		printf("%d ", i);
		//i = i + 1;
	}

	// 这段代码的作用是只打印数字字符，跳过其他字符的
	char ch = '\0';
	// getchar输入字符，ctrl+z返回EOF结束
	while ((ch = getchar()) != EOF)
	{	
		// 仅打印数字
		if (ch < '0'||ch > '9')
			continue;
		// putchar  输出一个字符
		putchar(ch);
	}

	

	return 0;
}