#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// 关机程序
// goto实现
int main1()
{
	char input[10] = { 0 };
	system("shutdown -s -t 60");
again:
	printf("电脑将在1分钟内关机，如果输入：我是猪，就取消关机!\n请输入:>");
	scanf("%s", input);
	// 比较字符串是否相等
	if (0 == strcmp(input, "我是猪")){
		system("shutdown -a");
	}else{
		// 回到again处
		goto again;
	}
	return 0;
}