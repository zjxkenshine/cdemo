#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

// if else
int main() {

	int age = 10;

	if (age < 18) {
		printf("青少年\n");
	}
	else if (age >= 18 && age < 28) {
		printf("青年\n");
	}
	else if (age >= 28 && age < 40) {
		printf("中年\n");
	}
	else if (age >= 40 && age < 60) {
		printf("壮年\n");
	}
	else if (age >= 60 && age < 100) {
		printf("老年\n");
	}
	else {
		printf("老寿星\n");
	}

	// 此段代码无输出
	int a = 0;
	int b = 2;
	if (a == 1)
		if (b == 2)
			printf("hehe\n");
	else // 此处else匹配最后一个if，与对齐无关
		printf("haha\n");


	// 数字写前面可以防止少写=错误
	int num = 3;
	if (5 == num) {	// 可以防止出现if（num=5）这种错误
		printf("hehe\n");
	}

	// 判断是否为奇数
	int a1 = 0;
	scanf("%d\n", &a1);
	// 判断
	if (num % 2 == 1) {
		printf("奇数\n");
	}
	else {
		printf("偶数\n")
	}

	return 0;
}