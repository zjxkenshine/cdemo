#define _CRT_SECURE_NO_WARNINGS

// switch语句
#include <stdio.h>

int main() {
	int day = 0;
	switch (day)
	{
		case 1:
			printf("星期一\n");
			break;
		case 2:
			printf("星期二\n");
			break;
		case 3:
			printf("星期三\n");
			break;
		case 4:
			printf("星期四\n");
			break;
		case 5:
			printf("星期五\n");
			break;
		case 6:
			printf("星期六\n");
			break;
		case 7:
			printf("星期天\n");
			break;
		// 默认值，所有都不匹配
		default:
			printf("未知日期\n");
			break;
	}

	// 练习
	int n = 1;
	int m = 2;
	switch (n)
	{
	case 1:
		m++;
	case 2:
		n++;
	case 3:
		switch (n)
		{//switch允许嵌套使用
		case 1:
			n++;
		case 2:
			m++;
			n++;
			break;
		}
	case 4:
		m++;
		break;
	default:
		break;
	}
	// M=5 N=3
	printf("m = %d, n = %d\n", m, n);
	return 0;
}