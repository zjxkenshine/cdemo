﻿#define _CRT_SECURE_NO_WARNINGS

// 猜数字游戏

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// 打印菜单
void menu() {
	printf("**********************************\n");
	printf("*********** 1.play     **********\n");
	printf("*********** 0.exit     **********\n");
	printf("**********************************\n");
}


// 选择菜单
int main() {
	int input = 0;
	// srand生成随机数 设置随机数生成器，防止生成的随机数顺序相同
	// time生成时间戳
	srand((unsigned)time(NULL));
	do {
		menu();
		printf("请选择:>");
		scanf("%d", &input);
		switch (input) {
		case 1:
			game();
			break;
		case 0:
			printf("退出");
			break;
		default:
			printf("选择错误,请重新输入!\n");
			break;
		}
	} while (input);
	return 0;
}


// RAND_MAX--rand函数能返回随机数的最大值。
// 猜数字
void game() {
	// 生成1-100之间的数字
	int random_num = rand() % 100 + 1;
	int input = 0;
	while (1){
		printf("请输入猜的数字>:");
		scanf("%d", &input);
		if (input > random_num)
		{
			printf("猜大了\n");
		}
		else if (input < random_num)
		{
			printf("猜小了\n");
		}
		else
		{
			printf("恭喜你，猜对了\n");
			break;
		}
	}
}