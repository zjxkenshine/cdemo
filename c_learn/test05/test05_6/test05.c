#define _CRT_SECURE_NO_WARNINGS

//编写代码实现，模拟用户登录情景，并且只能登录三次。（只允许输入三次密码，如果密码正确则
// 提示登录成，如果三次均输入错误，则退出程序。
#include <stdio.h>

int main5(){
	char psw[10] = "";
	int i = 0;
	int j = 0;
	for (i = 0; i < 3; ++i)
	{
		printf("输入密码:");
		scanf("%s", psw);
		// 比较两个字符串是否相等，不能使用== 
		// 返回值是0表示相等
		if (strcmp(psw, "kenshine") == 0)
			break;
	}
	if (i == 3)
		printf("密码错误3次，退出\n");
	else
		printf("登录成功\n");

	return 0;
}