#define _CRT_SECURE_NO_WARNINGS

// 编写代码，演示多个字符从两端移动，向中间汇聚
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>

int main4() {
	char arr1[] = "welcome to bit...";
	char arr2[] = "#################";
	int left = 0;
	// 字符串长度 strlen不会计算\0的长度
	int right = strlen(arr1) - 1;
	printf("%s\n", arr2);
	//while循环实现
	while (left <= right){
		//休眠1秒
		Sleep(1000);
		//清空屏幕 cmd cls命令
		system("cls");// system执行系统函数
		arr2[left] = arr1[left];
		arr2[right] = arr1[right];
		left++;
		right--;
		printf("%s\n", arr2);
	}
	//for循环实现
	//for (left = 0, right = strlen(src) - 1;
	//	left <= right;
	//	left++, right--)
	//{
	//	Sleep(1000);
	//	arr2[left] = arr1[left];
	//	arr2[right] = arr1[right];
	//	printf("%s\n", target);
	//}

	return 0;
}