#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
// 在一个有序数组中查找具体的某个数字n 二分查找

int main3() {
	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
	int left = 0;
	// 长度-1，最右边下标
	int right = sizeof(arr) / sizeof(arr[0]) - 1;
	// 查找7
	int key = 7;
	int mid = 0;
	while (left <= right){
		mid = (left + right) / 2;
		if (arr[mid] > key){
			right = mid - 1;
		}else if (arr[mid] < key){
			left = mid + 1;
		}else
			// =跳出
			break;
	}
	if (left <= right)
		printf("找到了,下标是%d\n", mid);
	else
		printf("找不到\n");
	return 0;
}