#define _CRT_SECURE_NO_WARNINGS
// 输出4个整数中的最大的数
#include <stdio.h>

int main7() {
	int arr[4] = {0};
	int i = 0;
	while (i < 4) {
		scanf("%d", &arr[i]);
		i++;
	}
	// 找最大值
	int max = arr[0];
	i = 1;
	while (i < 4) {
		if (arr[i] > max) {
			max = arr[i];
		}
		i++;
	}
	printf("最大值为：%d",max);
	return 0;
}