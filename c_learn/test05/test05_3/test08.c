#define _CRT_SECURE_NO_WARNINGS

// 给定球的半径，求球的体积 保留3位
#include <stdio.h>

int main8() {
	float r = 0.0f;
	float v = 0.0f;
	scanf("%f",&r);
	// 计算球的体积
	v = 4 / 3.0 * 3.1415926 * r * r * r;
	printf("%.3f\n", v);
	return 0;
}