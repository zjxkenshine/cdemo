#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

// 输入一个人的出生日期，将该生日中年月日分别输出
// 19980125

int main2() {

	// 输入数据
	int year = 0;
	int month = 0;
	int day = 0;
	// %md 限制输入位数
	scanf("%4d%2d%2d", &year, &month, &day);

	// 输出 %0 设置自动填充0
	printf("year=%d\n", year);
	printf("month=%02d\n", month);
	printf("day=%02d\n", day);

	return 0;
}