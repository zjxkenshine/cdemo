#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
// 输入学号0-2000000，成绩，并格式化输出，0-100，两位小数

int main3() {
	int id = 0;
	float c = 0.0f;
	float math = 0.0f;
	float eng = 0.0f;

	// 输入
	scanf("%d;%f,%f,%f", &id, &c, &math, &eng);
	// 输出 %.2f 保留两位小数
	printf("学号%d的学生的三科成绩为：%.2f,%.2f,%.2f", id, c, math, eng);

	return 0;
}