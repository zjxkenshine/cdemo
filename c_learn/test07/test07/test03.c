#define _CRT_SECURE_NO_WARNINGS

// 数组的访问与打印
#include <stdio.h>

int main3() {
	int arr[] = { 1,2,3,4,5,6,7,8,9,0 };
	int i = 0;
	int size = sizeof(arr) / sizeof(arr[0]);
	for (i = 0;i < size;i++) {
		printf("%d", arr[i]);
	}
	return 0;
}