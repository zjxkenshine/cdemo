#define _CRT_SECURE_NO_WARNINGS

int main2() {
	// 不完全初始化，其他元素默认为0
	int arr[10] = { 1,2,3 };
	// 完全初始化
	int arr1[10] = { 1,2,3,4,5,6,7,8,9,0 };
	int arr2[] = { 1,2,3,4 };
	int arr3[5] = { 1,2,3,4,5 };
	char arr4[3] = { 'a',98, 'c' };
	char arr5[] = { 'a','b','c' };
	// 字符串多一个\0
	char arr6[] = "abcdef";
	return 0;
}