#define _CRT_SECURE_NO_WARNINGS

// 数组创建
int main1() {
	//代码1
	int arr1[10];
	//代码2
	int count = 10;
	//int arr2[count];//只能在支持C99的机器上运行
	// gcc编译器支持c99标准
	//代码3
	char arr3[10];
	float arr4[1];
	double arr5[20];

	return 0;
}