#define _CRT_SECURE_NO_WARNINGS

#include "game.h"

void menu() {
	printf("*************************************\n");
	printf("********* 1.play   0.exit ***********\n");
	printf("*************************************\n");
}

void game() {
	// 存放布置好的雷的信息
	char mine[ROWS][COLS] = { 0 };
	// 存放排查出的雷的信息
	char show[ROWS][COLS] = { 0 };

	// 初始化数组内容为指定内容
	// mine数组在没有布置雷时都为0
	initBoard(mine,ROWS,COLS,'0');
	// show数组在没有排查雷时都是*
	initBoard(show, ROWS, COLS,'*');

	// 打印数组
	//displayBoard(mine, ROW, COL);
	// 设置雷
	setMine(mine, ROW, COL);
	// 打印数组
	displayBoard(show, ROW, COL);

	// 排查雷
	findMine(mine,show, ROW, COL);
}


int main() {
	// 设置随机数种子
	srand((unsigned)time(NULL));

	int input = 0;
	do {
		menu();
		printf("请选择:");
		scanf("%d", &input);
		switch (input) {
		case 1:
			game();
			break;
		case 0:
			printf("退出游戏\n");
			break;
		default:
			printf("选择错误\n");
			break;
		}
	} while (input);

	return 0;
}