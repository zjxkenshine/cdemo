#pragma once

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define ROW 9
#define COL 9

// 周围一圈，不显示及使用
#define ROWS ROW+2
#define COLS COL+2

#define EASY_COUNT 10

// 初始化数组
void initBoard(char arr[ROWS][COLS], int rows, int cols,char set);

// 打印数组
void displayBoard(char arr[ROWS][COLS], int rows, int cols);

// 设置雷
void setMine(char arr[ROWS][COLS], int row, int col);

// 排查雷
void findMine(char mine[ROWS][COLS], char show[ROWS][COLS], int row, int col);


