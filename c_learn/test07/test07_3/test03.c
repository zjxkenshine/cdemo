#define _CRT_SECURE_NO_WARNINGS

// 一维数组 数组名的含义
#include <stdio.h>

int main3() {
	int arr[10] = { 1,2,3,4,5 };

	// 表示第一个元素地址
	printf("%p\n", arr);
	printf("%p\n", arr+1);
	// 首元素地址
	printf("%p\n", &arr[0]);
	printf("%p\n", &arr[0]+1);
	// 表示整个数组的地址
	printf("%p\n", &arr);
	// +1表示跳过整个数组的地址
	printf("%p\n", &arr + 1);
	// 数组的指针
	printf("%d\n", *arr);
	// 这里数组名表示整个数组
	printf("%d\n", sizeof(arr));
	return 0;
}