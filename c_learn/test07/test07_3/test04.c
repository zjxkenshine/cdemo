#define _CRT_SECURE_NO_WARNINGS

// 二维数组数组名的含义
#include <stdio.h>

int main() {
	int arr[3][4];
	// sizeof 整个数组的长度
	int sz = sizeof(arr);
	printf("%d\n", sz);

	// 二维数组数组名也表示首元素(arr[0][n])的地址
	// 首元素为第一行，而不是arr[0][0]
	printf("%p\n", arr);
	printf("%p\n", arr + 1);
	// 首元素地址
	printf("%p\n", &arr[0][0]);
	printf("%p\n", &arr[0][0] + 1);
	// 整个数组地址
	printf("%p\n", &arr);
	printf("%p\n", &arr + 1);

	// 计算行大小
	int sz1 = sizeof(arr) /sizeof(arr[0]);
	// 计算列大小
	int sz2 = sizeof(arr[0]) / sizeof(arr[0][0]);
	printf("%d\n",sz1);
	printf("%d\n",sz2);
	return 0;
}