#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

// 冒泡排序 传递数组对象 错误方式
void bubble_sort(int arr[]){
	// 函数内部计算sz 错误 数组名本质上是首元素的地址
	int sz = sizeof(arr) / sizeof(arr[0]);

	printf("%d\n",sz);
	int i = 0;
	for (i = 0; i < sz - 1; i++){
		// 一趟冒泡排序
		int j = 0;
		// 0- sz-i-1进行排序
		for (j = 0; j < sz - i - 1; j++){
			if (arr[j] > arr[j + 1]){
				int tmp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = tmp;
			}
		}
	}
}


int main1(){
	int arr[] = { 3,1,7,5,8,9,0,2,4,6 };
	// 冒泡排序
	bubble_sort(arr);
	int i = 0;
	for (i = 0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		printf("%d ", arr[i]);
	}
	return 0;
}