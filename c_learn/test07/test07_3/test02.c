#define _CRT_SECURE_NO_WARNINGS

// 正确方式，传递数组对象时，在函数外部求数组长度
#include <stdio.h>

// 函数外部计算数组长度并传入
void bubble_sort1(int arr[],int sz) {

	printf("%d\n", sz);
	int i = 0;
	for (i = 0; i < sz - 1; i++) {
		// 一趟冒泡排序
		int j = 0;
		// 0- sz-i-1进行排序
		for (j = 0; j < sz - i - 1; j++) {
			if (arr[j] > arr[j + 1]) {
				int tmp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = tmp;
			}
		}
	}
}

int main2() {
	int arr[] = { 3,1,7,5,8,9,0,2,4,6 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	// 冒泡排序
	bubble_sort1(arr, sz);
	int i = 0;
	for (i = 0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		printf("%d ", arr[i]);
	}
	return 0;


	return 0;
}