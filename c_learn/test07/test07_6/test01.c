#define _CRT_SECURE_NO_WARNINGS

// 逗号表达式，返回最后一个表达式的结果
#include <stdio.h>

int main1() {
	// 逗号表达式 实际存储的是 1 2 4 5
	int arr[] = { 1,2,(3,4),5 };
	printf("%d\n", sizeof(arr));
	return 0;
}