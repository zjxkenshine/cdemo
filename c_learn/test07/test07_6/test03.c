#define _CRT_SECURE_NO_WARNINGS

// 交换数组 A数组与B数组交换 数组一样大
#include <stdio.h>

int main3() {
	int arr1[] = { 1,3,5,7,9 };
	int arr2[] = { 2,4,6,8,0 };
	int i = 0;
	int sz = sizeof(arr1) / sizeof(arr1[0]);
	for (i = 0;i < sz;i++) {
		int tmp = arr1[i];
		arr1[i] = arr2[i];
		arr2[i] = tmp;
	}

	for (i = 0;i < sz;i++) {
		printf("%d ", arr1[i]);
	}
	printf("\n");
	for (i = 0;i < sz;i++) {
		printf("%d ", arr2[i]);
	}
	return 0;
}

