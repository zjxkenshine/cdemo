#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main2() {
	int num = 10;
	// 数组arr的类型是 int [10]
	int arr[10] = { 0 };

	// 长度相同
	printf("%d\n", sizeof(arr));
	printf("%d\n", sizeof(int [10]));
	return 0;
}