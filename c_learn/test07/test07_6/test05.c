#define _CRT_SECURE_NO_WARNINGS

// 变长数组 vs不支持
#include <stdio.h>

int main() {
	// 支持变长数组的编译器，数组大小可以是变量
	int n = 0;
	scanf("%d", &n);
	// 变长数组不能初始化
	int arr[n];
	int i = 0;
	// 输入
	for (i = 0;i < n;i++) {
		scanf("%d", &arr[i]);
	}
	for (i = 0;i < n;i++) {
		printf("%n\n", arr[i]);
	}
	return 0;
}