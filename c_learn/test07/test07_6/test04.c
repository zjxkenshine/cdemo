#define _CRT_SECURE_NO_WARNINGS


// 创建整形数组，完成对数组的操作
// init() 初始化数组全为0
// print() 打印数组每个元素
// reverse() 数组逆序
#include <stdio.h>

void init(int arr[], int sz) {
	int i = 0;
	for (i = 0;i < sz;i++) {
		arr[i] = 0;
	}
}

void print(int arr[], int sz) {
	int i = 0;
	for (i = 0;i < sz;i++) {
		printf("%d ",arr[i]);
	}
	printf("\n");
}

void reverse(int arr[], int sz) {
	int left = 0;
	int right = sz-1;
	while (left < right) {
		int tmp = arr[left];
		arr[left] = arr[right];
		arr[right] = tmp;
		left++;
		right--;
	}
}


int main4() {
	int arr[10] = { 1,2,3,4,5,6,7,8,9,0 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	print(arr, sz);

	reverse(arr, sz);
	print(arr, sz);

	init(arr, sz);
	print(arr, sz);

	return 0;
}