#define _CRT_SECURE_NO_WARNINGS

// 二维数组越界访问
#include <stdio.h>

int main() {
	int arr[3][4] = { 1,2,3,4,2,3,4,5,3,4,5,6 };
	int i = 0;
	for (i = 0;i < 3;i++) {
		int j = 0;
		// 列坐标越界，最多只有3
		for (j = 0;j <=4;j++) {
			printf("%d ", arr[i][j]);
		}
		printf("\n");
	}
	return 0;
}