#define _CRT_SECURE_NO_WARNINGS
// 数组越界访问 一维数组
#include <stdio.h>

int main1() {

	int arr[5] = { 1,2,3,4,5 };
	int i = 0;
	for (i = 0; i <= 10; i++){
		//越界访问
		printf("%d\n", arr[i]);
	}
	// 不报错，会打印出后续内存中的数字
	return 0;
}