#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define ROW 3
#define COL 3

// ��ʼ������
void initBoard(char board[ROW][COL],int row,int col);

// ��ӡ���� 1�汾
void displayBoard1(char board[ROW][COL], int row, int col);

// ��ӡ���� 2�汾
void displayBoard(char board[ROW][COL], int row, int col);

// �������
void playerMove(char board[ROW][COL], int row, int col);

// ��������
void computerMove(char board[ROW][COL], int row, int col);

// �ж���Ӯ
char isWin(char board[ROW][COL], int row, int col);

