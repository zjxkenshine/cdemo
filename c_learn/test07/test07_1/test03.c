#define _CRT_SECURE_NO_WARNINGS

// 二进制数组赋值
#include <stdio.h>

int main3() {
	int arr[3][4] = { 1,2,3,4,2,3,4,5,3,4,5,6 };
	int i = 0;
	for (i = 0;i < 3;i++) {
		int j = 0;
		for (j = 0;j < 4;j++) {
			arr[i][j] = 1;
			printf("%d ", arr[i][j]);
		}
		printf("\n");
	}

	printf("【2,0】位置的元素是：%d ", arr[2][0]);
	return 0;
}