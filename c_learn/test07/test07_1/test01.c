#define _CRT_SECURE_NO_WARNINGS
// 二维数组 初始化
#include <stdio.h>

int main1() {
	// 3行4列
	int arr1[3][4] = {1,2,3,4,2,3,4,5,3,4};
	int arr2[3][4] = { {2,3},{3,4},{4,5} };

	// 行可以省略，列不能省略
	int arr3[][4] = { {2,3},{4,5} };

	return 0;
}
