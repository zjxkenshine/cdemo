#define _CRT_SECURE_NO_WARNINGS

// 递归实现，输入一个整数，求各个位置数字之和
#include <stdio.h>

int DigitSum(n) {
	if (n > 9) {
		return DigitSum(n / 10) + n % 10;
	}
	else {
		return n;
	}
}

int main4() {
	unsigned int n = 0;
	scanf("%u", &n);
	int sum = DigitSum(n);
	printf("%d\n", sum);
	return 0;
}
