#define _CRT_SECURE_NO_WARNINGS

// 字符串逆序，非递归实现
#include <stdio.h>
#include <string.h>

int main1() {
	char arr[] = "abcdef";
	// sz=7 包含\0
	int sz = sizeof(arr) / sizeof(arr[0]);
	int left = 0;
	int right = sz - 2;
	//另一种方式
	//int right = strlen(arr) - 1;
	while (left<right) {
		char tmp = arr[left];
		arr[left] = arr[right];
		arr[right] = tmp;
		left++;
		right--;
	}
	printf("%s\n", arr);
	return 0;
}
