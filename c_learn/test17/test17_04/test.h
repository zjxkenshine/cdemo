#define _CRT_SECURE_NO_WARNINGS
// 解决方案2 更现代的解决方案
#pragma once

// 解决方案1 条件编译，避免重复#include时多次编译
#ifndef __TEST_H__
#define __TEST_H__

int Add(int x, int y);

#endif