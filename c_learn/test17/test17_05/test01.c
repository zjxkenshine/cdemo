#define _CRT_SECURE_NO_WARNINGS

// offset宏
// 计算结构体变量相对于起始位置的偏移量
// offsetof函数实现
#include<stddef.h>
#include<stdio.h>

struct S {
	char c1;
	int i;
	char c2;
};

int main1() {
	struct S s = { 0 };
	printf("%d\n", offsetof(struct S, c1));
	printf("%d\n", offsetof(struct S, i));
	printf("%d\n", offsetof(struct S, c2));

	return 0;
}