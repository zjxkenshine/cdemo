#define _CRT_SECURE_NO_WARNINGS
// offset宏实现
#include<stddef.h>
#include<stdio.h>

// 自定义offset宏
#define OFFSET(type,m_name)  (int)&(((type*)0)->m_name)

struct S1 {
	char c1;
	int i;
	char c2;
};

int main() {
	struct S1 s = { 0 };
	printf("%d\n", OFFSET(struct S1, c1));
	printf("%d\n", OFFSET(struct S1, i));
	printf("%d\n", OFFSET(struct S1, c2));
	return 0;
}
