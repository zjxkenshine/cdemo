#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

// 定义宏
#define SQUARE(X) X*X
#define SQUARE2(X) ((X)*(X))

int main3() {
	int r = SQUARE(5);
	printf("%d\n",r);

	int r2 = SQUARE(5+2);
	printf("%d\n", r2);
	// 写宏的时候尽量加括号
	int r3 = SQUARE2(5 + 2);
	printf("%d\n", r3);
	return 0;
}