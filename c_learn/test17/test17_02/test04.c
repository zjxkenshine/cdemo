#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
// #N 转换为参数对应的字符串
#define PRINT(N) printf("the value of "#N" is %d\n",N)
#define PRINT2(N,FORMAT) printf("the value of "#N" is "FORMAT"\n",N)

int main4() {
	int a = 10;
	PRINT(a);

	int b = 20;
	PRINT(b);

	int c = 30;
	PRINT2(c, "%d");

	float f = 3.14f;
	PRINT2(f, "%lf");
	return 0;
}