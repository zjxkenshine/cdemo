#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>

#define MAX(a,b) ((a)>(b)?(a):(b))
// 带副作用的宏参数
int main6() {
	int a = 5;
	int b = 4;
	int m = MAX(a++, b++);
	printf("m=%d ", m);
	printf("a=%d b=%d\n", a, b);
	return 0;
}