#define _CRT_SECURE_NO_WARNINGS

// 输入7个分数，去掉一个最高分，一个最低分，求平均分数
#include<stdio.h>

int main2() {
	int score = 0;
	int n = 0;
	int max = 0;
	int min = 100;
	int sum = 0;
	while (scanf("%d", &score) == 1) {
		n++;
		if (score > max) {
			max = score;
		}
		if (score < min) {
			min = score;
		}
		sum += score;
		if (n == 7) {
			printf("%.2lf\n", (sum - max - min) / 5);
		}
	}

	return 0;
}