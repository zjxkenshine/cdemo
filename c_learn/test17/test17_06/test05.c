#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>

// 写一个宏，将一个整数的二进制位的奇数位和偶数位交换
#define SWAP_BIT(n) n=(((n&0x55555555)<<1)+((n&0xaaaaaaaa)>>1))

int main() {
	int n = 0;
	scanf("%d", &n);
	SWAP_BIT(n);
	printf("%d\n", n);
	return 0;
}