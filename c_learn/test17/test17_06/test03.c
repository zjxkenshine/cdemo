#define _CRT_SECURE_NO_WARNINGS
// 一个数组中只有两个数字是出现一次，其他所有数字都出现了两次
// 编写一个函数找出这两个出现了一次的数
#include<stdio.h>

void find_single(int arr[], int sz, int* pd1, int* pd2) {
	int i = 0;
	int ret = 0;
	//1. 异或
	for (i = 0;i < sz;i++) {
		ret ^= arr[i];
	}
	//printf("%d\n", ret);
	// 2.判断ret哪一位为1
	int pos = 0;
	for (pos = 0;pos < 32;pos++) {
		if (((ret >> pos) & 1) == 1) {
			break;
		}
	}
	// 3.分组
	for (i = 0;i < sz;i++) {
		if (((arr[i] >> pos)& 1) == 1) {
			*pd1 ^= arr[i];
		}
		else {
			*pd2 ^= arr[i];
		}
	}
}


int main3() {
	int arr[] = { 1,2,3,4,5,1,3,2,4,6 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	int dog1 = 0;
	int dog2 = 0;
	
	// 分组
	// 1.所有数异或
	// 2.找出异或的结果数字哪一位为1，该位为n
	// 3.以第n位为0分1组，第n位为1分一组
	find_single(arr, sz, &dog1, &dog2);
	printf("%d\n %d\n", dog1, dog2);
	return 0;
}