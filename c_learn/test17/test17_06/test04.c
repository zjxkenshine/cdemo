#define _CRT_SECURE_NO_WARNINGS
#include<stdlib.h>
#include<stdio.h>
// isspace支持
#include<ctype.h>
#include<assert.h>
#include<limits.h>

enum Status {
	VALID,
	INVALID
}sta= INVALID;	// 默认非法

// 空指针，空字符串，空格，正负号，越界访问
// 自定义atoi
int my_atoi(const char* str) {
	int flag = 1;
	assert(str);
	if (*str == '\0') {
		return 0; // 空字符
	}
	// 跳过空白字符
	while (isspace(*str)) {
		str++;
	}
	// 正负号
	if (*str == '+') {
		flag = 1;
		str++;
	}
	if (*str == '-') {
		flag = -1;
		str++;
	}
	long long ret = 0;
	while(*str) {
		// 是否是数字
		if (isdigit(*str)) {
			// 越界判断
			ret = ret * 10 + flag * (*str - '0');
			if (ret > INT_MAX || ret < INT_MIN) {
				return 0;
			}
		}
		else {
			return ret;
		}
		str++;
	}
	if (*str == '\0') {
		sta = VALID;
	}
	return flag*ret;
}

// atoi 字符串转int
int main4() {
	char arr[20] = "123456";
	int ret = atoi(arr);
	printf("%d\n", ret);

	int ret1 = my_atoi(arr);
	if (sta == INVALID) {
		printf("非法返回:%d\n",ret);
	}
	else if (sta == VALID) {
		printf("合法:%d\n",ret);
	}
	return 0;
}