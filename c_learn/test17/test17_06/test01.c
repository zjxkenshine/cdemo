#define _CRT_SECURE_NO_WARNINGS
// 输出箭头型图案
#include<stdio.h>

int main1() {

	int n = 0;
	while (scanf("%d", &n) == 1) {
		// 打印图案
		// 上N行
		int i = 0;
		for (i = 0;i < n;i++) {
			//空格
			int j = 0;
			for (j = 0;j < n-i;j++) {
				printf(" ");
			}

			for (j = 0;j <=i ;j++) {
				printf("*");
			}
			printf("\n");
		}

		// 下n+1行
		for (i = 0;i < n+1;i++) {
			//空格
			int j = 0;
			for (j = 0;j <i;j++) {
				printf(" ");
			}
			for (j = 0;j <= n-i;j++) {
				printf("*");
			}
			printf("\n");
		}
	}
	

	return 0;
}