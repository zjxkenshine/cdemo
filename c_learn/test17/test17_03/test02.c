#define _CRT_SECURE_NO_WARNINGS
// 命令行定义 GCC下可以
// gcc -D ARRAY_SIZE=10 test02.c
#include <stdio.h>
// 命令行定义不用#define定义
#define ARRAY_SIZE 10
int main2()
{
	int array[ARRAY_SIZE];
	int i = 0;
	for (i = 0; i < ARRAY_SIZE; i++)
	{
		array[i] = i;
	}
	for (i = 0; i < ARRAY_SIZE; i++)
	{
		printf("%d ", array[i]);
	}
	printf("\n");
	return 0;
}