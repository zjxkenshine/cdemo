#define _CRT_SECURE_NO_WARNINGS

// 全局变量
int g_val = 2022;

// static修饰全局变量
// 这个全局变量就变成内存链接属性了，其他源文件.c不能使用这个变量
// 在这个.c文件内可以使用
static int g_val1 = 2022;

int Add(int x, int y)
{
	return x + y;
}

static int Add1(int x, int y)
{
	return x + y;
}