#define _CRT_SECURE_NO_WARNINGS

// C语言关键字
#include <stdio.h>

// 将unsigned int重命名为unit
typedef unsigned int unit;

// 重命名结构体
typedef struct Node {
	int data;
	struct Node* next;
} Node;


void test() {
	//static修饰局部变量 静态变量 不使用static输出10个2
	static int a = 1;
	a++;
	printf("%d ", a);
}

//void test1() {
//	printf("hello world\n");
//}

// 声明外部符号
// 链接外部变量，一起编译生成可执行文件
extern int g_val;
// 声明外部方法
extern int Add(int x, int y);
// static修饰的方法私有，无法使用
//extern int Add1(int x, int y);


//define定义标识符常量
#define MAX 1000
//define定义宏
//宏的参数 x y 是无类型的
#define ADD3(x, y) ((x)+(y))

int main() {

	unsigned int num=0;
	// 使用重定义类型
	unit num1 = 0;

	struct Node n1;
	// 使用重定义结构体
	Node n2;

	// static关键字修饰
	int i = 0;
	while (i < 10) {
		test();
		i++;
	}

	//test1();
	// 需要使用extern声明外部关键字
	printf("%d\n",g_val);

	//static修饰的全局变量私有，这里不能使用
	//printf("%d\n", g_val1);

	// 声明
	int a = 10;
	int b = 20;
	int z = Add(a, b);
	printf("%d\n",z);

	// static修饰的方法在源文件内私有，无法使用
	//int z1 = Add1(a, b);
	//printf("%d\n", z1);

	// 寄存器变量
	// 建议3存放在寄存器中，最终是编译器觉得的
	register int t = 3; 

	// define定义宏
	int sum = ADD3(2, 3);
	printf("sum = %d\n", sum);
	sum = 10 * ADD3(2, 3);
	printf("sum = %d\n", sum);

	return 0;
}