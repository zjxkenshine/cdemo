#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

// 数组简单学习
int main() {
	int arr[10] = { 0,1,2,3,4,5,6,7,8,9 };
	// for循环
	for (int i = 0;i < 10;i++) {
		printf("%d",arr[i]);
	}
	printf("\n");
	return 0;
}