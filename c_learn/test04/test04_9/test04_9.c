#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

int main() {
	int num = 10;//向内存申请4个字节存放10
	&num;//取出num的地址
	//注：这里num的4个字节，每个字节都有地址，取出的是第一个字节的地址（较小的地址）
	printf("%p\n", &num);//打印地址，%p是以地址的形式打印

	// 指针变量存储地址
	int* p = &num;
	// 这里的*是解引用操作符，通过p中存放的地址找到p所指向的对象
	*p = 20;	// 将num值改为20
	printf("%d\n", num);

	char ch = '2';
	// char类型指针变量
	char* pc = &ch;


	// 指针大小，取决于地址的大小（32或者64位平台）
	printf("%d\n", sizeof(char*));
	printf("%d\n", sizeof(short*));
	printf("%d\n", sizeof(int*));
	printf("%d\n", sizeof(double*));
	return 0;
}