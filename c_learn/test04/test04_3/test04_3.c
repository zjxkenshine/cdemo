#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

int main() {

	int input = 0;
	printf("要好好学习吗(1/0)?");
	scanf("%d", &input);
	// if else选择语句
	if (input == 1) {
		printf("坚持住，你会有好生活\n");
	}else {
		printf("放弃，回家卖红薯\n");
	}
	return 0;
}