#define _CRT_SECURE_NO_WARNINGS
// 函数
#include <stdio.h>

int Add(int x, int y) {
	int z = 0;
	z = x + y;
	return z;
}

int Add1(int x, int y) {
	return x+y;
}


int main() {
	int n1 = 0;
	int n2 = 0;
	// 输入
	scanf("%d %d", &n1, &n2);
	// 调用函数求和
	int sum = Add(n1, n2);
	// 打印
	printf("%d\n", sum);
	return 0;
}
