#include <stdio.h>
int main()
{
	//问题1：在屏幕上打印一个单引号'，怎么做？
	//问题2：在屏幕上打印一个字符串，字符串的内容是一个双引号“，怎么做？
	printf("%c\n", '\'');
	printf("%s\n", "\"");
	printf("%s\n", "\\test04\\test04_1");
	printf("%s\n", "abcd\\0ef");

	// 8进制130转换十进制 以该数字88的ascii码值x
	printf("%c\n", '/130');
	// 十六进制60 转换为十进制对应的ascii码
	printf("%c\n", '/x60');

	printf("%d\n", strlen("abcdef"));
	// \62被解析成一个转义字符
	printf("%d\n", strlen("c:\test\628\test.c"));
	return 0;
}