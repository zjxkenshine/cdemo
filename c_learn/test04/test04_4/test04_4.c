#define _CRT_SECURE_NO_WARNINGS
// while循环
#include <stdio.h>

int main() {
	printf("持续学习\n");
	int line = 0;
	while (line <= 20000) {
		line++;
		printf("持续努力敲代码:%d\n",line);
	}
	if (line > 20000) {
		printf("好0ffer\n");
	}
	return 0;
}