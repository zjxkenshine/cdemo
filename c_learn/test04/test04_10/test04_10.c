#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

// 学生结构体
struct Stu {
	// 结构体成员
	// 名字
	char name[20];
	// 年龄
	int age;
	// 性别
	char sex[5];
	// 学号
	char id[15];
};

// 结构体指针成员打印
void print(struct Stu* ps) {
	printf("name = %s age = %d sex = %s id = %s\n", (*ps).name, (*ps).age, (*ps).sex, (*ps).id);
	printf("name = %s age = %d sex = %s id = %s\n", ps->name, ps->age, ps->sex, ps->id);
}


int main() {
	//打印结构体信息
	struct Stu s = { "kenshine", 20, "男", "20180101" };
	//.为结构成员访问操作符 对象.成员
	printf("name = %s age = %d sex = %s id = %s\n", s.name, s.age, s.sex, s.id);
	//->操作符 结构体指针访问成员 结构体指针->成员
	struct Stu* ps = &s;
	printf("name = %s age = %d sex = %s id = %s\n", ps->name, ps->age, ps->sex, ps -> id);

	print(ps);
}