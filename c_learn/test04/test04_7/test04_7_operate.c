#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
// 操作符

int main() {
	// 两端只要有一个浮点数就执行浮点数操作
	int a1 = 7 / 2;
	// 取模
	int b1 = 7 % 2;
	printf("%d\n", a1);
	printf("%d\n", b1);
	// 浮点数
	float a2 = 7 / 2.0;
	int b2 = 7 % 2;
	printf("%f\n", a2);
	// 保留一位小数
	printf("%.1f\n", a2);
	printf("%d\n", b2);

	// c语言中 0为假，非0为真
	int flag = 0;
	if (!flag) {
		printf("!flag为真\n");
	}

	// sizeof是单目操作符
	int a3 = 10;
	printf("%d\n", sizeof(a3));
	// sizeof计算的是整个数组长度，40字节
	int arr[10] = { 0 };
	printf("%d\n", sizeof(arr));// 40字节
	printf("%d\n", sizeof(arr[0]));// 第一个字节大小 4
	printf("%d\n", sizeof(arr)/sizeof(arr[0]));//数组元素个数 10

	// a++ 先用后加
	int a4 = 10;
	int b4 = a4++;
	printf("%d\n", b4);
	printf("%d\n", a4);

	// ++a 先加再用
	int a5 = 10;
	int b5 = ++a5;
	printf("%d\n", b5);
	printf("%d\n", a5);

	// 强制类型转换
	int pi = (int)3.1415;
	printf("%d\n", pi);

	// && 且
	// || 或
	int a6 = 0;
	int b6 = 20;
	if (a6 && b6) {
		printf("&&操作符\n");
	}
	if (a6 || b6) {
		printf("||操作符\n");
	}

	// 条件操作符 条件?真:假
	int a7 = 10;
	int b7 = 20;
	int r = a7 > b7 ? a7 : b7;
	printf("%d\n", r);

	// 逗号表达式，隔开的一串表达式，从左到右依次计算，返回最后一个结果
	int a = 10;
	int b = 20;
	int c = 0;
	// c=8，a=28，结果=5
	int d = (c = a - 2, a = b + c, c - 3);
	printf("%d\n", d); //5
	printf("%d\n", a); //28
	printf("%d\n", c); //8

	return 0;
}	
