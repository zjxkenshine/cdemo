#include <stdio.h>
#include <string.h>

int main() {
	// c语言中没有字符串类型，使用字符数组表示
	// f10 调试-》窗口-》监视 输入arr进行监视
	// 占7个字符
	char arr1[] = "abcdef";
	char arr2[] = { 'a','b','c','d','e','f'};
	char arr3[] = { 'a','b','c','d','e','f','\0'};
	printf("%s\n", arr1);
	// 打印时直到遇到\0才会停止
	printf("%s\n", arr2);
	printf("%s\n", arr3);

	// 头文件string.h
	// 字符串长度 strlen
	int len = strlen("abc");
	// 3 \0只表示结束标志，不影响字符串长度
	printf("%d\n", len);
	// 遇到\0之前有38个字符
	printf("%d\n", strlen(arr2));
	return 0;
}