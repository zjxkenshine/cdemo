#define _CRT_SECURE_NO_WARNINGS
// 指针与数组
#include <stdio.h>

int main1() {
	int arr[10] = { 1,2,3,4,5,6,7,8,9,0 };
	//数组名表示的是数组首元素的地址
	printf("%p\n", arr);
	printf("%p\n", &arr[0]);
	return 0;
}