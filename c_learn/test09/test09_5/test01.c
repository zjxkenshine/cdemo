#define _CRT_SECURE_NO_WARNINGS
// 指针数组
#include<stdio.h>

int main1() {

	int a = 10;
	int b = 20;
	int c = 30;

	// 存放指针的数组
	int* parr[10] = { &a,&b,&c };

	int i = 0;
	for (i = 0;i < 3;i++) {
		printf("%d\n", *parr[i]);
		*parr[i] = 0;
	}
	return 0;
}

