#define _CRT_SECURE_NO_WARNINGS

// 1.指针变量没有初始化 导致野指针
#include <stdio.h>

int main1() {
	int* p;
	// p没有初始化，意味着没有明确指向
	// 放的是随机地址
	// 非法访问了，这里的p就是野指针
	*p = 10;
	return 0;
}