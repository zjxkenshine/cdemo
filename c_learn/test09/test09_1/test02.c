#define _CRT_SECURE_NO_WARNINGS

// 2.指针越界访问导致野指针
#include <stdio.h>

int main2() {
	int arr[10] = { 0 };
	int* p = arr;
	int i = 0;
	// i=10时 数组越界访问，变为野指针
	for (i = 0;i <= 10;i++) {
		*p = i;
		p++;
	}
	return 0;
}