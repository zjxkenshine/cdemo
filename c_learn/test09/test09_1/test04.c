#define _CRT_SECURE_NO_WARNINGS
// 避免野指针
#include <stdio.h>

int main() {
	// null为0地址
	int* p = NULL;
	int a = 10;
	p = &a;
	// 有效性判断，0地址不做更改
	if (p != NULL){
		*p = 20;
	}
	printf("%d\n", *p);
	return 0;
}