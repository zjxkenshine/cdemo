#define _CRT_SECURE_NO_WARNINGS

//3.指针指向的对象被销毁，变为野指针
#include <stdio.h>

int* test() {
	// a在方法执行结束就销毁了
	int a = 10;
	return &a;
}

int main3() {
	int* p = test();
	return 0;
}