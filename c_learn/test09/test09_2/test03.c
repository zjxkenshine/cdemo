#define _CRT_SECURE_NO_WARNINGS
// 指针-指针 得到两个指针之间元素的个数
#include <stdio.h>

int main() {
	char s[20] ="kenshine";
	char* p =s;
	while (*p != '\0') {
		p++;
	}
	printf("%i\n",p - s);
	return 0;
}