#define _CRT_SECURE_NO_WARNINGS
// 指针+-整数
// 指针关系运算
#include <stdio.h>

int main1() {
	float values[5];
	float* vp;
	// 初始化value数组
	// vp++，每次往后走一个float长度(8)
	for (vp = &values[0];vp < &values[5];vp++) {
		*vp = 0;
	}

	return 0;
}