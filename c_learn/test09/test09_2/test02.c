#define _CRT_SECURE_NO_WARNINGS

// 指针初始化第二种写法
#include <stdio.h>

int main2() {
	int arr[10] = { 0 };
	int i = 0;
	int sz = sizeof(arr) / sizeof(arr[0]);
	
	int* p = arr;
	for (i = 0;i < sz;i++) {
		*p = 1;
		p++;
	}
	// 通过调试窗口查看数组内存数据变化
	return 0;
}