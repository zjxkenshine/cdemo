#define _CRT_SECURE_NO_WARNINGS

// 指针类型的意义
// 希望以什么样的方式访问内存(1个字节1个字节还是1个)
#include <stdio.h>

int main() {
	int a = 0x11223344;
	int* pa = &a;
	char* pc = (char*)&a;

	// 指针大小
	printf("%zu\n", sizeof(pa));
	printf("%zu\n", sizeof(pc));

	printf("pa=%p\n", pa);
	printf("pa+1=%p\n", pa+1); //跳过4个字节
	printf("pc=%p\n", pc);
	printf("pc+1=%p\n", pc+1); //跳过1个字节

	return 0;
}
