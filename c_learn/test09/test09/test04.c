﻿#define _CRT_SECURE_NO_WARNINGS

// 指针解引用
// 指针的类型决定了，对指针解引用的时候有多大的权限（能操作几个字节）
#include <stdio.h>

int main() {
	int n = 0x11223344;
	char* pc = (char*)&n;
	int* pi = &n;
	// pc解引用仅能操作1字节，n能操作4字节
	*pc = 0;
	// pi解引用能操作4字节
	*pi = 0;
	return 0;
}