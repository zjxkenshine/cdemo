#define _CRT_SECURE_NO_WARNINGS

// 指针变量的大小、类型
#include <stdio.h>

int main2() {
	char* pc = NULL;
	short* pc1 = NULL;
	int* pc2 = NULL;
	double* pc3 = NULL;
	// %zu 专为sizeof打印格式
	printf("%zu\n", sizeof(char*));
	printf("%zu\n", sizeof(pc));
	printf("%zu\n", sizeof(pc1));
	printf("%zu\n", sizeof(pc2));
	printf("%zu\n", sizeof(pc3));
	return 0;
}