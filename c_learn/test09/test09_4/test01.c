#define _CRT_SECURE_NO_WARNINGS
//二级指针
#include <stdio.h>

int main() {
	int a = 10;
	// 一级指针，指向a的地址
	int *pa = &a;
	// 二级指针 地址
	int** ppa = &pa;

	printf("%p\n", pa);
	printf("%p\n", ppa);

	// 修改a中的数据，需要解引用两次
	**ppa = 20;
	printf("%d\n", a);
	return 0;
}