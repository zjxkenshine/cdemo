#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

// 位段
struct A{
	// 开辟4个字节 32位
	int _a : 2; // 表示占用2比特位
	int _b : 5;	// 占用5bit位
	int _c : 10;
	// 剩下15bit 不会使用
	// 再开辟4个字节 使用30位
	int _d : 30;
};

int main1() {
	// 结果为8
	printf("%d\n", sizeof(struct A));
	return 0;
}