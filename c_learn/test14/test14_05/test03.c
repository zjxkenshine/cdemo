#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
// 判断大小端存储 联合体方法
int check_sys1() {
	union Un {
		char c;
		int i;
	}u;
	u.i = 1;
	return u.c;
}

int main3() {
	int ret = check_sys1();
	if (ret == 1) {
		printf("小端\n");
	}
	else {
		printf("大端\n");
	}
	return 0;
}