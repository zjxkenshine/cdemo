#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
// 联合体大小
union Un1 {
	char arr[5];//5 相当于5个char 对齐数为1
	int i;	//4 对齐数为4
};

int main() {
	printf("%d\n", sizeof(union Un1));
	return 0;
}