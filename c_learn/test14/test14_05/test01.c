#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

// 联合体
union Un {
	int a;
	char b;
	char c;
};

struct St {
	int a;
	char c;
};

// 联合体共用内存
int main1() {
	union Un u;
	// 结果为4
	printf("%d\n", sizeof(u));

	// 3个成员地址一致
	printf("%p\n",&u);
	printf("%p\n",&(u.a));
	printf("%p\n",&(u.c));
	return 0;
}