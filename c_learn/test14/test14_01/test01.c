#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
// 结构体传参
struct S
{
	int data[1000];
	int num;
};

void print1(struct S ss) {
	int i = 0;
	for (i = 0;i < 3;i++) {
		printf("%d", ss.data[i]);
	}
	printf("%d\n", ss.num);
}

// 效率比较高
void print2(struct S* ps) {
	int i = 0;
	for (i = 0;i < 3;i++) {
		printf("%d", ps->data[i]);
	}
	printf("%d\n", ps->num);
}

int main() {
	struct S s = { {1,2,3},100 };
	// 传值调用
	print1(s);
	// 传址调用
	print2(&s);
}
