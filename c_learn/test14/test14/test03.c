#define _CRT_SECURE_NO_WARNINGS

// 结构体自引用
struct Node {
	int data;
	// 包含同类型地址
	struct Node* next;
};

// 自引用不支持匿名
typedef struct Node1{
	int data;
	struct Node1* next;
}Node;

int main3() {
	return 0;
}