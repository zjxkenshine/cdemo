#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
#include<stddef.h>
/*
结构体内存对齐
*/
struct S1
{
	char c1; //1字节
	int i; //4字节
	char c2; //1字节
};

struct S2
{
	char c1; //1字节
	char c2; //1字节
	int i; //4字节
};

struct S3
{
	double d; //8字节
	char c; //1字节
	int i; //4字节
};

// 结构体嵌套内存对齐
// 最大对齐数为8
struct S4
{
	double d; //8字节
	struct S3 s3; // 16字节 最大对齐数8
	int i; //4字节
};

int main5() {
	// 结果为12
	printf("%d\n", sizeof(struct S1));
	// 结果为8
	printf("%d\n", sizeof(struct S2));
	// 结果为16
	printf("%d\n", sizeof(struct S3));
	// 结果为32
	printf("%d\n", sizeof(struct S4));

	// offsetof 偏移量
	printf("%d\n", offsetof(struct S2,c1));
	printf("%d\n", offsetof(struct S2,i));
	printf("%d\n", offsetof(struct S2,c2));
	return 0;
}