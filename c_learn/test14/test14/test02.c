#define _CRT_SECURE_NO_WARNINGS

// 匿名结构体
// 只能使用一次
struct{
	// 学生相关属性
	char name[20];
	int age;
}s4; // 全局变量


struct {
	char name[20];
	int age;
}s5,* s; 

int main2() {
	// 错误，成员变量一样，但是是不同类型
	s = &s4;
;	return 0;
}