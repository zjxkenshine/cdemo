#define _CRT_SECURE_NO_WARNINGS
// 修改默认对齐数
#include <stdio.h>
#pragma pack(8)//设置默认对齐数为8
struct S1
{
	char c1;
	int i;
	char c2;
};
#pragma pack()//取消设置的默认对齐数，还原为默认

// 不进行对齐
#pragma pack(1)//设置默认对齐数为1
struct S2
{
	char c1; //1
	int i; //4
	char c2; //1
};
#pragma pack()//取消设置的默认对齐数，还原为默认

int main()
{
	printf("%d\n", sizeof(struct S1));
	printf("%d\n", sizeof(struct S2));
	return 0;
}
