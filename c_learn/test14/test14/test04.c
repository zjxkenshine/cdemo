#define _CRT_SECURE_NO_WARNINGS
// 结构体类型定义与初始化
struct Point
{
	int x;
	int y;
}p1; //声明类型的同时定义变量p1
struct Point p2; //定义结构体变量p2


//类型声明
struct Stu2{
	char name[15];//名字
	int age;//年龄
};

int main4() {
	int x = 0;
	int y = 0;
	//初始化：定义变量的同时赋初值。
	struct Point p3 = { x, y };
	struct Stu2 s = { "zhangsan", 20 };//初始化
	return 0;
}