#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

// 修改默认对齐数为4
#pragma pack(4)

struct S {
	int i;
	double d;
};

int main6() {
	// 默认对齐数8的情况下，结果为16
	printf("%d\n", sizeof(struct S));
	return 0;
}