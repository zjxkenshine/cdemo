#define _CRT_SECURE_NO_WARNINGS

// 字符串地址
#include <stdio.h>
 
int main1() {
	// 字符地址
	char ch = 'w';
	char* pc = &ch;
	// 修改字符值
	*pc = 'a';
	printf("%c\n", ch);

	// 首字符a的地址放到了p中
	// 常量字符串是不可修改的，使用const
	const char* p = "abcdef";
	// 这种写法把整个字符串放到arr地址中
	char arr[] = "abcdef";
	printf("%s\n", p);
	return 0;
}