#define _CRT_SECURE_NO_WARNINGS
// 地址比较
#include <stdio.h>

int main() {
	// 这两个不相同
	char str1[] = "hello bit.";
	char str2[] = "hello bit.";
	// 这两个相等，指向同一个字符串，只读数据区
	const char* str3 = "hello bit.";
	const char* str4 = "hello bit.";
	if (str1 == str2)
		printf("str1 and str2 are same\n");
	else
		printf("str1 and str2 are not same\n");

	if (str3 == str4)
		printf("str3 and str4 are same\n");
	else
		printf("str3 and str4 are not same\n");

	return 0;
}
