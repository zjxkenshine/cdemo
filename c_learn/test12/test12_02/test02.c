#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

int main2() {
	// 数组
	int arr[10] = { 0 };
	// 数组首地址指针
	int* p = arr;
	// 数组指针
	int(*p1)[10] = &arr;

	printf("%p\n", p);
	printf("%p\n",p1);
	return 0;
}