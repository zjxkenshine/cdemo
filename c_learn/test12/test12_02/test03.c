#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
// 数组指针解引用的意义
int main3() {
	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
	int(*p)[10] = &arr;

	int i = 0;
	int sz = sizeof(arr) / sizeof(arr[0]);
	for (i = 0;i < sz;i++) {
		// *p是指向数组的，相当于数组名
		// **p访问数组元素
		printf("%d ", *(*p+i));
	}
	return 0;
}