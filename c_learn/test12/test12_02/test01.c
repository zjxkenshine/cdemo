#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

// 数组名通常表示数组首元素地址，两个例外
int main1() {
	int arr[10] = { 0 };
	printf("%p\n", arr);
	printf("%p\n", arr+1); // 加了4个字节

	printf("%p\n", &arr[0]);
	printf("%p\n", &arr[0]+1); // 加了4个字节

	// 1.表示整个数组地址
	printf("%p\n", &arr);
	printf("%p\n", &arr+1); // 加了40个字节，数组长度

	// 2.表示整个数组
	int sz = sizeof(arr);
	printf("%d\n", sz);
	return 0;
}