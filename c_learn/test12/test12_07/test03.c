#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

int main3() {
	int a[4] = { 1, 2, 3, 4 };
	// 指向数组后1位
	int* ptr1 = (int*)(&a + 1);
	// a=0x0012ff40
	// a+1=0x0012ff44 // 第二个元素
	// (int)a + 1 0x0012ff41 错误，指向第一个个字节开始的内容
	int* ptr2 = (int*)((int)a + 1);
	// 
	printf("%x,%x", ptr1[-1], *ptr2);
	return 0;
}