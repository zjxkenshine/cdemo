#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main5(){
	int a[5][5];
	// 一次跳过4个字节的数组指针
	int(*p)[4];
	p = a;
	// p[4][2] 相当于*(*(p+4)+2)
	// 结果为-4  %p打印：补码转换为十六进制
	printf("%p,%d\n", &p[4][2] - &a[4][2], &p[4][2] - &a[4][2]);
	return 0;
}