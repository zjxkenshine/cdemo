#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

int main7() {
	// 指针数组 w地址，a地址，a地址的数组
	char* a[] = { "work","at","alibaba" };
	char** pa = a;
	// 往后挪到a地址
	pa++;
	// 打印at，只需要知道首地址就可以打印，一直到\0
	printf("%s\n", *pa);

	return 0;
}