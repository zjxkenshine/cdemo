#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
int main4(){
	// 大坑，这里是小括号，是逗号表达式，不是初始化
	// 初始化为，1,3,5
	int a[3][2] = { (0, 1), (2, 3), (4, 5) };
	int* p;
	// a[0] 表示第一行地址
	p = a[0];
	// P[0] 为第一行第一个元素
	printf("%d", p[0]);
	return 0;
}