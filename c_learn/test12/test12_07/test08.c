#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
// 习题8  详见笔记图片
int main(){
	char* c[] = { "ENTER","NEW","POINT","FIRST" };
	char** cp[] = { c + 3,c + 2,c + 1,c };
	char*** cpp = cp;
	// POINT
	printf("%s\n", **++cpp);
	// ER
	printf("%s\n", *-- * ++cpp + 3);
	// 相当于*(*(cpp-2))+3  ST
	printf("%s\n", *cpp[-2] + 3);
	// 相当于*(*(cpp-1)-1)+1  EW
	printf("%s\n", cpp[-1][-1] + 1);
	return 0;
}