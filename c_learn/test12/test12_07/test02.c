#define _CRT_SECURE_NO_WARNINGS

// x86环境下结构体20字节
struct Test{
	int Num;
	char* pcName;
	short sDate;
	char cha[2];
	short sBa[4];
}*p=(struct Test*)0x100000;
//假设p 的值为0x100000。 如下表表达式的值分别为多少？
//已知，结构体Test类型的变量大小是20个字节
int main2(){
	printf("%p\n", p + 0x1);  // 0x100014 （16进制）
	printf("%p\n", (unsigned long)p + 0x1);	// +1
	printf("%p\n", (unsigned int*)p + 0x1); // 地址 4字节
	return 0;
}