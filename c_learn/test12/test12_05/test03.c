#define _CRT_SECURE_NO_WARNINGS

#include <stdlib.h>
#include <search.h>
#include <string.h>
// q_sort结构体排序

struct Stu {
	char name[20];
	int age;
};

// 比较结构体
int cmp_stu_by_name(const void* p1, const void* p2) {
	return strcmp(((struct Stu*)p1)->name, ((struct Stu*)p1)->name);
}


int main3() {
	struct Stu s[] = { {"kenshine",22},{"hong",30},{"qin",28} };
	int sz = sizeof(s)/sizeof(s[0]);
	// 按名字排序
	qsort(s, sz, sizeof(s[0]),cmp_stu_by_name);
	return 0;
}