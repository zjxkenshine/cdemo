#define _CRT_SECURE_NO_WARNINGS

// 交换
void _swap(void* p1, void* p2, int width){
	int i = 0;
	for (i = 0; i < width; i++){
		char tmp = *((char*)p1 + i);
		*((char*)p1 + i) = *((char*)p2 + i);
		*((char*)p2 + i) = tmp;
	}
}

// 这种写法效果相同
void _swap1(void* p1, void* p2, int width) {
	int i = 0;
	for (i = 0; i < width; i++) {
		char tmp = *(char*)p1;
		*(char*)p1 = *(char*)p2;
		*(char*)p2 = tmp;
		((char*)p1)++;
		((char*)p2)++;
	}
}

// 模拟q_sort实现
// sz 数组长度
// width：元素长度
void my_b_sort(void* base, int sz, int width, int(*cmp)(const void*e1,const void*e2)) {
	int i = 0;
	// 趟数
	for (i = 0;i < sz - 1;i++) {
		// 数组排好序
		int flag = 1;
		//一趟冒泡排序
		int j = 0;
		for (j = 0;j < sz - i - 1;j++) {
			// 比较此元素与后一个元素 使用cmp回调函数
			if (cmp((char*)base + j * width, (char*)base + (j + 1) * width) > 0) {
				// 交换
				_swap((char*)base + j * width, (char*)base + (j + 1) * width, width);
				flag = 0;
			}
		}
		// 已经有序了
		if (flag == 1) {
			break;
		}
	}

}
// 使用自定义qsort
int int_cmp1(const void* p1, const void* p2) {
	return (*(int*)p1 - *(int*)p2);
}

int main() {
	int arr[] = { 1, 3, 5, 7, 9, 2, 4, 6, 8, 0 };
	my_b_sort(arr, sizeof(arr) / sizeof(arr[0]), sizeof(int), int_cmp1);
	int i = 0;
	for (i = 0; i < sizeof(arr) / sizeof(arr[0]); i++){
		printf("%d ", arr[i]);
	}
	printf("\n");
	return 0;
}