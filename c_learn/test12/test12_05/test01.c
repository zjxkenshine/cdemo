#define _CRT_SECURE_NO_WARNINGS
// 回调函数 自己写的冒泡排序
#include <stdio.h>

void bubble_sort(int arr[], int sz) {
	int i = 0;
	// 趟数
	for (i = 0;i < sz - 1;i++) {
		// 数组排好序
		int flag = 1;
		//一趟冒泡排序
		int j = 0;
		for (j = 0;j < sz - i -1;j++) {
			if (arr[j] > arr[j+1]) {
				int tmp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = tmp;
				flag = 0;
			}
		}
		// 已经有序了
		if (flag ==1) {
			break;
		}
	}
}

int main1() {
	int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
	// 把数组排成升序
	int sz = sizeof(arr) / sizeof(arr[0]);
	bubble_sort(arr, sz);
	int i = 0;
	for (i = 0;i < sz;i++) {
		printf("%d ",arr[i]);
	}
	return 0;
}