#define _CRT_SECURE_NO_WARNINGS
// 官方库 q_sort()快速排序
#include <stdlib.h>
#include <search.h>

//qosrt函数的使用者得实现一个比较函数
int int_cmp(const void* p1, const void* p2){
	// void*是无具体类型指针，可以接收任意地址 不能解引用，不能进行计算
	// 需要强制类型转换
	return (*(int*)p1 - *(int*)p2);
}
// qsort排序整型

int main2(){
	int arr[] = { 1, 3, 5, 7, 9, 2, 4, 6, 8, 0 };
	int i = 0;
	// 4个参数
	qsort(arr, sizeof(arr) / sizeof(arr[0]), sizeof(int), int_cmp);
	for (i = 0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
	return 0;
}