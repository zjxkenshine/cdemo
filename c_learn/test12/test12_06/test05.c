#define _CRT_SECURE_NO_WARNINGS
// 二维数组
#include<stdio.h>

int main() {
	//二维数组
	int a[3][4] = { 0 };

	printf("%d\n", sizeof(a));	// 整个二维数组大小 48
	printf("%d\n", sizeof(a[0][0]));	// 4
	printf("%d\n", sizeof(a[0]));	// 第1行大小 16 相当于是第一行数组名
	printf("%d\n", sizeof(a[0] + 1));	// 第1行第2个元素地址 8
	// a[0] + 1 是第一行第二个元素的地址，解引用就是第二个元素 int长度为4
	printf("%d\n", sizeof(*(a[0] + 1)));	// 4 
	printf("%d\n", sizeof(a + 1));	// a表示首元素地址，第一行+1，第2行地址 地址长度为8
	printf("%d\n", sizeof(*(a + 1)));	// 第二行解引用 16
	printf("%d\n", sizeof(&a[0] + 1));	// &a[0] 第一行地址，+1第二行地址，地址为8
	printf("%d\n", sizeof(*(&a[0] + 1)));	// 第二行解引用 16
	printf("%d\n", sizeof(*a));	// 首元素，第一行大小，16
	printf("%d\n", sizeof(a[3]));	// *不会越界，也是一行大小 16 

	return 0;
}