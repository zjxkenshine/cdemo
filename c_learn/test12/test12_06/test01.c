#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

// 一维数组解析
int main1() {
	//一维数组
	int a[] = { 1,2,3,4 };
	printf("%d\n", sizeof(a));	// 16
	printf("%d\n", sizeof(a + 0));	// a+0是首元素地址，地址X64为8字节
	printf("%d\n", sizeof(*a)); // *a中的a为首元素地址，*a就是int类型长度 4
	printf("%d\n", sizeof(a + 1)); //a表示首元素地址，a+1为第二个元素地址，地址为8字节
	printf("%d\n", sizeof(a[1]));	// int 4
	printf("%d\n", sizeof(&a));	// 地址 8	
	printf("%d\n", sizeof(*&a)); // 数组 16 
	printf("%d\n", sizeof(&a + 1));	// 数组地址 8
	printf("%d\n", sizeof(&a[0])); // 指针 8
	printf("%d\n", sizeof(&a[0] + 1));	// 指针 8
	return 0;
}