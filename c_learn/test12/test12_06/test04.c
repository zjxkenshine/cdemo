#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <string.h>
// 字符串常量地址

int main4() {
	// 存放字符串常量的地址
	char* p = "abcdef";

	printf("%d\n", sizeof(p));	// 地址 8
	printf("%d\n", sizeof(p + 1));	// 地址 8
	printf("%d\n", sizeof(*p));	//访问1个字节 char* 1
	printf("%d\n", sizeof(p[0]));	//=*p 1
	printf("%d\n", sizeof(&p));		// 地址 8
	printf("%d\n", sizeof(&p + 1));	// 地址 8
	printf("%d\n", sizeof(&p[0] + 1));	// 地址 8


	printf("%d\n", strlen(p));	// 6
	printf("%d\n", strlen(p + 1)); // 5
	//printf("%d\n", strlen(*p)); // 错误
	//printf("%d\n", strlen(p[0]));	// 错误
	printf("%d\n", strlen(&p));	// 野指针，随机
	printf("%d\n", strlen(&p + 1));	// 随机 与上一个随机地址无关，可能会跳过\0
	printf("%d\n", strlen(&p[0] + 1));	// 随机 &p的地址-1

	return 0;
}