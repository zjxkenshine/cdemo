#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <string.h>

// 字符串
int main3() {
	// 这种方式初始化带\0
	char arr[] = "abcdef";
	printf("%d\n", sizeof(arr));	//长度 7
	printf("%d\n", sizeof(arr + 0)); //地址 8
	printf("%d\n", sizeof(*arr));	// 元素 1
	printf("%d\n", sizeof(arr[1]));	// 元素 1
	printf("%d\n", sizeof(&arr));	// 地址 8
	printf("%d\n", sizeof(&arr + 1));	// 地址 8
	printf("%d\n", sizeof(&arr[0] + 1));	// 地址 8

		
	printf("%d\n", strlen(arr));	// 6
	printf("%d\n", strlen(arr + 0));	//6
	//printf("%d\n", strlen(*arr));	// 错误
	//printf("%d\n", strlen(arr[1]));	//错误
	printf("%d\n", strlen(&arr));	// 6
	printf("%d\n", strlen(&arr + 1));	// 随机值，下一个\0为止
	printf("%d\n", strlen(&arr[0] + 1));	//5
	return 0;
}

