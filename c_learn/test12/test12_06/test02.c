#define _CRT_SECURE_NO_WARNINGS

// 字符数组解析
#include<stdio.h>
#include<string.h>

int main2() {
	//字符数组
	char arr[] = { 'a','b','c','d','e','f' };
	printf("%d\n", sizeof(arr)); // sizeof(数组名) 6
	printf("%d\n", sizeof(arr + 0));	// 地址 8
	printf("%d\n", sizeof(*arr));	// 解引用 1
	printf("%d\n", sizeof(arr[1]));	// 第2个元素 1
	printf("%d\n", sizeof(&arr));	// 数组地址 8 是地址就是8
	printf("%d\n", sizeof(&arr + 1));	// 还是地址 8
	printf("%d\n", sizeof(&arr[0] + 1)); // 还是地址 8

	printf("%d\n", strlen(arr));		// 随机值，直到\0位置
	printf("%d\n", strlen(arr + 0));	// 从arr+0向后找\0,还是随机的
	//printf("%d\n", strlen(*arr));	//有问题，野指针 
	//printf("%d\n", strlen(arr[1]));	//有问题，野指针 
	printf("%d\n", strlen(&arr));  // 数组地址开始往后找 随机值
	printf("%d\n", strlen(&arr + 1));	// 数组尾开始找，随机值-6
	printf("%d\n", strlen(&arr[0] + 1));	// arr[1] 开始找，随机值-1
	return 0;
}