#define _CRT_SECURE_NO_WARNINGS
// 二级指针传参
#include <stdio.h>
void test4(int** ptr){
	printf("num = %d\n", **ptr);
}

int main4(){
	int n = 10;
	int* p = &n;
	int** pp = &p;
	test4(pp);
	test4(&p);
	return 0;
}