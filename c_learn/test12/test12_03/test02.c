#define _CRT_SECURE_NO_WARNINGS
// 二维数组传参
#include <stdio.h>
//void test(int arr[3][5])//对
//{}
//void test(int arr[][])//错
//{}
//void test(int arr[][5])//对
//{}
//总结：二维数组传参，函数形参的设计只能省略第一个[]的数字。
//因为对一个二维数组，可以不知道有多少行，但是必须知道一行多少元素。
//这样才方便运算。
//void test(int* arr)//错 一维数组地址
//{}
//void test(int* arr[5])//错
//{}
//void test(int(*arr)[5])//对	指针指向5个元素，每个元素是int
//{}
//void test(int** arr){}// 错 arr是一级指针

int main2()
{
	int arr[3][5] = { 0 };
	test(arr);
	return 0;
}