#define _CRT_SECURE_NO_WARNINGS
// 函数指针
#include <stdio.h>

int Add(int x, int y) {
	return x + y;
}

int main() {
	int arr[5] = { 0 };
	// 数组指针
	int(*p)[5] = &arr;

	// 函数名与&函数名拿到的都是函数地址
	printf("%p\n", &Add);
	printf("%p\n", Add);
	// 函数指针存储
	int (*pf)(int, int) = &Add;
	int (*pf1)(int, int) = Add;

	// 函数指针解引用
	int ret = (*pf)(2, 3);
	printf("%d\n", ret);
	// *号不写也可以
	int ret1 = pf(2, 3);
	printf("%d\n", ret1);
	return 0;
}