#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include <errno.h>

// 问题2 对动态开辟空间的越界访问
int main2() {
	int* p = (int*)malloc(40);
	if (p == NULL) {
		return 1;
	}
	int i = 0;
	// 越界访问
	for (i = 0;i <= 10;i++) {
		p[i] = i;
	}
	free(p);
	p = NULL;
	return 0;
}