#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include <errno.h>

// 4.使用free释放一块动态开辟内存的一部分
int main() {
	int* p = (int*)malloc(40);
	if (p == NULL) {
		return 1;
	}
	// 使用
	int i = 0;
	for (i = 0;i < 10;i++) {
		*p = i;
		p++;
	}
	// 释放内存的一部分
	free(p);
	p = NULL;
	return 0;
}