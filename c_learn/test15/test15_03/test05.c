#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include <errno.h>

//问题5 对同一块动态内存多次释放
int main5() {
	int* p = (int*)malloc(40);
	free(p);
	p = NULL;// 不加这句就会报错
	free(p);
	return 0;
}
