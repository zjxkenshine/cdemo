#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>

// 柔性数组的使用
struct S1 {
	int n;
	int arr[]; // 柔性数组成员
};

int main() {
	// 柔性数组的使用
	struct S1* ps=(struct S1*)malloc(sizeof(struct S1) + 40);
	if (ps == NULL) {
		return 0;
	}
	ps->n = 100;
	int i = 0;
	for (i = 0;i < 10;i++) {
		ps->arr[i] = i;
	}
	for (i = 0;i < 10;i++) {
		printf("%d ", ps->arr[i]);
	}

	// 扩容,柔性数组变化
	struct S1* ptr = (struct S1*)realloc(ps, sizeof(struct S1) + 80);
	if (ptr != NULL) {
		ps = ptr;
		ptr = NULL;
	}
	// 释放
	free(ps);
	ps = NULL;
	return 0;
}