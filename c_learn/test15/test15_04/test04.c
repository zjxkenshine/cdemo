#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include <errno.h>

// 野指针问题
void Test2() {
	char* str = (char*)malloc(100);
	strcpy(str, "hello");
	free(str);
	// str=NULL; // 正确写法，置空
	// 非法访问，空间已经释放了
	if (str != NULL)
	{
		strcpy(str, "world");
		printf(str);
	}
}

int main4() {
	Test2();
	return 0;
}