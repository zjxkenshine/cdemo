#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

// malloc函数与free函数
int main1() {
	int arr[10] = { 0 };
	// 申请40个字节
	int* p = (int*)malloc(40);
	// 开辟失败，需要判断空指针
	if (p == NULL) {
		// 打印错误信息
		printf("%s\n", strerror(errno));
		return 1;
	}
	// 空间开辟成功，使用空间
	int i = 0;
	for (i = 0;i < 10;i++) {
		*(p + i) = i;
	}
	for (i = 0;i < 10;i++) {
		printf("%d ", *(p + i));
	}

	// 没有free也不会泄露
	// 程序退出时，系统会自动回收内存空间
	// 释放内存
	free(p);
	p = NULL;	// 防止野指针
	return 0;
}