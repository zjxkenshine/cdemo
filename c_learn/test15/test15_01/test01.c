#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
// free不能释放静态开辟的空间
int main1() {
	int a = 10;
	int* p = &a;
	// free必须释放动态开辟出来的空间 错误
	free(p);
	p = NULL;
	return 0;
}