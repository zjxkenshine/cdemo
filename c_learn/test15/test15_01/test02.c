#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

int main2() {
	// calloc 会在申请空间时自动初始化为0
	int*p = (int*)calloc(10, sizeof(int));
	if (p == NULL) {
		printf("%s\n", strerror(errno));
		return 1;
	}
	// 打印
	int i = 0;
	for (i = 0;i < 10;i++) {
		printf("%d ", *(p + i));
	}
	// 释放
	free(p);
	p = NULL;
	return 0;
}