#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

// realloc 调整动态内存空间大小
int main() {
	int arr[10] = { 0 };
	// 申请40个字节
	int* p = (int*)malloc(40);
	// 开辟失败，需要判断空指针
	if (p == NULL) {
		// 打印错误信息
		printf("%s\n", strerror(errno));
		return 1;
	}
	// 空间开辟成功，使用空间
	int i = 0;
	for (i = 0;i < 10;i++) {
		*(p + i) = i;
	}

	//扩容到80字节
	int* ptr=realloc(p, 80);
	// 直接用p接收，扩容失败会导致原来的地址都找不到了
	if (p != NULL) {
		p = ptr;
	}
	// 使用
	for (i = 0;i < 10;i++) {
		printf("%d ", *(p + i));
	}
	// 释放
	free(p);
	return 0;
}