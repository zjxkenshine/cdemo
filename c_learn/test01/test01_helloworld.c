// std 标准 stdio标准io库
#include <stdio.h>

int main()
{
	// printf是一个库函数，专门用来打印数据
	printf("hello world");
	// 正常返回0，异常返回非0
	return 0;
}