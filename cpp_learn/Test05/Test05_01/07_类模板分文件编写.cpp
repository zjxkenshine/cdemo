#define _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS

// 会报错
// #include "person.h"
// 解决方法1，引入person.cpp不会报错
//#include "person.cpp"

// 常用解决方式
//解决方式2，将声明和实现写到一起，文件后缀名改为.hpp
#include "person.hpp"


void test01() {
	Person<string, int> p("Tom", 20);
	p.showPerson();
}

int main() {
	test01();
	system("pause");
	return 0;
}
