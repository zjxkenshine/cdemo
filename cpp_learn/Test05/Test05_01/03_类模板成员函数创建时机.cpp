#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
using namespace std;

class Person1{
public:
	void showPerson1(){
		cout << "Person1 show" << endl;
	}
};

class Person2{
public:
	void showPerson2(){
		cout << "Person2 show" << endl;
	}
};

// 类模板
template<class T>
class MyClass{
public:
	T obj;

	//类模板中的成员函数，并不是一开始就创建的，而是在模板调用时再生成
	void fun1() { obj.showPerson1(); }
	void fun2() { obj.showPerson2(); }
};

void test01(){
	MyClass<Person1> m;
	m.fun1();
	// 调用了fun1之后，obj创建为person1,调用func2会出错
	//m.fun2();//编译会出错，说明函数调用才会去创建成员函数
}

int main() {
	test01();
	system("pause");
	return 0;
}