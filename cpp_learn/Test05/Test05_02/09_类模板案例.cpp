#define _CRT_SECURE_NO_WARNINGS

#include<iostream>
#include<string>
using namespace std;
#include "MyArray.hpp"

void printIntArray(MyArray <int>& arr) {
	for (int i = 0;i < arr.getSize();i++) {
		cout << arr[i] << endl;
	}
}

void test01() {
	MyArray<int> arr1(5);
	//MyArray<int> arr2(arr1);
	//MyArray<int> arr3(100);
	//arr3 = arr1;

	for (int i = 0;i < 5;i++) {
		// 利用尾插法向数组中插入数据
		arr1.push_back(i);
	}
	cout << "arr1的打印输出为：" << endl;
	printIntArray(arr1);

	cout << "arr1的容量为："<< arr1.getCapacity() << endl;
	cout << "arr1的大小为：" << arr1.getSize() << endl;

	// 尾删
	arr1.Pop_Back();
	cout << "尾删后arr1的容量为：" << arr1.getCapacity() << endl;
	cout << "尾删后arr1的大小为：" << arr1.getSize() << endl;
}

// 自定义数据类型
class Person {
public:
	Person() {};
	Person(string name, int age) {
		this->m_Age = age;
		this->m_Name = name;
	}

	string m_Name;
	int m_Age;
};

void printPersonArray(MyArray<Person>& arr) {
	for (int i = 0;i < arr.getSize();i++) {
		cout << "姓名:" << arr[i].m_Name << " 年龄:" << arr[i].m_Age << endl;
	}
}

void test02() {
	MyArray<Person> arr(10);
	Person p1("孙悟空", 999);
	Person p2("赵信", 30);
	Person p3("韩信", 20);
	Person p4("李信", 50);
	Person p5("妲己", 800);

	// 尾插入数组
	arr.push_back(p1);
	arr.push_back(p2);
	arr.push_back(p3);
	arr.push_back(p4);
	arr.push_back(p5);

	// 打印数组
	printPersonArray(arr);
	// 容量和大小
	cout << "arr的容量为：" << arr.getCapacity() << endl;
	cout << "arr的大小为：" << arr.getSize() << endl;

	// 尾删
	arr.Pop_Back();
	cout << "尾删后arr的容量为：" << arr.getCapacity() << endl;
	cout << "尾删后arr的大小为：" << arr.getSize() << endl;
}

int main() {
	//test01();
	test02();
	system("pause");
	return 0;
}