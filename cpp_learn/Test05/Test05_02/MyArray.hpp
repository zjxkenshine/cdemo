// 自定义数组类
#pragma once
#include<iostream>
using namespace std;

template <class T>
class MyArray {
public:
	// 有参构造
	MyArray(int capacity) {
		cout << "MyArray有参构造调用" << endl;
		this->m_Capacity = capacity;
		this->m_Size = 0;
		this->pAddress = new T[this->m_Capacity];
	}

	// 拷贝构造 深拷贝
	MyArray(const MyArray& arr) {
		cout << "MyArray拷贝构造调用" << endl;
		this->m_Capacity = arr.m_Capacity;
		this->m_Size = arr.m_Size;
		// 深拷贝
		this->pAddress = new T[arr.m_Capacity];
		// 拷贝arr中的数据
		for (int i = 0;i < this->m_Size;i++) {
			this->pAddress[i] = arr.pAddress[i];
		}
	}

	// 运算符重载 防止浅拷贝
	MyArray& operator=(const MyArray& arr) {
		cout << "MyArray的operator=调用" << endl;
		// 先判断原来堆区是否有数据，如果有先释放
		if (this->pAddress != NULL) {
			delete[] this->pAddress;
			this->pAddress = NULL;
			this->m_Capacity = 0;
			this->m_Size = 0;
		}

		// 深拷贝
		this->m_Capacity = arr.m_Capacity;
		this->m_Size = arr.m_Size;
		// 深拷贝
		this->pAddress = new T[arr.m_Capacity];
		// 拷贝arr中的数据
		for (int i = 0;i < this->m_Size;i++) {
			this->pAddress[i] = arr.pAddress[i];
		}
		return *this;
	}

	// 尾插法
	void push_back(const T& val){
		// 容量大小
		if (this->m_Capacity == this->m_Size) {
			return;
		}
		// 数组末尾插入数据
		this->pAddress[this->m_Size] = val;
		// 更新数组大小
		this->m_Size++;
	}

	// 尾删法
	void Pop_Back() {
		// 让用户访问不到最后一个元素 逻辑删除
		if (this->m_Size == 0) {
			return;
		}
		this->m_Size--;
	}

	// 通过下标访问元素 重载[]函数
	T& operator[](int index) {
		return this->pAddress[index];
	}

	// 返回数组容量
	int getCapacity() {
		return this->m_Capacity;
	}

	// 返回数组大小
	int getSize() {
		return this->m_Size;
	}

	// 析构
	~MyArray() {
		cout << "MyArray析构函数调用" << endl;
		if (this -> pAddress != NULL) {
			delete[] this->pAddress;
			this->pAddress = NULL;
		}
	}

private:
	T* pAddress;// 指针指向堆区开辟的真实数组
	int m_Capacity; // 数组容量
	int m_Size; // 数组大小
};