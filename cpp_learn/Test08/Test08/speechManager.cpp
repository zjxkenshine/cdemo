#define _CRT_SECURE_NO_WARNINGS
#include "speechManager.h"

// 构造函数
SpeechManager::SpeechManager() {
	// 初始化比赛
	this->initSpeech();
	// 创建选手
	this->createSpeaker();
	// 加载往届记录
	this->loadRecord();
}

// 显示菜单
void SpeechManager::showMenu() {
	cout << "********************************************" << endl;
	cout << "*************  欢迎参加演讲比赛 ************" << endl;
	cout << "*************  1.开始演讲比赛  *************" << endl;
	cout << "*************  2.查看往届记录  *************" << endl;
	cout << "*************  3.清空比赛记录  *************" << endl;
	cout << "*************  0.退出比赛程序  *************" << endl;
	cout << "********************************************" << endl;
	cout << endl;
}

// 退出系统
void SpeechManager::existSystem() {
	cout << "欢迎下次使用" << endl;
	system("pause");
	exit(0);
}

// 初始化容器
void SpeechManager::initSpeech() {
	// 容器都清空
	this->v1.clear();
	this->v2.clear();
	this->vVictory.clear();
	this->m_Speaker.clear();
	// 初始化记录容器
	this->m_Record.clear();

	// 初始化轮数
	this->m_Index = 1;
}

// 创建选手
void SpeechManager::createSpeaker() {
	string nameSeed = "ABCDEFGHIJKL";
	for (int i = 0;i < nameSeed.size();i++) {
		string name = "选手";
		name += nameSeed[i];
		Speaker sp;
		sp.m_Name = name;
		for (int j = 0;j < 2;j++) {
			sp.m_Score[j] = 0;
		}
		// 创建选手编号，并且放入到v1容器中
		this->v1.push_back(i + 10001);
		// 选手编号及对应选手 放入到map容器中
		this->m_Speaker.insert(make_pair(i + 10001, sp));
	}
}

// 抽签
void SpeechManager::speechDraw() {
	cout << "第<<" << this->m_Index << ">>轮比赛选手正在抽签" << endl;
	cout << "----------------------------------------------" << endl;
	cout << "抽签后演讲顺序如下：" << endl;
	// 第一轮
	if (this->m_Index == 1) {
		random_shuffle(v1.begin(), v1.end());
		// 输出
		for (vector<int>::iterator it = v1.begin();it != v1.end();it++) {
			cout << *it << " ";
		}
		cout << endl;
	}
	else {
		// 第二轮
		random_shuffle(v2.begin(), v2.end());
		for (vector<int>::iterator it=v2.begin();it != v2.end();it++) {
			cout << *it << " ";
		}
		cout << endl;
	}

	cout << "--------------------------" << endl;
	system("pause");
	cout << endl;
}

// 正式比赛
void SpeechManager::speechContest() {
	cout << "第" << this->m_Index << "轮比赛正式开始" << endl;

	// 临时容器，存放小组成绩
	multimap<double, int, greater<double>> groupScore;
	int num = 0; //记录人员个数 6人一组

	vector<int> v_Src; // 比赛选手容器
	if (this->m_Index == 1) {
		v_Src = v1;
	}
	else {
		v_Src = v2;
	}

	// 遍历所有选手进行比赛
	for (vector<int>::iterator it = v_Src.begin();it != v_Src.end();it++) {
		num++;
		// 评委打分
		deque<double> d;
		for (int i = 0;i < 10;i++) {
			double score = (rand() % 401 + 600)/10.0f; // 60~100
			//cout << score << " ";
			d.push_back(score);
		}
		//cout << endl;

		// 排序
		sort(d.begin(), d.end(), greater<double>());
		d.pop_front(); // 去除最高分
		d.pop_back(); // 去除最低分
		// 累加
		double sum = accumulate(d.begin(), d.end(), 0.0f);
		// 平均分
		double avg = sum /(double)d.size();

		// 打印平均分
		// cout << "编号：" << *it << " 姓名：" << this->m_Speaker[*it].m_Name <<"平均分："<<avg<<endl;
		// 将平均分放入map容器
		this->m_Speaker[*it].m_Score[this->m_Index - 1] = avg;

		// 将打分数据放入临时小组容器
		groupScore.insert(make_pair(avg, *it)); //key是得分，value是选手具体编号
		// 每6人取出前3名
		if (num % 6 == 0) {
			cout << "第" << num / 6 << "小组比赛名次：" << endl;
			for (multimap<double, int, greater<double>>::iterator it = groupScore.begin();it != groupScore.end();it++) {
				cout << "编号:" << it->second << "姓名:" << this->m_Speaker[it->second].m_Name << "成绩:" << this->m_Speaker[it->second].m_Score[this->m_Index - 1]<<endl;
			}
			// 取走前3名
			int count = 0;
			for (multimap<double, int, greater<double>>::iterator it = groupScore.begin();it != groupScore.end();it++ ) {
				if (count < 3) {
					if (this->m_Index == 1) {
						// 晋级区
						v2.push_back((*it).second);
					}
					else {
						// 获胜区
						vVictory.push_back((*it).second);
					}
					count++;
				}
			}
			
			// 清空容器
			groupScore.clear();
			cout << endl;
		}
	}
	cout << "---------------------------第" << this->m_Index << "轮比赛完毕！---------------------------" << endl;
	system("pause");
}

// 显示得分
void SpeechManager::showScore() {
	cout << "-----------------第" << this->m_Index << "轮晋级的选手信息如下：------------------------" << endl;
	vector<int> v;
	if (this->m_Index == 1) {
		v = v2;
	}
	else {
		v = vVictory;
	}

	for (vector<int>::iterator it = v.begin();it != v.end();it++) {
		cout << "选手编号：" << *it << " 姓名：" << this->m_Speaker[*it].m_Name << "得分：" <<
			this->m_Speaker[*it].m_Score[this->m_Index - 1] << endl;
	}
	cout << endl;

	system("pause");
	system("cls");
	this->showMenu();
}

// 保存记录
void SpeechManager::saveRecord() {
	ofstream ofs;
	ofs.open("speech.csv", ios::out | ios::app);//追加的方式写文件
	// 将每个选手的数据写入到文件中
	for (vector<int>::iterator it = vVictory.begin();it != vVictory.end();it++) {
		ofs << *it << "," << this->m_Speaker[*it].m_Score[1] << ",";
	}
	ofs << endl;

	cout << "文件已保存" << endl;
	this->fileIsEmpty = false;
};

// 读取记录
void SpeechManager::loadRecord() {
	ifstream ifs("speech.csv", ios::in); //读文件
	if (!ifs.is_open()) {
		this->fileIsEmpty = true;
		cout << "文件不存在" << endl;
		ifs.close();
		return;
	}

	// 文件清空情况
	char ch;
	ifs >> ch;
	if (ifs.eof()) {
		cout << "文件为空" << endl;
		this->fileIsEmpty = true;
		ifs.close();
		return;
	}

	// 文件不为空
	this->fileIsEmpty = false;
	ifs.putback(ch); // 将上面读取的字符放回来
	string data;
	// 第几届
	int index = 0;
	while (ifs >> data) {
		//cout << data << endl;
		// 存放字符串
		vector<string> v;

		//解析打印结果
		int pos = -1; //查找","位置的变量
		int start = 0;
		while (true) {
			pos = data.find(",", start);
			// 未找到
			if (pos == -1) {
				break;
			}
			string temp = data.substr(start, pos - start);
			//cout << temp << endl;
			v.push_back(temp);
			start = pos + 1;
		}
		this->m_Record.insert(make_pair(index,v));
		index++;
	}
	ifs.close();

	//for (map<int, vector<string>> ::iterator it = this->m_Record.begin();it != m_Record.end();it++) {
	//	cout << it->first << "冠军编号：" << it->second[0] <<"分数：" << it->second[1] << endl;
	//}
}

// 显示往届记录
void SpeechManager::showRecord() {
	if (this->fileIsEmpty) {
		cout << "文件不存在或记录为空！" << endl;
	}
	else {
		for (int i = 0; i < this->m_Record.size(); i++) {
			cout << "第" << i + 1 << "届 " <<
				"冠军编号：" << this->m_Record[i][0] << " 得分：" << this->m_Record[i][1] << " "
				"亚军编号：" << this->m_Record[i][2] << " 得分：" << this->m_Record[i][3] << " "
				"季军编号：" << this->m_Record[i][4] << " 得分：" << this->m_Record[i][5] << endl;
		}
	}
	system("pause");
	system("cls");
}

// 清空文件
void SpeechManager::clearRecord() {
	cout << "确认清空？" << endl;
	cout << "1、确认" << endl;
	cout << "2、返回" << endl;
	int select = 0;
	cin >> select;

	if (select == 1){
		//打开模式 ios::trunc 如果存在删除文件并重新创建
		ofstream ofs("speech.csv", ios::trunc);
		ofs.close();
		//初始化属性
		this->initSpeech();
		//创建选手
		this->createSpeaker();
		//获取往届记录
		this->loadRecord();

		cout << "清空成功！" << endl;
	}

	system("pause");
	system("cls");
}

// 开始比赛
void SpeechManager::startSpeech() {
	// 第一轮比赛开始
	// 1.抽签
	this->speechDraw();
	// 2.比赛
	this->speechContest();
	// 3.显示晋级结果
	this->showScore();

	this->m_Index++;
	// 第二轮比赛开始
	// 1.抽签
	this->speechDraw();
	// 2.比赛
	this->speechContest();
	// 3.显示最终结果
	this->showScore();

	// 保存分数到文件中
	this->saveRecord();

	// 重置比赛，获取记录
	// 初始化比赛
	this->initSpeech();
	// 创建选手
	this->createSpeaker();
	// 加载往届记录
	this->loadRecord();

	cout << "本届比赛完毕！" << endl;
	system("pause");
	system("cls");
}



// 析构函数
SpeechManager::~SpeechManager() {
}