#pragma once
#include <iostream>
using namespace std;
#include <vector>
#include <map>
#include "speaker.h"
#include<deque>
#include <algorithm>
#include <functional>
#include <numeric>
#include <string>
#include <fstream>


// 设计演讲管理类
class SpeechManager {
public:
	// 构造函数
	SpeechManager();

	// 显示菜单
	void showMenu();

	// 退出系统
	void existSystem();

	// 析构函数
	~SpeechManager();

	// 初始化容器和属性
	void initSpeech();

	// 创建选手
	void createSpeaker();

	// 开始比赛 整个流程的控制函数
	void startSpeech();

	// 抽签
	void speechDraw();

	// 比赛
	void speechContest();

	// 显示得分
	void showScore();

	// 写入文件
	void saveRecord();

	// 读取记录
	void loadRecord();

	// 显示往届记录
	void showRecord();

	// 清空记录
	void clearRecord();

	// 往届记录
	map<int, vector<string>> m_Record;


	// 成员属性
	// 第一轮比赛选手编号容器
	vector<int> v1;
	// 第一轮晋级选手编号容器
	vector<int> v2;
	// 胜出的前三名
	vector<int> vVictory;
	// 编号及对应选手
	map<int, Speaker> m_Speaker;
	// 比赛轮数
	int m_Index;
	// 文件是否为空
	bool fileIsEmpty;
};
