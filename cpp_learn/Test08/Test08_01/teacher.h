#pragma once
#include <iostream>
using namespace std;
#include "identity.h"
#include "orderFile.h"

class Teacher :public Identity {
public:
	// 构造函数
	Teacher();

	// 有参构造
	Teacher(int empId, string name, string pwd);

	// 菜单页面
	virtual void openMenu();

	// 查看所有预约
	void showAllOrder();
	// 审核预约
	void validOrder();

	// 职工号
	int m_EmpId;
};
