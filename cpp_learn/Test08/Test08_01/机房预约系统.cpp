#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
using namespace std;
#include "globalFile.h"
#include "identity.h"
#include "student.h"
#include "teacher.h"
#include "manager.h"
#include <fstream>
#include <string>

// 教师子菜单
void TeacherMenu(Identity*& teacher){
	while (true){
		//教师菜单
		teacher->openMenu();
		Teacher* tea = (Teacher*)teacher;
		int select = 0;

		cin >> select;

		if (select == 1){
			//查看所有预约
			tea->showAllOrder();
		}else if (select == 2){
			//审核预约
			tea->validOrder();
		}else{
			delete teacher;
			cout << "注销成功" << endl;
			system("pause");
			system("cls");
			return;
		}
	}
}

// 进入学生子菜单页面
void studentMenu(Identity*& student) {
	while (true){
		//学生菜单
		student->openMenu();
		Student* stu = (Student*)student;
		int select = 0;
		cin >> select;

		if (select == 1){//申请预约
			stu->applyOrder();
		}
		else if (select == 2){ //查看自身预约
			stu->showMyOrder();
		}else if (select == 3) {//查看所有预约
			stu->showAllOrder();
		}else if (select == 4) {//取消预约
			stu->cancelOrder();
		}else{
			delete student;
			cout << "注销成功" << endl;
			system("pause");
			system("cls");
			return;
		}
	}
}

// 进入管理员子菜单界面
void managerMenu(Identity* manager){
	while(true){
		//管理员菜单
		manager->openMenu();
		Manager* man = (Manager*)manager;
		int select = 0;
		cin >> select;
		if (select == 1) { //添加账号
			//cout << "添加账号" << endl;
			man->addPerson();
		}
		else if (select == 2) {//查看账号
			//cout << "查看账号" << endl;
			man->showPerson();
		}
		else if (select == 3) { //查看机房
			//cout << "查看机房" << endl;
			man->showComputer();
		}
		else if (select == 4) {//清空预约
			//cout << "清空预约" << endl;
			man->cleanFile();
		}else {	// 注销
			delete manager;
			cout << "注销成功" << endl;
			system("pause");
			system("cls");
			return;
		}
	}
}


// 登录功能 参数1 操作文件名 参数2 操作身份类型
void LoginIn(string fileName, int type) {
	// 父类指针，指向子类对象
	Identity* person = NULL;

	// 读文件
	ifstream ifs;
	ifs.open(fileName, ios::in);

	// 判断文件是否存在
	if (!ifs.is_open()) {
		cout << "文件不存在" << endl;
		ifs.close();
		return;
	}

	int id = 0;
	string name;
	string pwd;

	// 判断身份
	if (type == 1){//学生登录
		cout << "请输入你的学号" << endl;
		cin >> id;
	}
	else if (type == 2) { //教师登录
		cout << "请输入你的职工号" << endl;
		cin >> id;
	}

	// 输入用户名密码
	cout << "请输入用户名：" << endl;
	cin >> name;
	cout << "请输入密码： " << endl;
	cin >> pwd;

	if (type == 1){
		//学生登录验证
		// 文件id
		int fId;
		// 文件名称
		string fName;
		// 文件密码
		string fPwd;
		// 读取文件信息
		while (ifs >> fId && ifs >> fName && ifs >> fPwd){
			// 与用户输入的信息对比
			if (id == fId && name == fName && pwd == fPwd){
				cout << "学生验证登录成功!" << endl;
				system("pause");
				system("cls");
				// 创建学生对象
				person = new Student(id, name, pwd);
				// 进入学生子菜单
				studentMenu(person);
				return;
			}
		}
	}else if (type == 2){
		//教师登录验证
		//文件id
		int fId;
		// 文件名称
		string fName;
		// 文件密码
		string fPwd;
		while (ifs >> fId && ifs >> fName && ifs >> fPwd){
			if (id == fId && name == fName && pwd == fPwd){
				cout << "教师验证登录成功!" << endl;
				system("pause");
				system("cls");
				// 创建教师对象
				person = new Teacher(id, name, pwd);
				// 进入教师子菜单
				TeacherMenu(person);
				return;
			}
		}
	}
	else if (type == 3){
		//管理员登录验证
		string fName; // 文件中的名称
		string fPwd; // 文件中的密码
		while (ifs >> fName && ifs >> fPwd){
			if (name == fName && pwd == fPwd){
				cout << "验证登录成功!" << endl;
				//登录成功后，按任意键进入管理员界面
				system("pause");
				system("cls");
				//创建管理员对象
				person = new Manager(name, pwd);
				// 管理员菜单
				managerMenu(person);
				return;
			}
		}
	}

	cout << "验证登录失败!" << endl;
	system("pause");
	system("cls");
	return;
}

int main() {
	int select = 0;
	while (true) {
		cout << "======================  欢迎来到传智播客机房预约系统  ====================="
			<< endl;
		cout << endl << "请输入您的身份" << endl;
		cout << "\t\t -------------------------------\n";
		cout << "\t\t|                               |\n";
		cout << "\t\t|          1.学生代表           |\n";
		cout << "\t\t|                               |\n";
		cout << "\t\t|          2.老    师           |\n";
		cout << "\t\t|                               |\n";
		cout << "\t\t|          3.管 理 员           |\n";
		cout << "\t\t|                               |\n";
		cout << "\t\t|          0.退    出           |\n";
		cout << "\t\t|                               |\n";
		cout << "\t\t -------------------------------\n";
		cout << "输入您的选择: ";

		cin >> select; // 用户选择
		switch (select) {
		case 1:	 // 学生
			LoginIn(STUDENT_FILE, 1);
			break;
		case 2:	// 老师
			LoginIn(TEACHER_FILE, 2);
			break;
		case 3: // 管理员
			LoginIn(ADMIN_FILE, 3);
			break;
		case 0:	// 退出
			cout << "欢迎下次使用" << endl;
			system("pause");
			return 0;
			break;
		default:
			cout << "输入有误，请重新输入" << endl;
			system("pause");
			system("cls");
			break;
		}
	}
	

	system("pause");
	return 0;
}