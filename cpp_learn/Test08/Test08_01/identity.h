#pragma once
#include<iostream>
using namespace std;
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>
#include "globalFile.h"


// 身份抽象函数基类
class Identity {
public:
	// 操作函数，纯虚函数
	virtual void openMenu() = 0;

	// 用户名
	string m_Name;
	// 密码
	string m_Pwd;
};
