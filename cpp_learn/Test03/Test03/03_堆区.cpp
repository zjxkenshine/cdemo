#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
using namespace std;

// 堆区
// 由程序员分配释放,若程序员不释放,程序结束时由操作系统回收
int* func1(){
	// new 操作符开辟内存
	int* a = new int(10);
	return a;
}

int main3() {

	int* p = func1();
	cout << *p << endl;
	cout << *p << endl;

	system("pause");
	return 0;
}