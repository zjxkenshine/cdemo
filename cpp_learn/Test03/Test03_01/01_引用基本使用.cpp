#define _CRT_SECURE_NO_WARNINGS

#include<iostream>
using namespace std;

int main1() {

	int a = 10;
	// 引用 起别名
	int& b = a;

	cout << "a = " << a << endl;
	cout << "b = " << b << endl;

	b = 100;
	cout << "a = " << a << endl;
	cout << "b = " << b << endl;

	system("pause");
	return 0;
}