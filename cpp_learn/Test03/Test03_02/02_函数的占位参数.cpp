#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
using namespace std;

// 暂时用不到，后面有用
//函数占位参数 ，占位参数也可以有默认参数
void func(int a, int) {
	cout << "this is func" << endl;
}

// 占位参数默认参数
void func1(int a, int=10) {
	cout << "this is func" << endl;
}

int main2() {
	func(10, 10); //占位参数必须填补

	system("pause");
	return 0;
}