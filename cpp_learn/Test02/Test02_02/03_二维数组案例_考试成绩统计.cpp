#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
using namespace std;

// 考试成绩统计
int main() {
	// 1.创建二维数组
	int score[3][3] = {
		{100,100,100},
		{90,50,80},
		{60,70,80}
	};
	// 2.统计每个人的总和分数
	for (int i = 0;i < 3;i++) {
		int sum = 0;
		for (int j = 0;j < 3;j++) {
			sum += score[i][j];
			//cout << score[i][j] << " ";
		}
		cout <<"第"<<i+1<<"个人的总分是"<<sum<< endl;
	}
	return 0;
}