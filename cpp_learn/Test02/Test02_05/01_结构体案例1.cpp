#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
#include <ctime>
using namespace std;

/*
  案例1 
  3个老师，1个老师带5个学生
  设计学生和老师的结构体，其中在老师的结构体中，有老师姓名和一个存放5名学生的数组作为成员
*/

// 学生结构体
struct Student {
	// 姓名
	string name;
	// 分数
	int score;
};

// 老师结构体
struct Teacher {
	// 姓名
	string tName;
	// 学生数组
	struct Student sArray[5];
};

// 给老师和学生赋值
void allocateSpace(struct Teacher tArray[], int len) {
	string nameSeed = "ABCDE";
	// 老师赋值
	for (int i = 0;i < len;i++) {
		tArray[i].tName = "Teacher_";
		tArray[i].tName += nameSeed[i];

		// 学生赋值
		for (int j = 0; j < 5; j++){
			// 直接用+号拼接会有问题
			tArray[i].sArray[j].name = "Student_";
			tArray[i].sArray[j].name += nameSeed[j];
			tArray[i].sArray[j].score = rand() % 61 + 40;
		}
	}
}

// 打印学生信息
void printTeachers(struct Teacher tArray[], int len) {
	for (int i = 0; i < len; i++){
		cout << tArray[i].tName << endl;
		for (int j = 0; j < 5; j++)
		{
			cout << "\t姓名：" << tArray[i].sArray[j].name << " 分数：" << tArray[i].sArray[j].score << endl;
		}
	}
}

int main1() {
	srand((unsigned int)time(NULL)); //随机数种子 头文件 #include <ctime>

	// 创建3名老师的数组
	struct Teacher tArray[3];

	// 通过函数给3名老师赋值，并给对应的学生赋值
	int len = sizeof(tArray) / sizeof(tArray[0]);

	allocateSpace(tArray, len); //创建数据
	printTeachers(tArray, len); //打印数据
	return 0;
}