#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
using namespace std;
#include<vector>
#include<algorithm>

//常用遍历算法  搬运 transform
class TransForm{
public:
	int operator()(int val){
		return val;
	}
};

class MyPrint{
public:
	void operator()(int val){
		cout << val << " ";
	}
};

void test01(){
	vector<int>v;
	for (int i = 0; i < 10; i++){
		v.push_back(i);
	}
	vector<int>vTarget; //目标容器
	vTarget.resize(v.size()); // 目标容器需要提前开辟空间，否则会报错
	transform(v.begin(), v.end(), vTarget.begin(), TransForm());
	for_each(vTarget.begin(), vTarget.end(), MyPrint());
}

int main() {
	test01();
	system("pause");
	return 0;
}