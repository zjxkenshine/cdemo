#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
using namespace std;
#include <algorithm>
#include <vector>
#include <ctime>

class myPrint{
public:
	void operator()(int val){
		cout << val << " ";
	}
};

void test01(){
	// 需要设置种子
	srand((unsigned int)time(NULL));
	vector<int> v;
	for (int i = 0; i < 10;i++){
		v.push_back(i);
	}
	for_each(v.begin(), v.end(), myPrint());
	cout << endl;

	//打乱顺序
	random_shuffle(v.begin(), v.end());
	for_each(v.begin(), v.end(), myPrint());
	cout << endl;
}

int main() {
	test01();
	system("pause");
	return 0;
}