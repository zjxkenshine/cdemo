#define _CRT_SECURE_NO_WARNINGS

#include<iostream>
using namespace std;
// 测试多态
#include "worker.h"
#include "employee.h"
#include "manager.h"
#include "boss.h"

int main1() {
	Worker* worker = NULL;
	worker = new Employee(1, "张三", 1);
	worker->showInfo();
	delete worker;

	worker = new Manager(2, "李四", 2);
	worker->showInfo();
	delete worker;

	worker = new Boss(3, "王五", 3);
	worker->showInfo();
	delete worker;
	return 0;
}