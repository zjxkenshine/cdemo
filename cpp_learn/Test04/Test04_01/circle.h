#pragma once
#include<iostream>
using namespace std;

#include "point.h"

class Circle {
public:
	void setR(int r);
	int getR();
	void setCenter(Point center);
	Point getCenter();
private:
	int m_R;// 半径
	// 类中可以让另一个类放在成员中
	Point m_Center; // 圆心坐标
};