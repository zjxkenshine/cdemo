#define _CRT_SECURE_NO_WARNINGS
#include "circle.h"

// 设置获取半径
void Circle::setR(int r) {
	m_R = r;
}
int  Circle::getR() {
	return m_R;
}
// 设置圆心
void  Circle::setCenter(Point center) {
	m_Center = center;
}
// 获取圆心
Point Circle::getCenter() {
	return m_Center;
}