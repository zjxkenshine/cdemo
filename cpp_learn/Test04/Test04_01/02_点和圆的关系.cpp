#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
using namespace std;

// 点类
class Point2 {
public:
	void setX(int x) {
		m_X = x;
	}
	int getX() {
		return m_X;
	}
	void setY(int y) {
		m_Y = y;
	}
	int getY() {
		return m_Y;
	}
private:
	int m_X;
	int m_Y;
};

 //点和圆的关系 点在圆内，点在圆外，点在圆上
class Circle2 {
public:
	// 设置获取半径
	void setR(int r) {
		m_R1 = r;
	}
	int getR() {
		return m_R1;
	}
	// 设置圆心
	void setCenter(Point2 center) {
		m_Center1 = center;
	}
	// 获取圆心
	Point2 getCenter() {
		return m_Center1;
	}

private:
	int m_R1;// 半径
	// 类中可以让另一个类放在成员中
	Point2 m_Center1; // 圆心坐标
};

//判断点与圆关系
void isInCircle(Circle2& c,Point2& p){
	// 计算两点之间的距离 平方
	int distance = (c.getCenter().getX() - p.getX()) * (c.getCenter().getX() - p.getX()) -
		(c.getCenter().getY() - p.getY()) * (c.getCenter().getY() - p.getY());
	// 计算半径的平方
	int rDistance = c.getR() * c.getR();
	// 判断点的位置
	if (distance == rDistance) {
		cout << "点在圆上" << endl;
	}
	else if (distance > rDistance) {
		cout << "点在圆外" << endl;
	}
	else{
		cout << "点在圆内" << endl;
	}
}

int main2() {
	// 创建圆
	Point2 p1;
	p1.setX(0);
	p1.setY(0);
	Circle2 c;
	c.setCenter(p1);
	c.setR(10);

	// 创建点
	Point2 p2;
	p2.setX(10);
	p2.setY(0);

	isInCircle(c,p2);
	return 0;
}