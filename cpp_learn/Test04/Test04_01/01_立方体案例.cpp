#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
using namespace std;

// 立方体类设计
class Cube {
public:
	// 设置/获取长宽高
	void setL(int l) {
		m_L = l;
	}
	int getL() {
		return m_L;
	}
	void setW(int l) {
		m_W = l;
	}
	int getW() {
		return m_W;
	}
	void setH(int l) {
		m_H = l;
	}
	int getH() {
		return m_H;
	}
	// 获取面积
	int calculateS() {
		return 2 * m_L * m_W + 2 * m_L * m_H + 2 * m_W * m_H;
	}

	// 获取体积
	int calculateV() {
		return  m_L * m_W * m_H;
	}

	// 判断是否相等
	bool isSameTo(Cube& c2) {
		if (m_L == c2.getL() && m_W == c2.getW() && m_H == c2.getH()) {
			return true;
		}
		else {
			return false;
		}
	}

private:
	int m_L;//长
	int m_W;//宽
	int m_H; //高
};

// 全局函数判断两个立方体是否相等
bool isSame(Cube& c1, Cube& c2) {
	if (c1.getL() == c2.getL() && c1.getW() == c2.getW() && c1.getH() == c2.getH()){
		return true;
	}
	else {
		return false;
	}
}


int main1() {
	// 创建立方体对象
	Cube c1;
	c1.setH(10);
	c1.setL(10);
	c1.setW(10);
	cout << "c1的面积为" << c1.calculateS() << endl;
	cout << "c1的体积为" << c1.calculateV() << endl;

	// 立方体2
	Cube c2;
	c2.setH(10);
	c2.setL(10);
	c2.setW(10);
	// 两种判断方式 全局函数
	cout << isSame(c1,c2) << endl;
	// 成员函数
	cout << c1.isSameTo(c2) << endl;
	return 0;
}