#define _CRT_SECURE_NO_WARNINGS
#include"circle.h"
#include"point.h"


// 判断点与圆关系
void isInCircle1(Circle& c, Point& p) {
	// 计算两点之间的距离 平方
	int distance = (c.getCenter().getX() - p.getX()) * (c.getCenter().getX() - p.getX()) -
		(c.getCenter().getY() - p.getY()) * (c.getCenter().getY() - p.getY());
	// 计算半径的平方
	int rDistance = c.getR() * c.getR();
	// 判断点的位置
	if (distance == rDistance) {
		cout << "点在圆上" << endl;
	}
	else if (distance > rDistance) {
		cout << "点在圆外" << endl;
	}
	else {
		cout << "点在圆内" << endl;
	}


}

int main() {
	// 创建圆
	Point p1;
	p1.setX(0);
	p1.setY(0);
	Circle c;
	c.setCenter(p1);
	c.setR(10);

	// 创建点
	Point p2;
	p2.setX(10);
	p2.setY(0);

	isInCircle1(c, p2);
	return 0;
}