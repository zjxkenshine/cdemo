#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
using namespace std;

// 抽象类
class AbstractDrinking {
public:
	// 煮水
	virtual void Boil() = 0;
	// 冲泡
	virtual void Brew() = 0;
	// 放入杯子中
	virtual void putInCup() = 0;
	// 加入辅料
	virtual void putSomething() = 0;
	// 制作饮品
	void makeDrink() {
		Boil();
		Brew();
		putInCup();
		putSomething();
	}
};

// 咖啡
class Coffee :public AbstractDrinking {
	// 煮水
	virtual void Boil() {
		cout << "煮咖啡水" << endl;
	}
	// 冲泡
	virtual void Brew() {
		cout << "冲泡咖啡" << endl;
	}
	// 放入杯子中
	virtual void putInCup() {
		cout << "咖啡放入杯中" << endl;
	}
	// 加入辅料
	virtual void putSomething() {
		cout << "加入咖啡辅料" << endl;
	}
};

// 茶
class Tea :public AbstractDrinking {
	// 煮水
	virtual void Boil() {
		cout << "煮水" << endl;
	}
	// 冲泡
	virtual void Brew() {
		cout << "冲泡龙井" << endl;
	}
	// 放入杯子中
	virtual void putInCup() {
		cout << "放入杯中" << endl;
	}
	// 加入辅料
	virtual void putSomething() {
		cout << "加入糖" << endl;
	}
};

// 制作函数
void doWork(AbstractDrinking* abs) {
	abs->makeDrink();
	delete abs;
}

int main() {
	// 制作咖啡
	doWork(new Coffee);
	// 制作茶
	doWork(new Tea);
	return 0;
}
