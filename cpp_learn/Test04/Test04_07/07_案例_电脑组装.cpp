#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
using namespace std;

class CPU {
	// 抽象
public:
	virtual void calculate() = 0;
};

class VideoCard {
	// 抽象
public:
	virtual void display() = 0;
};

class Memory {
	// 抽象
public:
	virtual void storage() = 0;
};

class Computer {
public:
	Computer(CPU* cpu, VideoCard* vc, Memory* mem) {
		this->cpu = cpu;
		this->mem = mem;
		this->vc = vc;
	}

	void work() {
		cpu->calculate();
		vc->display();
		mem->storage();
	}

	~Computer() {
		if (cpu != NULL) {
			delete cpu;
			cpu = NULL;
		}
		if (vc != NULL) {
			delete vc;
			vc = NULL;
		}
		if (mem != NULL) {
			delete mem;
			mem = NULL;
		}
	}
private:
	CPU* cpu;
	VideoCard* vc;
	Memory* mem;
};

class IntelCPU :public CPU {
public :
	virtual void calculate() {
		cout << "Intel的CPU开始计算了" << endl;
	}
};

class IndelVideoCard :public VideoCard {
public:
	virtual void display() {
		cout << "Intel的显卡显示" << endl;
	}
};

class IntelMemory :public Memory {
public:
	virtual void storage() {
		cout << "Intel的内存条存储" << endl;
	}
};


class LenovoCPU :public CPU {
public:
	virtual void calculate() {
		cout << "Lenovo的CPU开始计算了" << endl;
	}
};

class LenovoVideoCard :public VideoCard {
public:
	virtual void display() {
		cout << "Lenovo的显卡显示" << endl;
	}
};

class LenovoMemory :public Memory {
public:
	virtual void storage() {
		cout << "Lenovo的内存条存储" << endl;
	}
};

int main() {
	// 组装电脑
	CPU* cpu1 = new IntelCPU;
	VideoCard* vc1 = new IndelVideoCard;
	Memory* mem1 = new IntelMemory;
	// 组装运行电脑
	Computer* computer1 = new Computer(cpu1, vc1, mem1);
	computer1->work();
	delete computer1;

	// 第二台电脑
	CPU* cpu2 = new LenovoCPU;
	VideoCard* vc2 = new IndelVideoCard;
	Memory* mem2 = new LenovoMemory;
	// 组装运行电脑
	Computer* computer2 = new Computer(cpu2, vc2, mem2);
	computer2->work();
	delete computer2;
	return 0;
}
