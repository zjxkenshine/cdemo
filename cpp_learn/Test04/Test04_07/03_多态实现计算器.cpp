#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
using namespace std;

// 多态实现计算器
class AbstractCalculator {
public:
	virtual int getResult() {
		return 0;
	}
	int num1;
	int num2;

};

// 加法计算器
class Adder :public AbstractCalculator {
public :
	int getResult() {
		return num1 + num2;
	}
};

// 减法计算器
class Subber :public AbstractCalculator {
public:
	int getResult() {
		return num1 - num2;
	}
};

int main3() {
	// 加法运算 父类指针指向子类对象
	AbstractCalculator* add = new Adder;
	add->num1 = 10;
	add->num2 = 10;
	cout << add->getResult() << endl;
	// 销毁
	delete add;

	// 减法
	AbstractCalculator* sub = new Subber;
	sub->num1 = 10;
	sub->num2 = 10;
	cout << sub->getResult() << endl;
	delete sub;

	return 0;
}