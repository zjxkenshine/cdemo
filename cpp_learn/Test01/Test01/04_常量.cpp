#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
using namespace std;

/*
常量定义方式
1. #define
2. const
*/
// 常量定义方式1
#define DAY 7

int main4() {

	cout << "一周总共有" << DAY <<"天" << endl;
	// 常量定义方式2
	const int month = 12;
	cout << "一年总共有" << month << "个月" << endl;
	system("pause");
	return 0;
}