#define _CRT_SECURE_NO_WARNINGS

#include<iostream>
using namespace std;

/*
标识符规则

- 标识符不能是关键字
- 标识符只能由字母、数字、下划线组成
- 第一个字符必须为字母或下划线
- 标识符中字母区分大小写
*/
int main() {
	int abc = 10;
	int _abc = 20;
	int _123_abc = 30;
	cout << "abc" << abc << endl;

	int a = 10;
	int b = 20;
	int c = a + b;
	cout <<  c << endl;

	return 0;
}