#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
using namespace std;

int main5() {

	// 换行符
	cout << "hello world\n";

	// 反斜杠
	cout << "\\" << endl;

	// 水平制表符 输出对齐
	cout << "aaa\thelloworld" << endl;
	cout << "aa\thelloworld" << endl;
	cout << "aaaa\thelloworld" << endl;

	return 0;
}
