#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
using namespace std;

// sizeof 计算占用空间
int main2() {
	cout << "short 类型所占内存空间为： " << sizeof(short) << endl;
	cout << "int 类型所占内存空间为： " << sizeof(int) << endl;
	cout << "long 类型所占内存空间为： " << sizeof(long) << endl;
	cout << "long long 类型所占内存空间为： " << sizeof(long long) << endl;
	return 0;
}