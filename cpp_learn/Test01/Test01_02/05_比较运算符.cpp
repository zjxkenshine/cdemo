#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
using namespace std;

int main5() {
	
	int a = 10;
	int b = 20;
	// 非0表示真，0表示假
	cout << (a == b) << endl; // 0 

	cout << (a != b) << endl; // 1

	cout << (a > b) << endl; // 0

	cout << (a < b) << endl; // 1

	cout << (a >= b) << endl; // 0

	cout << (a <= b) << endl; // 1

	system("pause");

	return 0;
}