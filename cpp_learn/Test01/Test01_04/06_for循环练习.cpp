#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
using namespace std;
/*
   敲桌子
   1到100 含有7或是7的倍数打印敲桌子，其余数字直接输出
*/

int main6() {

	for (int i = 1;i <= 100;i++) {
		if (i % 10 == 7 || i / 10 == 7||i%7==0) {
			cout << "敲桌子" << endl;
		}
		else {
			cout << i << endl;
		}
	}
	return 0;
}