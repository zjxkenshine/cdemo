#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <ctime>
using namespace std;

// 猜数字游戏
int main2() {
	// 设置随机数种子
	srand((unsigned int)time(0));
	// 生成随机数 1-100随机数
	int num = rand() % 100 + 1;
	//cout << num << endl;
	
	// 玩家猜测
	int val = 0;
	while (1) {
		cin >> val;

		if (val > num) {
			cout << "大了" << endl;
		}
		else if (val < num) {
			cout << "小了" << endl;
		}
		else {
			cout << "对喽" << endl;
			break;
		}
	}
	

	return 0;
}