#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
using namespace std;

// 求所有三位数的水仙花数
// 1^3+5^3+3^3=153
int main4() {

	// 1.所有三位数输出(100~999)
	// 2.找出水仙花数
	int num = 100;
	do {
		int a = 0;
		int b = 0;
		int c = 0;
		a = num % 10;
		b = (num / 10) % 10;
		c = num / 100;

		if (a* a* a + b * b * b + c * c * c ==num) {
			cout << num << endl;
		}
	} while (num++ < 999);
	return 0;
}