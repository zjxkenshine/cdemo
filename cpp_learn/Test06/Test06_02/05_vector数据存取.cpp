#define _CRT_SECURE_NO_WARNINGS
#include <vector>
#include<iostream>
using namespace std;

void test01(){
	vector<int>v1;
	for (int i = 0; i < 10; i++){
		v1.push_back(i);
	}
	// 中括号方式访问
	for (int i = 0; i < v1.size(); i++){
		cout << v1[i] << " ";
	}
	cout << endl;
	// at方式访问
	for (int i = 0; i < v1.size(); i++){
		cout << v1.at(i) << " ";
	}
	cout << endl;
	cout << "v1的第一个元素为： " << v1.front() << endl;
	cout << "v1的最后一个元素为： " << v1.back() << endl;
}

int main() {
	test01();
	system("pause");
	return 0;
}