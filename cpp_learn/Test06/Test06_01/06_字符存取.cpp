#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
using namespace std;
#include<string>

void test07(){
	string str = "hello world";
	// [] 方式存取字符
	for (int i = 0; i < str.size(); i++){
		cout << str[i] << " ";
	}
	cout << endl;

	// at方法取字符
	for (int i = 0; i < str.size(); i++){
		cout << str.at(i) << " ";
	}
	cout << endl;

	//字符修改
	str[0] = 'x';
	str.at(1) = 'x';
	cout << str << endl;
}

int main6() {
	test07();
	system("pause");
	return 0;
}