#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
using namespace std;
#include<string>

//查找和替换
void test04(){
	//查找
	string str1 = "abcdefgde";
	int pos = str1.find("de");
	if (pos == -1){
		cout << "未找到" << endl;
	}else{
		cout << "pos = " << pos << endl;
	}
	// 最后一次出现的位置 rfind从右往左查找
	pos = str1.rfind("de");
	cout << "pos = " << pos << endl;
}

void test05(){
	//替换
	string str1 = "abcdefgde";
	str1.replace(1, 3, "1111");
	cout << "str1 = " << str1 << endl;
}

int main4() {
	test04();
	test05();
	system("pause");
	return 0;
}