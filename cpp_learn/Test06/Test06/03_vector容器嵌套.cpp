#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
using namespace std;
// vector容器
#include<vector>

void test03() {
	vector<vector<int>> v;
	// 小容器
	vector<int> v1;
	vector<int> v2;
	vector<int> v3;
	vector<int> v4;

	// 小容器添加数据
	for (int i = 0;i < 4;i++) {
		v1.push_back(i + 10);
		v2.push_back(i + 20);
		v3.push_back(i + 30);
		v4.push_back(i + 40);
	}

	// 将小容器放入大容器
	v.push_back(v1);
	v.push_back(v2);
	v.push_back(v3);
	v.push_back(v4);

	// 通过大容器，把所有数据遍历一遍
	for (vector<vector<int>>::iterator it = v.begin();it < v.end();it++) {
		for (vector<int>::iterator vit = (*it).begin();vit < (*it).end();vit++) {
			cout << *vit << " ";
		}
		cout << endl;
	}
}


int main() {
	test03();
	return 0;
}
