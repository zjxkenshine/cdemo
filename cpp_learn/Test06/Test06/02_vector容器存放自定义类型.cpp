#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
using namespace std;
// vector容器
#include<vector>
#include<string>

class Person {
public:
	Person(string name, int age) {
		this->m_Name = name;
		this->m_Age = age;
	}
	string m_Name;
	int m_Age;
};


void test02() {
	vector<Person> v;
	Person p1("a", 1);
	Person p2("b", 2);
	Person p3("c", 3);
	Person p4("d", 4);
	Person p5("e", 5);

	v.push_back(p1);
	v.push_back(p2);
	v.push_back(p3);
	v.push_back(p4);
	v.push_back(p5);
	// 存放指针
	vector<Person*> v1;
	v1.push_back(&p1);
	v1.push_back(&p2);
	v1.push_back(&p3);
	v1.push_back(&p4);
	v1.push_back(&p5);

	// 遍历容器数据
	for (vector<Person>::iterator it = v.begin();it != v.end();it++) {
		cout <<"姓名： "<< (*it).m_Name<< " 年龄： "<< (*it).m_Age << endl;
	}
	// 指针数组遍历
	for (vector<Person*>::iterator it = v1.begin();it != v1.end();it++) {
		cout << "姓名： " << (*it)->m_Name << " 年龄： " << (*it)->m_Age << endl;
	}

}

int main2() {
	test02();
	system("pause");
	return 0;
}