#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
using namespace std;
// vector容器
#include<vector>
// 标准算法
#include<algorithm>

void myPrint(int val) {
	cout << val << endl;
}


// vector容器存放内置数据类型
void test01() {
	// 创建一个vector容器，数组
	vector<int> v;

	// 插入数据
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);

	// 通过迭代器访问容器中的数据
	vector<int>::iterator itBegin = v.begin(); // 起始迭代器，指向容器中的第一个元素
	vector<int>::iterator itEnd = v.end();  // 指向最后一个元素的下一个位置

	// 第一种遍历方式
	while(itBegin != itEnd) {
		cout << *itBegin << endl;
		itBegin++;
	}

	// 第二种方式 常用方式
	for (vector<int>::iterator it = v.begin();it != v.end();it++) {
		cout << *it << endl;
	}

	// 第三种 遍历算法 回调打印
	for_each(v.begin(), v.end(),myPrint);
}

int main1() {
	test01();
	system("pause");
	return 0;
}