#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
using namespace std;
/*
- 公司今天招聘了10个员工（ABCDEFGHIJ），10名员工进入公司之后，需要指派员工在那个部门工作
- 员工信息有: 姓名  工资组成；部门分为：策划、美术、研发
- 随机给10名员工分配部门和工资
- 通过multimap进行信息的插入  key(部门编号) value(员工)
- 分部门显示员工信息
*/

#include <vector>
#include <string>
#include <map>

#define CEHUA 0
#define MEISHU 1
#define KAIFA 2

class Worker {
public:
	string m_Name;
	int m_Salary;
};

// 创建Worker
void createWorker(vector<Worker>& v) {
	string nameSeed = "ABCDEFGHIJ";
	for (int i = 0;i < 10;i++) {
		Worker worker;
		worker.m_Name = "";
		worker.m_Name += nameSeed[i];
		worker.m_Salary = rand() % 10000 + 10000;

		v.push_back(worker);
	}
}

// 员工分组
void setGroup(vector<Worker>& v, multimap<int, Worker>& m) {
	for (vector<Worker>::iterator it = v.begin();it != v.end();it++){
		// 随机产生部门编号
		int deptId = rand() % 3;

		// 将员工插入到分组中
		m.insert(make_pair(deptId,*it));
	}
}

// 分组打印用户
void showWorkerByGroup(multimap<int, Worker>& m) {
	cout << "策划部门" << endl;
	// 起始位置
	multimap<int,Worker>::iterator pos =m.find(CEHUA);
	// 统计策划部门人数
	int count = m.count(CEHUA);
	for (int index = 0;pos != m.end() && index < count;pos++,index++) {
		cout << "姓名: " << pos->second.m_Name << "工资" << pos->second.m_Salary<<endl;
	}

	cout << "美术部门" << endl;
	// 起始位置
	pos = m.find(MEISHU);
	// 美术部门人数
	count = m.count(MEISHU);
	for (int index = 0;pos != m.end() && index < count;pos++, index++) {
		cout << "姓名: " << pos->second.m_Name << "工资" << pos->second.m_Salary << endl;
	}

	cout << "开发部门" << endl;
	// 起始位置
	pos = m.find(KAIFA);
	// 开发部门人数
	count = m.count(KAIFA);
	for (int index = 0;pos != m.end() && index < count;pos++, index++) {
		cout << "姓名: " << pos->second.m_Name << "工资" << pos->second.m_Salary << endl;
	}
}


int main() {
	// 设置种子
	srand((unsigned int)time(NULL));

	//1、创建员工
	vector<Worker>vWorker;
	createWorker(vWorker);

	for (vector<Worker>::iterator it = vWorker.begin();it != vWorker.end();it++) {
		cout << "姓名： " << it->m_Name << "工资：" << it->m_Salary << endl;
	}

	// 2.员工分组
	multimap<int, Worker>mWorker;
	setGroup(vWorker, mWorker);

	// 3.打印员工 分组显示员工
	showWorkerByGroup(mWorker);

	system("pause");
	return 0;
}
